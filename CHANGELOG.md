# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.0.215](https://github.com/scratchfoundation/scratch-render/compare/v2.0.214...v2.0.215) (2025-03-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.73 ([1e8312b](https://github.com/scratchfoundation/scratch-render/commit/1e8312bbd1f2cf4f986542135b47948f4040464b))

## [2.0.214](https://github.com/scratchfoundation/scratch-render/compare/v2.0.213...v2.0.214) (2025-03-08)


### Bug Fixes

* **deps:** lock file maintenance ([e2bdae6](https://github.com/scratchfoundation/scratch-render/commit/e2bdae6b619863b32b826c7b05026be115684a6b))

## [2.0.213](https://github.com/scratchfoundation/scratch-render/compare/v2.0.212...v2.0.213) (2025-03-07)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.72 ([b8cc7e6](https://github.com/scratchfoundation/scratch-render/commit/b8cc7e6ce3650ac3b7734e28a12de856598c0fe3))

## [2.0.212](https://github.com/scratchfoundation/scratch-render/compare/v2.0.211...v2.0.212) (2025-03-07)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.171 ([19b4cc7](https://github.com/scratchfoundation/scratch-render/commit/19b4cc7b901883779134e855e74183b00e08a047))

## [2.0.211](https://github.com/scratchfoundation/scratch-render/compare/v2.0.210...v2.0.211) (2025-03-07)


### Bug Fixes

* **deps:** lock file maintenance ([3e8a8fc](https://github.com/scratchfoundation/scratch-render/commit/3e8a8fcedd0e4e068008e049d9ebdc5de10a599c))

## [2.0.210](https://github.com/scratchfoundation/scratch-render/compare/v2.0.209...v2.0.210) (2025-03-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.70 ([71e2ba4](https://github.com/scratchfoundation/scratch-render/commit/71e2ba4d788fde2322478541fc16f84f14f7a02b))

## [2.0.209](https://github.com/scratchfoundation/scratch-render/compare/v2.0.208...v2.0.209) (2025-03-06)


### Bug Fixes

* **deps:** lock file maintenance ([fb85048](https://github.com/scratchfoundation/scratch-render/commit/fb850485ded9f904f1049cfb5245a2483c405721))

## [2.0.208](https://github.com/scratchfoundation/scratch-render/compare/v2.0.207...v2.0.208) (2025-03-06)


### Bug Fixes

* **deps:** lock file maintenance ([455ca0e](https://github.com/scratchfoundation/scratch-render/commit/455ca0eacf858085e4c9639575ff89080dd7fb9b))

## [2.0.207](https://github.com/scratchfoundation/scratch-render/compare/v2.0.206...v2.0.207) (2025-03-05)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.69 ([79c17bb](https://github.com/scratchfoundation/scratch-render/commit/79c17bb092309c52b7da970d06f877dd4708fbd8))

## [2.0.206](https://github.com/scratchfoundation/scratch-render/compare/v2.0.205...v2.0.206) (2025-03-05)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.170 ([26b0d0f](https://github.com/scratchfoundation/scratch-render/commit/26b0d0f2c66ec9e7bd0744f8c107897eabc8f44c))

## [2.0.205](https://github.com/scratchfoundation/scratch-render/compare/v2.0.204...v2.0.205) (2025-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([43d81a0](https://github.com/scratchfoundation/scratch-render/commit/43d81a0fe9e0bd28a71adf6fe952d9bd571fbef3))

## [2.0.204](https://github.com/scratchfoundation/scratch-render/compare/v2.0.203...v2.0.204) (2025-03-04)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.67 ([5a8148a](https://github.com/scratchfoundation/scratch-render/commit/5a8148a6d856189b99e73128bd1588d4bde1fa52))

## [2.0.203](https://github.com/scratchfoundation/scratch-render/compare/v2.0.202...v2.0.203) (2025-03-04)


### Bug Fixes

* **deps:** lock file maintenance ([457fd80](https://github.com/scratchfoundation/scratch-render/commit/457fd802f7efbb8990d390d646270c493f46bf9c))

## [2.0.202](https://github.com/scratchfoundation/scratch-render/compare/v2.0.201...v2.0.202) (2025-03-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.66 ([bb2f2d3](https://github.com/scratchfoundation/scratch-render/commit/bb2f2d369632b50311b9b22e9ab609eee9a741e1))

## [2.0.201](https://github.com/scratchfoundation/scratch-render/compare/v2.0.200...v2.0.201) (2025-03-03)


### Bug Fixes

* **deps:** lock file maintenance ([8479d3a](https://github.com/scratchfoundation/scratch-render/commit/8479d3a8a757dac51f04e88e771cdab3bfeddebb))

## [2.0.200](https://github.com/scratchfoundation/scratch-render/compare/v2.0.199...v2.0.200) (2025-03-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.65 ([94b4d01](https://github.com/scratchfoundation/scratch-render/commit/94b4d01cd3c850182f97158088ec3c7c80aefdaa))

## [2.0.199](https://github.com/scratchfoundation/scratch-render/compare/v2.0.198...v2.0.199) (2025-03-02)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.169 ([929b966](https://github.com/scratchfoundation/scratch-render/commit/929b966e557d4269bf76970fac92e55f68606e76))

## [2.0.198](https://github.com/scratchfoundation/scratch-render/compare/v2.0.197...v2.0.198) (2025-03-02)


### Bug Fixes

* **deps:** lock file maintenance ([df4915e](https://github.com/scratchfoundation/scratch-render/commit/df4915e37d8df8c0c7b9c43793595ff400291f5b))

## [2.0.197](https://github.com/scratchfoundation/scratch-render/compare/v2.0.196...v2.0.197) (2025-03-02)


### Bug Fixes

* **deps:** lock file maintenance ([177513f](https://github.com/scratchfoundation/scratch-render/commit/177513f437ddc08c4a5879f204f787f21da2e79e))

## [2.0.196](https://github.com/scratchfoundation/scratch-render/compare/v2.0.195...v2.0.196) (2025-03-01)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.64 ([c9ad0da](https://github.com/scratchfoundation/scratch-render/commit/c9ad0da8a25c569615c184ed400ee6877b991a85))

## [2.0.195](https://github.com/scratchfoundation/scratch-render/compare/v2.0.194...v2.0.195) (2025-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([1649ec0](https://github.com/scratchfoundation/scratch-render/commit/1649ec00b902e13c6f595b402b3e6e4a0426c6c6))

## [2.0.194](https://github.com/scratchfoundation/scratch-render/compare/v2.0.193...v2.0.194) (2025-02-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.63 ([13c6585](https://github.com/scratchfoundation/scratch-render/commit/13c65852283624191fdc48d42111c08254ed3f3b))

## [2.0.193](https://github.com/scratchfoundation/scratch-render/compare/v2.0.192...v2.0.193) (2025-02-28)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.168 ([4d9c6ff](https://github.com/scratchfoundation/scratch-render/commit/4d9c6ff82f495d5094372188ca40ea7f00b7c034))

## [2.0.192](https://github.com/scratchfoundation/scratch-render/compare/v2.0.191...v2.0.192) (2025-02-28)


### Bug Fixes

* **deps:** lock file maintenance ([aadeff9](https://github.com/scratchfoundation/scratch-render/commit/aadeff9e7fb34cbc1dc5a7a869db90ae4fd529ae))

## [2.0.191](https://github.com/scratchfoundation/scratch-render/compare/v2.0.190...v2.0.191) (2025-02-27)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.61 ([dda60c7](https://github.com/scratchfoundation/scratch-render/commit/dda60c7e011736369ee75cc9adffce018cc1d811))

## [2.0.190](https://github.com/scratchfoundation/scratch-render/compare/v2.0.189...v2.0.190) (2025-02-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.60 ([503ef02](https://github.com/scratchfoundation/scratch-render/commit/503ef02adbe11de5edf2f95bf74ab1fd0b802e8f))

## [2.0.189](https://github.com/scratchfoundation/scratch-render/compare/v2.0.188...v2.0.189) (2025-02-25)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.59 ([2c25b5b](https://github.com/scratchfoundation/scratch-render/commit/2c25b5bb1a17466625b819a64c0a4a9f0e4221bc))

## [2.0.188](https://github.com/scratchfoundation/scratch-render/compare/v2.0.187...v2.0.188) (2025-02-24)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.58 ([0ec4ef3](https://github.com/scratchfoundation/scratch-render/commit/0ec4ef3434de3705e63acfb02bb9c684efcbf110))

## [2.0.187](https://github.com/scratchfoundation/scratch-render/compare/v2.0.186...v2.0.187) (2025-02-24)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.167 ([515ba1e](https://github.com/scratchfoundation/scratch-render/commit/515ba1ea14bc401b4d806921d77bb57983ee5d9f))

## [2.0.186](https://github.com/scratchfoundation/scratch-render/compare/v2.0.185...v2.0.186) (2025-02-24)


### Bug Fixes

* **deps:** lock file maintenance ([d414299](https://github.com/scratchfoundation/scratch-render/commit/d4142997269effd7ccc1df95961d930c1b996701))

## [2.0.185](https://github.com/scratchfoundation/scratch-render/compare/v2.0.184...v2.0.185) (2025-02-24)


### Bug Fixes

* **deps:** lock file maintenance ([83e8cb3](https://github.com/scratchfoundation/scratch-render/commit/83e8cb3768ef2cb665d7f4869dad698ebd8bc7f3))

## [2.0.184](https://github.com/scratchfoundation/scratch-render/compare/v2.0.183...v2.0.184) (2025-02-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.57 ([6d54772](https://github.com/scratchfoundation/scratch-render/commit/6d54772b350324ea172a80e3c14e41a866d74680))

## [2.0.183](https://github.com/scratchfoundation/scratch-render/compare/v2.0.182...v2.0.183) (2025-02-23)


### Bug Fixes

* **deps:** lock file maintenance ([281961a](https://github.com/scratchfoundation/scratch-render/commit/281961a05f95855339df9ad439e742ad6e21afee))

## [2.0.182](https://github.com/scratchfoundation/scratch-render/compare/v2.0.181...v2.0.182) (2025-02-22)


### Bug Fixes

* **deps:** lock file maintenance ([4ae27c2](https://github.com/scratchfoundation/scratch-render/commit/4ae27c2fe86a44b8e70cf698ac191244f88b88f9))

## [2.0.181](https://github.com/scratchfoundation/scratch-render/compare/v2.0.180...v2.0.181) (2025-02-21)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.166 ([4523e9e](https://github.com/scratchfoundation/scratch-render/commit/4523e9e4b86f154aef5891420e559dcf2f478d66))

## [2.0.180](https://github.com/scratchfoundation/scratch-render/compare/v2.0.179...v2.0.180) (2025-02-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.56 ([a5f7021](https://github.com/scratchfoundation/scratch-render/commit/a5f7021e053245d0545811c9fb8d68834669b446))

## [2.0.179](https://github.com/scratchfoundation/scratch-render/compare/v2.0.178...v2.0.179) (2025-02-20)


### Bug Fixes

* **deps:** lock file maintenance ([2e5742d](https://github.com/scratchfoundation/scratch-render/commit/2e5742dee7cf893c86a57c4fc4e0b7e51c143ca4))

## [2.0.178](https://github.com/scratchfoundation/scratch-render/compare/v2.0.177...v2.0.178) (2025-02-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.55 ([e8985af](https://github.com/scratchfoundation/scratch-render/commit/e8985af5695903de307ad14d3ce90beb8f2dcab3))

## [2.0.177](https://github.com/scratchfoundation/scratch-render/compare/v2.0.176...v2.0.177) (2025-02-19)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.165 ([97721d1](https://github.com/scratchfoundation/scratch-render/commit/97721d10738924dfc133767299e5db821b3018fe))

## [2.0.176](https://github.com/scratchfoundation/scratch-render/compare/v2.0.175...v2.0.176) (2025-02-19)


### Bug Fixes

* **deps:** lock file maintenance ([84c8b6d](https://github.com/scratchfoundation/scratch-render/commit/84c8b6dba7a89dfdb9e0fe56488a9be10ef7f9b5))

## [2.0.175](https://github.com/scratchfoundation/scratch-render/compare/v2.0.174...v2.0.175) (2025-02-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.54 ([de38190](https://github.com/scratchfoundation/scratch-render/commit/de381901ef39212fa8642606bcba43e8b45ce6bb))

## [2.0.174](https://github.com/scratchfoundation/scratch-render/compare/v2.0.173...v2.0.174) (2025-02-18)


### Bug Fixes

* **deps:** lock file maintenance ([cc271c4](https://github.com/scratchfoundation/scratch-render/commit/cc271c4d8b8c51d587f08de14081a974f36c51c9))

## [2.0.173](https://github.com/scratchfoundation/scratch-render/compare/v2.0.172...v2.0.173) (2025-02-17)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.53 ([8ab5f88](https://github.com/scratchfoundation/scratch-render/commit/8ab5f886a60800c7c3532651eb4ecf73ba92a146))

## [2.0.172](https://github.com/scratchfoundation/scratch-render/compare/v2.0.171...v2.0.172) (2025-02-17)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.164 ([4a49779](https://github.com/scratchfoundation/scratch-render/commit/4a4977951712cb43adf28c7d66077f2683e851ab))

## [2.0.171](https://github.com/scratchfoundation/scratch-render/compare/v2.0.170...v2.0.171) (2025-02-17)


### Bug Fixes

* **deps:** lock file maintenance ([48a67df](https://github.com/scratchfoundation/scratch-render/commit/48a67df4288941b5e58b8f67cec56a2bf93f5d64))

## [2.0.170](https://github.com/scratchfoundation/scratch-render/compare/v2.0.169...v2.0.170) (2025-02-17)


### Bug Fixes

* **deps:** lock file maintenance ([d4c1ec4](https://github.com/scratchfoundation/scratch-render/commit/d4c1ec4e0cf530f54af1b5b5d931d6a78fb11ddc))

## [2.0.169](https://github.com/scratchfoundation/scratch-render/compare/v2.0.168...v2.0.169) (2025-02-17)


### Bug Fixes

* **deps:** lock file maintenance ([a98608d](https://github.com/scratchfoundation/scratch-render/commit/a98608d8bfcca816fb9071854e9698995774b894))

## [2.0.168](https://github.com/scratchfoundation/scratch-render/compare/v2.0.167...v2.0.168) (2025-02-16)


### Bug Fixes

* **deps:** lock file maintenance ([7fdc6ef](https://github.com/scratchfoundation/scratch-render/commit/7fdc6ef17d4b169fbcc153f5dd4e50182d59e56b))

## [2.0.167](https://github.com/scratchfoundation/scratch-render/compare/v2.0.166...v2.0.167) (2025-02-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.52 ([e9760a2](https://github.com/scratchfoundation/scratch-render/commit/e9760a28c5cbcbbe5ccbdd1592d6f6aa5fbcabc2))

## [2.0.166](https://github.com/scratchfoundation/scratch-render/compare/v2.0.165...v2.0.166) (2025-02-15)


### Bug Fixes

* **deps:** lock file maintenance ([7b96701](https://github.com/scratchfoundation/scratch-render/commit/7b96701ede36e55dcd1fbd1426d1b91c3617a29e))
* **deps:** update dependency scratch-render-fonts to v1.0.163 ([678000e](https://github.com/scratchfoundation/scratch-render/commit/678000e32d403072727ada5876f4831e2a88c500))

## [2.0.165](https://github.com/scratchfoundation/scratch-render/compare/v2.0.164...v2.0.165) (2025-02-15)


### Bug Fixes

* **deps:** lock file maintenance ([d6783fc](https://github.com/scratchfoundation/scratch-render/commit/d6783fcb65d4ea84db44b7547c77ed0c7bb14d0a))

## [2.0.164](https://github.com/scratchfoundation/scratch-render/compare/v2.0.163...v2.0.164) (2025-02-14)


### Bug Fixes

* **deps:** lock file maintenance ([03440b1](https://github.com/scratchfoundation/scratch-render/commit/03440b1e36411262d18095452ad6c9169a3aa134))

## [2.0.163](https://github.com/scratchfoundation/scratch-render/compare/v2.0.162...v2.0.163) (2025-02-13)


### Bug Fixes

* **deps:** lock file maintenance ([8303bac](https://github.com/scratchfoundation/scratch-render/commit/8303bac9f7b999f0a73780616706a2e422e38448))

## [2.0.162](https://github.com/scratchfoundation/scratch-render/compare/v2.0.161...v2.0.162) (2025-02-13)


### Bug Fixes

* **deps:** lock file maintenance ([1a1a369](https://github.com/scratchfoundation/scratch-render/commit/1a1a3691f5290155743292084bd888d28c35fe19))

## [2.0.161](https://github.com/scratchfoundation/scratch-render/compare/v2.0.160...v2.0.161) (2025-02-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.50 ([34bd9a0](https://github.com/scratchfoundation/scratch-render/commit/34bd9a0cd050714d4398357fd99a91cdc4811353))

## [2.0.160](https://github.com/scratchfoundation/scratch-render/compare/v2.0.159...v2.0.160) (2025-02-12)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.162 ([71ecad5](https://github.com/scratchfoundation/scratch-render/commit/71ecad5e4c7d09e12b2418609fd6638033eee2aa))

## [2.0.159](https://github.com/scratchfoundation/scratch-render/compare/v2.0.158...v2.0.159) (2025-02-12)


### Bug Fixes

* **deps:** lock file maintenance ([b1b90f9](https://github.com/scratchfoundation/scratch-render/commit/b1b90f9ff4099555aeb35f496c5a795467e31a9b))

## [2.0.158](https://github.com/scratchfoundation/scratch-render/compare/v2.0.157...v2.0.158) (2025-02-10)


### Bug Fixes

* **deps:** lock file maintenance ([2bee376](https://github.com/scratchfoundation/scratch-render/commit/2bee376386cfc6b999aeff1d3a8b9289a074d033))

## [2.0.157](https://github.com/scratchfoundation/scratch-render/compare/v2.0.156...v2.0.157) (2025-02-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.49 ([6537f64](https://github.com/scratchfoundation/scratch-render/commit/6537f64339321c629f66ad76eaeb87a614c0d436))

## [2.0.156](https://github.com/scratchfoundation/scratch-render/compare/v2.0.155...v2.0.156) (2025-02-09)


### Bug Fixes

* **deps:** lock file maintenance ([f95f43d](https://github.com/scratchfoundation/scratch-render/commit/f95f43df8647f250f7e8301d00a1abda893a2cbb))

## [2.0.155](https://github.com/scratchfoundation/scratch-render/compare/v2.0.154...v2.0.155) (2025-02-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.48 ([3f35cb1](https://github.com/scratchfoundation/scratch-render/commit/3f35cb18b52b52cafc7f0d452ae854cf22437cc7))

## [2.0.154](https://github.com/scratchfoundation/scratch-render/compare/v2.0.153...v2.0.154) (2025-02-08)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.161 ([f4f603c](https://github.com/scratchfoundation/scratch-render/commit/f4f603c9f72f0be7534bc55a8a17dba5104bfc7f))

## [2.0.153](https://github.com/scratchfoundation/scratch-render/compare/v2.0.152...v2.0.153) (2025-02-06)


### Bug Fixes

* **deps:** lock file maintenance ([9032419](https://github.com/scratchfoundation/scratch-render/commit/90324192951bdfdf1f0b7e9e77dec279476323dd))

## [2.0.152](https://github.com/scratchfoundation/scratch-render/compare/v2.0.151...v2.0.152) (2025-02-05)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.44 ([26e7a86](https://github.com/scratchfoundation/scratch-render/commit/26e7a864d52a1a6d89ca758c5b43eded608d5313))

## [2.0.151](https://github.com/scratchfoundation/scratch-render/compare/v2.0.150...v2.0.151) (2025-02-05)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.160 ([e2aa647](https://github.com/scratchfoundation/scratch-render/commit/e2aa647519360ffe220d973ba83659a132b5eb71))

## [2.0.150](https://github.com/scratchfoundation/scratch-render/compare/v2.0.149...v2.0.150) (2025-02-05)


### Bug Fixes

* **deps:** lock file maintenance ([206ae13](https://github.com/scratchfoundation/scratch-render/commit/206ae135059b76c55cd3bcc34eef9f66226b946e))

## [2.0.149](https://github.com/scratchfoundation/scratch-render/compare/v2.0.148...v2.0.149) (2025-02-05)


### Bug Fixes

* **deps:** lock file maintenance ([00c856c](https://github.com/scratchfoundation/scratch-render/commit/00c856ca265eb71861798de5ba3f8f175d0ddd11))

## [2.0.148](https://github.com/scratchfoundation/scratch-render/compare/v2.0.147...v2.0.148) (2025-02-04)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.43 ([779d9dc](https://github.com/scratchfoundation/scratch-render/commit/779d9dceedad7e5e30b05b4131fe0748827d9a8d))

## [2.0.147](https://github.com/scratchfoundation/scratch-render/compare/v2.0.146...v2.0.147) (2025-02-04)


### Bug Fixes

* **deps:** lock file maintenance ([12b7190](https://github.com/scratchfoundation/scratch-render/commit/12b7190a13d8df6c9528615934d56a9d31d4f277))

## [2.0.146](https://github.com/scratchfoundation/scratch-render/compare/v2.0.145...v2.0.146) (2025-02-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.42 ([8816fe8](https://github.com/scratchfoundation/scratch-render/commit/8816fe8088bc376c8ff88fb22f7eb096086c4704))

## [2.0.145](https://github.com/scratchfoundation/scratch-render/compare/v2.0.144...v2.0.145) (2025-02-03)


### Bug Fixes

* **deps:** lock file maintenance ([6816878](https://github.com/scratchfoundation/scratch-render/commit/6816878d3ad1b98c73f85e86eb4e50e3e1dbf62d))

## [2.0.144](https://github.com/scratchfoundation/scratch-render/compare/v2.0.143...v2.0.144) (2025-02-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.41 ([55f4713](https://github.com/scratchfoundation/scratch-render/commit/55f471309ab0e39e6ca27983dcb9911fdf8cd4d4))

## [2.0.143](https://github.com/scratchfoundation/scratch-render/compare/v2.0.142...v2.0.143) (2025-02-02)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.159 ([42466d1](https://github.com/scratchfoundation/scratch-render/commit/42466d1367a84e292379a55d0264fcc6c8a0fc10))

## [2.0.142](https://github.com/scratchfoundation/scratch-render/compare/v2.0.141...v2.0.142) (2025-02-02)


### Bug Fixes

* **deps:** lock file maintenance ([3c4ff45](https://github.com/scratchfoundation/scratch-render/commit/3c4ff457daef2dab09194d663394c60285d1e895))

## [2.0.141](https://github.com/scratchfoundation/scratch-render/compare/v2.0.140...v2.0.141) (2025-01-31)


### Bug Fixes

* **deps:** lock file maintenance ([e7012f6](https://github.com/scratchfoundation/scratch-render/commit/e7012f608de88f2124705a05dca82faeef3dec4e))

## [2.0.140](https://github.com/scratchfoundation/scratch-render/compare/v2.0.139...v2.0.140) (2025-01-30)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.40 ([78ec470](https://github.com/scratchfoundation/scratch-render/commit/78ec470040b62283de9f50c90f458c015b7f250c))

## [2.0.139](https://github.com/scratchfoundation/scratch-render/compare/v2.0.138...v2.0.139) (2025-01-29)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.158 ([1b8b872](https://github.com/scratchfoundation/scratch-render/commit/1b8b872157421317c070ef29293be026dca0b7d3))

## [2.0.138](https://github.com/scratchfoundation/scratch-render/compare/v2.0.137...v2.0.138) (2025-01-29)


### Bug Fixes

* **deps:** lock file maintenance ([7255862](https://github.com/scratchfoundation/scratch-render/commit/72558629b72e1cd1a90108feaefbbdf816c588fb))

## [2.0.137](https://github.com/scratchfoundation/scratch-render/compare/v2.0.136...v2.0.137) (2025-01-28)


### Bug Fixes

* **deps:** lock file maintenance ([447d0ce](https://github.com/scratchfoundation/scratch-render/commit/447d0ce07434ec25c7ae274d60ae2747b1bf610a))

## [2.0.136](https://github.com/scratchfoundation/scratch-render/compare/v2.0.135...v2.0.136) (2025-01-28)


### Bug Fixes

* **deps:** lock file maintenance ([e6e5473](https://github.com/scratchfoundation/scratch-render/commit/e6e54736a99832a65ed4544d34cd0bc5b966f6c0))

## [2.0.135](https://github.com/scratchfoundation/scratch-render/compare/v2.0.134...v2.0.135) (2025-01-27)


### Bug Fixes

* **deps:** lock file maintenance ([7fe38ad](https://github.com/scratchfoundation/scratch-render/commit/7fe38ad534f254facbcf3c335514886e1ac51de2))

## [2.0.134](https://github.com/scratchfoundation/scratch-render/compare/v2.0.133...v2.0.134) (2025-01-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.39 ([e71a30a](https://github.com/scratchfoundation/scratch-render/commit/e71a30a4c7fb7f93681405a5029d92972d396d89))

## [2.0.133](https://github.com/scratchfoundation/scratch-render/compare/v2.0.132...v2.0.133) (2025-01-26)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.157 ([654ccd1](https://github.com/scratchfoundation/scratch-render/commit/654ccd16162749c2f3860aef027230c2efd8009b))

## [2.0.132](https://github.com/scratchfoundation/scratch-render/compare/v2.0.131...v2.0.132) (2025-01-26)


### Bug Fixes

* **deps:** lock file maintenance ([c2ddc28](https://github.com/scratchfoundation/scratch-render/commit/c2ddc2826ac453f82e8bde1190a6a3c1ebdf4543))

## [2.0.131](https://github.com/scratchfoundation/scratch-render/compare/v2.0.130...v2.0.131) (2025-01-24)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.37 ([741363d](https://github.com/scratchfoundation/scratch-render/commit/741363dc5ccba095e60a2816abf1b8eb1f108dd3))

## [2.0.130](https://github.com/scratchfoundation/scratch-render/compare/v2.0.129...v2.0.130) (2025-01-24)


### Bug Fixes

* **deps:** lock file maintenance ([d869287](https://github.com/scratchfoundation/scratch-render/commit/d86928783839a19012a66bf7c9f9a59eba852a5a))

## [2.0.129](https://github.com/scratchfoundation/scratch-render/compare/v2.0.128...v2.0.129) (2025-01-23)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.156 ([a789c94](https://github.com/scratchfoundation/scratch-render/commit/a789c94450019818df6833b8f4275baea688373e))

## [2.0.128](https://github.com/scratchfoundation/scratch-render/compare/v2.0.127...v2.0.128) (2025-01-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.36 ([8b1d1d3](https://github.com/scratchfoundation/scratch-render/commit/8b1d1d36a47e6ffc3556d452544da7a5d41c9630))

## [2.0.127](https://github.com/scratchfoundation/scratch-render/compare/v2.0.126...v2.0.127) (2025-01-23)


### Bug Fixes

* **deps:** lock file maintenance ([eb82b19](https://github.com/scratchfoundation/scratch-render/commit/eb82b1991f09c5b21d1610c4592e55196a8b4d51))

## [2.0.126](https://github.com/scratchfoundation/scratch-render/compare/v2.0.125...v2.0.126) (2025-01-23)


### Bug Fixes

* **deps:** lock file maintenance ([b4c9ae5](https://github.com/scratchfoundation/scratch-render/commit/b4c9ae55d11b450f22b019a9d61d927184cfaabf))

## [2.0.125](https://github.com/scratchfoundation/scratch-render/compare/v2.0.124...v2.0.125) (2025-01-23)


### Bug Fixes

* **deps:** lock file maintenance ([6df22cc](https://github.com/scratchfoundation/scratch-render/commit/6df22ccbd5429950127a003fb07620674a2dd316))

## [2.0.124](https://github.com/scratchfoundation/scratch-render/compare/v2.0.123...v2.0.124) (2025-01-22)


### Bug Fixes

* **deps:** lock file maintenance ([406d306](https://github.com/scratchfoundation/scratch-render/commit/406d30686df55a7fc15ce1fc74325a49c5f61978))

## [2.0.123](https://github.com/scratchfoundation/scratch-render/compare/v2.0.122...v2.0.123) (2025-01-21)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.155 ([0f9ade6](https://github.com/scratchfoundation/scratch-render/commit/0f9ade667d5dab5430f08c01a691fd3e1acfccd6))

## [2.0.122](https://github.com/scratchfoundation/scratch-render/compare/v2.0.121...v2.0.122) (2025-01-21)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.35 ([db20112](https://github.com/scratchfoundation/scratch-render/commit/db20112bdaf3fc4dd27bd1312c624dc1e3761e2e))

## [2.0.121](https://github.com/scratchfoundation/scratch-render/compare/v2.0.120...v2.0.121) (2025-01-21)


### Bug Fixes

* **deps:** lock file maintenance ([9f6d450](https://github.com/scratchfoundation/scratch-render/commit/9f6d4504feb2d84b9e51ba3df2e5b72418dee4ce))

## [2.0.120](https://github.com/scratchfoundation/scratch-render/compare/v2.0.119...v2.0.120) (2025-01-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.34 ([9f7ce6f](https://github.com/scratchfoundation/scratch-render/commit/9f7ce6f7e09e89cbf080989d78d15e0c154f9178))

## [2.0.119](https://github.com/scratchfoundation/scratch-render/compare/v2.0.118...v2.0.119) (2025-01-20)


### Bug Fixes

* **deps:** lock file maintenance ([9c9b945](https://github.com/scratchfoundation/scratch-render/commit/9c9b945c66d77b9e26eaa0249199acc6b90644a6))

## [2.0.118](https://github.com/scratchfoundation/scratch-render/compare/v2.0.117...v2.0.118) (2025-01-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.33 ([8e289e0](https://github.com/scratchfoundation/scratch-render/commit/8e289e03a28c03e146d9bb22a7ee759da247f3d8))

## [2.0.117](https://github.com/scratchfoundation/scratch-render/compare/v2.0.116...v2.0.117) (2025-01-19)


### Bug Fixes

* **deps:** lock file maintenance ([861f3a0](https://github.com/scratchfoundation/scratch-render/commit/861f3a028b529402ecf84c075dad1a46b7363683))

## [2.0.116](https://github.com/scratchfoundation/scratch-render/compare/v2.0.115...v2.0.116) (2025-01-19)


### Bug Fixes

* **deps:** lock file maintenance ([65540a7](https://github.com/scratchfoundation/scratch-render/commit/65540a747da49e5c708745491a0d426422a8626a))

## [2.0.115](https://github.com/scratchfoundation/scratch-render/compare/v2.0.114...v2.0.115) (2025-01-19)


### Bug Fixes

* **deps:** lock file maintenance ([905f4e1](https://github.com/scratchfoundation/scratch-render/commit/905f4e1ea6c2998200d1f562e11a2d5c67eee0dd))

## [2.0.114](https://github.com/scratchfoundation/scratch-render/compare/v2.0.113...v2.0.114) (2025-01-18)


### Bug Fixes

* **deps:** lock file maintenance ([3a0f16c](https://github.com/scratchfoundation/scratch-render/commit/3a0f16cb3b2cd7b5fe219584638614c316b1e759))

## [2.0.113](https://github.com/scratchfoundation/scratch-render/compare/v2.0.112...v2.0.113) (2025-01-17)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.154 ([7591add](https://github.com/scratchfoundation/scratch-render/commit/7591addc2fc106fc270b0cdda35d9f5941927419))

## [2.0.112](https://github.com/scratchfoundation/scratch-render/compare/v2.0.111...v2.0.112) (2025-01-17)


### Bug Fixes

* **deps:** lock file maintenance ([8333f00](https://github.com/scratchfoundation/scratch-render/commit/8333f00fbc61908f8a576fc622c93458cadd8de1))

## [2.0.111](https://github.com/scratchfoundation/scratch-render/compare/v2.0.110...v2.0.111) (2025-01-16)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.32 ([122dd66](https://github.com/scratchfoundation/scratch-render/commit/122dd66e8c306f8b55931e6237630cdd386735dc))

## [2.0.110](https://github.com/scratchfoundation/scratch-render/compare/v2.0.109...v2.0.110) (2025-01-16)


### Bug Fixes

* **deps:** lock file maintenance ([903f5ab](https://github.com/scratchfoundation/scratch-render/commit/903f5ab435520dbb995b7ac84903ff6d69fbc535))

## [2.0.109](https://github.com/scratchfoundation/scratch-render/compare/v2.0.108...v2.0.109) (2025-01-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.31 ([81ba68e](https://github.com/scratchfoundation/scratch-render/commit/81ba68eb7a3492fa2c293552cbf9fff25ba4b366))

## [2.0.108](https://github.com/scratchfoundation/scratch-render/compare/v2.0.107...v2.0.108) (2025-01-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.30 ([c683f08](https://github.com/scratchfoundation/scratch-render/commit/c683f08e41339fabe2f7f025a00a6faaabc33b01))

## [2.0.107](https://github.com/scratchfoundation/scratch-render/compare/v2.0.106...v2.0.107) (2025-01-14)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.153 ([c9e3b4e](https://github.com/scratchfoundation/scratch-render/commit/c9e3b4ee40d066ab3bf9d03b185c2ab63c863199))

## [2.0.106](https://github.com/scratchfoundation/scratch-render/compare/v2.0.105...v2.0.106) (2025-01-14)


### Bug Fixes

* **deps:** lock file maintenance ([9b81644](https://github.com/scratchfoundation/scratch-render/commit/9b8164447612a8855af312815b1c69dd38167bf7))

## [2.0.105](https://github.com/scratchfoundation/scratch-render/compare/v2.0.104...v2.0.105) (2025-01-13)


### Bug Fixes

* **deps:** lock file maintenance ([549d723](https://github.com/scratchfoundation/scratch-render/commit/549d72383eb0cefecb5a85235e4877ad643ef81d))

## [2.0.104](https://github.com/scratchfoundation/scratch-render/compare/v2.0.103...v2.0.104) (2025-01-12)


### Bug Fixes

* **deps:** lock file maintenance ([204d279](https://github.com/scratchfoundation/scratch-render/commit/204d279a003f4b3bb1797aff11fd973166ef2b50))

## [2.0.103](https://github.com/scratchfoundation/scratch-render/compare/v2.0.102...v2.0.103) (2025-01-12)


### Bug Fixes

* **deps:** lock file maintenance ([c908db3](https://github.com/scratchfoundation/scratch-render/commit/c908db371269267e17304f04e17cee7dd64f77d2))

## [2.0.102](https://github.com/scratchfoundation/scratch-render/compare/v2.0.101...v2.0.102) (2025-01-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.29 ([c3b9141](https://github.com/scratchfoundation/scratch-render/commit/c3b9141ef89efa1c8662cd1ad78497e2938824ef))

## [2.0.101](https://github.com/scratchfoundation/scratch-render/compare/v2.0.100...v2.0.101) (2025-01-11)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.152 ([c4cc9ea](https://github.com/scratchfoundation/scratch-render/commit/c4cc9ea912c64d3d66b466a0f037e3a57b0c963a))

## [2.0.100](https://github.com/scratchfoundation/scratch-render/compare/v2.0.99...v2.0.100) (2025-01-11)


### Bug Fixes

* **deps:** lock file maintenance ([eed5537](https://github.com/scratchfoundation/scratch-render/commit/eed55376814b26695dfc5da8a2e921cc1fe79727))

## [2.0.99](https://github.com/scratchfoundation/scratch-render/compare/v2.0.98...v2.0.99) (2025-01-11)


### Bug Fixes

* **deps:** lock file maintenance ([52a776d](https://github.com/scratchfoundation/scratch-render/commit/52a776d71a036362faccb042f1296f5165382953))

## [2.0.98](https://github.com/scratchfoundation/scratch-render/compare/v2.0.97...v2.0.98) (2025-01-11)


### Bug Fixes

* **deps:** lock file maintenance ([9fddbc9](https://github.com/scratchfoundation/scratch-render/commit/9fddbc93479cd0bf1f1827f69b8870fc52dada19))

## [2.0.97](https://github.com/scratchfoundation/scratch-render/compare/v2.0.96...v2.0.97) (2025-01-10)


### Bug Fixes

* **deps:** lock file maintenance ([952777c](https://github.com/scratchfoundation/scratch-render/commit/952777c2df46e90b4940c8494cb2b14cf006f505))
* **deps:** update dependency scratch-storage to v4.0.27 ([1b7b61e](https://github.com/scratchfoundation/scratch-render/commit/1b7b61e00580b97168702377b8d526b14189fb4f))

## [2.0.96](https://github.com/scratchfoundation/scratch-render/compare/v2.0.95...v2.0.96) (2025-01-10)


### Bug Fixes

* **deps:** lock file maintenance ([af782c1](https://github.com/scratchfoundation/scratch-render/commit/af782c1f270a65281717265d8a152f914bffe390))

## [2.0.95](https://github.com/scratchfoundation/scratch-render/compare/v2.0.94...v2.0.95) (2025-01-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.26 ([adbf438](https://github.com/scratchfoundation/scratch-render/commit/adbf4384c45d31ef914d6583f57eb5438beff040))

## [2.0.94](https://github.com/scratchfoundation/scratch-render/compare/v2.0.93...v2.0.94) (2025-01-09)


### Bug Fixes

* **deps:** lock file maintenance ([3d94327](https://github.com/scratchfoundation/scratch-render/commit/3d943279a05aeb25109ded034a4462b4584e8664))
* **deps:** update dependency scratch-storage to v4.0.25 ([9ae127a](https://github.com/scratchfoundation/scratch-render/commit/9ae127aedcba2799b11b9c3b1b2069e25eb87484))

## [2.0.93](https://github.com/scratchfoundation/scratch-render/compare/v2.0.92...v2.0.93) (2025-01-09)


### Bug Fixes

* **deps:** lock file maintenance ([92986cd](https://github.com/scratchfoundation/scratch-render/commit/92986cdec8761c6e3b6487b72e5ea2044b0bd4ec))

## [2.0.92](https://github.com/scratchfoundation/scratch-render/compare/v2.0.91...v2.0.92) (2025-01-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.24 ([111f967](https://github.com/scratchfoundation/scratch-render/commit/111f9672c3249235cf6ee783b356f09f8400d218))

## [2.0.91](https://github.com/scratchfoundation/scratch-render/compare/v2.0.90...v2.0.91) (2025-01-06)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.151 ([dac311b](https://github.com/scratchfoundation/scratch-render/commit/dac311b04a36964ca50ee39ddeb0a8b70e63ed15))

## [2.0.90](https://github.com/scratchfoundation/scratch-render/compare/v2.0.89...v2.0.90) (2025-01-06)


### Bug Fixes

* **deps:** lock file maintenance ([5b429e9](https://github.com/scratchfoundation/scratch-render/commit/5b429e93e34eb97b39a7a9f9f64f154ebfa92a43))

## [2.0.89](https://github.com/scratchfoundation/scratch-render/compare/v2.0.88...v2.0.89) (2025-01-05)


### Bug Fixes

* **deps:** lock file maintenance ([980f68a](https://github.com/scratchfoundation/scratch-render/commit/980f68ae70f702ba33553d9593ad9c374b988fb7))

## [2.0.88](https://github.com/scratchfoundation/scratch-render/compare/v2.0.87...v2.0.88) (2025-01-05)


### Bug Fixes

* **deps:** lock file maintenance ([349d435](https://github.com/scratchfoundation/scratch-render/commit/349d435754b3ac73acbc789dc9477d004a6b7d78))

## [2.0.87](https://github.com/scratchfoundation/scratch-render/compare/v2.0.86...v2.0.87) (2025-01-04)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.150 ([fcb530a](https://github.com/scratchfoundation/scratch-render/commit/fcb530a1d59498d66029cbd3c9b2ecdb6f518d4d))

## [2.0.86](https://github.com/scratchfoundation/scratch-render/compare/v2.0.85...v2.0.86) (2025-01-04)


### Bug Fixes

* **deps:** lock file maintenance ([bf0d5fc](https://github.com/scratchfoundation/scratch-render/commit/bf0d5fc58c2d58dbac8f44b089f036f6e6cef240))

## [2.0.85](https://github.com/scratchfoundation/scratch-render/compare/v2.0.84...v2.0.85) (2025-01-04)


### Bug Fixes

* **deps:** lock file maintenance ([7b7f31d](https://github.com/scratchfoundation/scratch-render/commit/7b7f31dda2afb6b8a846a3f010b72cc79b9edb09))

## [2.0.84](https://github.com/scratchfoundation/scratch-render/compare/v2.0.83...v2.0.84) (2025-01-03)


### Bug Fixes

* **deps:** lock file maintenance ([1e191cd](https://github.com/scratchfoundation/scratch-render/commit/1e191cde3723902f9e4b37bc5092e598b475c247))

## [2.0.83](https://github.com/scratchfoundation/scratch-render/compare/v2.0.82...v2.0.83) (2025-01-03)


### Bug Fixes

* **deps:** lock file maintenance ([604d9ee](https://github.com/scratchfoundation/scratch-render/commit/604d9ee8276dd635519978cdb37af4ce15c53950))

## [2.0.82](https://github.com/scratchfoundation/scratch-render/compare/v2.0.81...v2.0.82) (2025-01-02)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.149 ([d82c717](https://github.com/scratchfoundation/scratch-render/commit/d82c71733431205c246d5e0e94e8233fc1956440))

## [2.0.81](https://github.com/scratchfoundation/scratch-render/compare/v2.0.80...v2.0.81) (2025-01-02)


### Bug Fixes

* **deps:** lock file maintenance ([3ff9d56](https://github.com/scratchfoundation/scratch-render/commit/3ff9d564edef49c2fd5a569287518609508ff9bf))

## [2.0.80](https://github.com/scratchfoundation/scratch-render/compare/v2.0.79...v2.0.80) (2025-01-01)


### Bug Fixes

* **deps:** lock file maintenance ([85e2b88](https://github.com/scratchfoundation/scratch-render/commit/85e2b88a52384e7b625391dcea6f6bf9d925075a))

## [2.0.79](https://github.com/scratchfoundation/scratch-render/compare/v2.0.78...v2.0.79) (2025-01-01)


### Bug Fixes

* **deps:** lock file maintenance ([d971785](https://github.com/scratchfoundation/scratch-render/commit/d97178585da80cb6e12b7bbd1e54798b4bdaffef))

## [2.0.78](https://github.com/scratchfoundation/scratch-render/compare/v2.0.77...v2.0.78) (2024-12-31)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.148 ([f10ab9c](https://github.com/scratchfoundation/scratch-render/commit/f10ab9cbf8945fb6242dd15eb86981558b2c1b23))

## [2.0.77](https://github.com/scratchfoundation/scratch-render/compare/v2.0.76...v2.0.77) (2024-12-31)


### Bug Fixes

* **deps:** lock file maintenance ([cb68507](https://github.com/scratchfoundation/scratch-render/commit/cb68507c94692972efa4ccdb318213761fb3e687))

## [2.0.76](https://github.com/scratchfoundation/scratch-render/compare/v2.0.75...v2.0.76) (2024-12-30)


### Bug Fixes

* **deps:** lock file maintenance ([c027bc6](https://github.com/scratchfoundation/scratch-render/commit/c027bc6d16975ec694561f78a2e45eb6de8681f9))

## [2.0.75](https://github.com/scratchfoundation/scratch-render/compare/v2.0.74...v2.0.75) (2024-12-29)


### Bug Fixes

* **deps:** lock file maintenance ([62addbb](https://github.com/scratchfoundation/scratch-render/commit/62addbbde25000c601060e6e5c60c0ce7da3be9a))

## [2.0.74](https://github.com/scratchfoundation/scratch-render/compare/v2.0.73...v2.0.74) (2024-12-29)


### Bug Fixes

* **deps:** lock file maintenance ([241983b](https://github.com/scratchfoundation/scratch-render/commit/241983b32ccc8153c8ab7723427f7f76164ef1a5))

## [2.0.73](https://github.com/scratchfoundation/scratch-render/compare/v2.0.72...v2.0.73) (2024-12-28)


### Bug Fixes

* **deps:** lock file maintenance ([141d52f](https://github.com/scratchfoundation/scratch-render/commit/141d52fdbc086beaa3b0e555b9f4b2bbbf398ac3))

## [2.0.72](https://github.com/scratchfoundation/scratch-render/compare/v2.0.71...v2.0.72) (2024-12-28)


### Bug Fixes

* **deps:** lock file maintenance ([43ba942](https://github.com/scratchfoundation/scratch-render/commit/43ba942b4dad5ffcf9f5d4ef5c1adf1d945ecf2c))

## [2.0.71](https://github.com/scratchfoundation/scratch-render/compare/v2.0.70...v2.0.71) (2024-12-27)


### Bug Fixes

* **deps:** lock file maintenance ([0867b41](https://github.com/scratchfoundation/scratch-render/commit/0867b41c5c15fcd9fe8400c4d6906a6988e9a56f))

## [2.0.70](https://github.com/scratchfoundation/scratch-render/compare/v2.0.69...v2.0.70) (2024-12-27)


### Bug Fixes

* **deps:** lock file maintenance ([28764c2](https://github.com/scratchfoundation/scratch-render/commit/28764c2f27e58daa17457b05ebaff9374c46e1bc))

## [2.0.69](https://github.com/scratchfoundation/scratch-render/compare/v2.0.68...v2.0.69) (2024-12-26)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.147 ([735573a](https://github.com/scratchfoundation/scratch-render/commit/735573a6e01dc4a53d8f011bd6acb350a21ab5e8))

## [2.0.68](https://github.com/scratchfoundation/scratch-render/compare/v2.0.67...v2.0.68) (2024-12-26)


### Bug Fixes

* **deps:** lock file maintenance ([7e6f9ad](https://github.com/scratchfoundation/scratch-render/commit/7e6f9ad2abc9f67a2c7342cf12d8a96c268b9464))

## [2.0.67](https://github.com/scratchfoundation/scratch-render/compare/v2.0.66...v2.0.67) (2024-12-26)


### Bug Fixes

* **deps:** lock file maintenance ([052c0f7](https://github.com/scratchfoundation/scratch-render/commit/052c0f79568f94029741b09b5542ce235ee2c130))

## [2.0.66](https://github.com/scratchfoundation/scratch-render/compare/v2.0.65...v2.0.66) (2024-12-25)


### Bug Fixes

* **deps:** lock file maintenance ([e40b829](https://github.com/scratchfoundation/scratch-render/commit/e40b8291d9893921b7bfaf2a6d85c43497b81c9b))

## [2.0.65](https://github.com/scratchfoundation/scratch-render/compare/v2.0.64...v2.0.65) (2024-12-25)


### Bug Fixes

* **deps:** lock file maintenance ([9f0863c](https://github.com/scratchfoundation/scratch-render/commit/9f0863c0da5ed01ffabd461bcc15e5edb414c0ca))

## [2.0.64](https://github.com/scratchfoundation/scratch-render/compare/v2.0.63...v2.0.64) (2024-12-24)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.146 ([6ee13c2](https://github.com/scratchfoundation/scratch-render/commit/6ee13c24c69469a192e9f6759b5ab44fbda93dc0))

## [2.0.63](https://github.com/scratchfoundation/scratch-render/compare/v2.0.62...v2.0.63) (2024-12-24)


### Bug Fixes

* **deps:** lock file maintenance ([3ea448b](https://github.com/scratchfoundation/scratch-render/commit/3ea448bb29900a1e58ac0f60ce91fa16e726d951))

## [2.0.62](https://github.com/scratchfoundation/scratch-render/compare/v2.0.61...v2.0.62) (2024-12-24)


### Bug Fixes

* **deps:** lock file maintenance ([7c0e0e9](https://github.com/scratchfoundation/scratch-render/commit/7c0e0e957c8b8d6826b4b5bdef2f51221710b22d))

## [2.0.61](https://github.com/scratchfoundation/scratch-render/compare/v2.0.60...v2.0.61) (2024-12-23)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.145 ([1c165dc](https://github.com/scratchfoundation/scratch-render/commit/1c165dc70397aebe67466ad6a1190673e8089374))

## [2.0.60](https://github.com/scratchfoundation/scratch-render/compare/v2.0.59...v2.0.60) (2024-12-23)


### Bug Fixes

* **deps:** lock file maintenance ([332c820](https://github.com/scratchfoundation/scratch-render/commit/332c8203b806e31ed9eb3094b0f01e6374691e95))

## [2.0.59](https://github.com/scratchfoundation/scratch-render/compare/v2.0.58...v2.0.59) (2024-12-23)


### Bug Fixes

* **deps:** lock file maintenance ([0994951](https://github.com/scratchfoundation/scratch-render/commit/0994951e9f8e2364a92bf3a4268053bc5f5de99c))

## [2.0.58](https://github.com/scratchfoundation/scratch-render/compare/v2.0.57...v2.0.58) (2024-12-22)


### Bug Fixes

* **deps:** lock file maintenance ([eb3f7a7](https://github.com/scratchfoundation/scratch-render/commit/eb3f7a76cd0ae719f2bd0a9fdac49a4b635b15ad))

## [2.0.57](https://github.com/scratchfoundation/scratch-render/compare/v2.0.56...v2.0.57) (2024-12-22)


### Bug Fixes

* **deps:** lock file maintenance ([54a0c99](https://github.com/scratchfoundation/scratch-render/commit/54a0c9979a8d0b16cb6dc3ad8df1d09d29c22037))

## [2.0.56](https://github.com/scratchfoundation/scratch-render/compare/v2.0.55...v2.0.56) (2024-12-21)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.144 ([2b521e5](https://github.com/scratchfoundation/scratch-render/commit/2b521e56aba1bcc2deefb04e80ec5db5bd717073))

## [2.0.55](https://github.com/scratchfoundation/scratch-render/compare/v2.0.54...v2.0.55) (2024-12-21)


### Bug Fixes

* **deps:** update dependency scratch-storage to v4.0.23 ([ebaaa29](https://github.com/scratchfoundation/scratch-render/commit/ebaaa29b8dfbb42697f6f7d45632f6eff4cb89e2))

## [2.0.54](https://github.com/scratchfoundation/scratch-render/compare/v2.0.53...v2.0.54) (2024-12-20)


### Bug Fixes

* **deps:** lock file maintenance ([331bbcf](https://github.com/scratchfoundation/scratch-render/commit/331bbcf5c069b6447ab618068495a882b171b1c0))

## [2.0.53](https://github.com/scratchfoundation/scratch-render/compare/v2.0.52...v2.0.53) (2024-12-20)


### Bug Fixes

* **deps:** lock file maintenance ([e93ae8f](https://github.com/scratchfoundation/scratch-render/commit/e93ae8ff9b2b803a9b6d1808aa90e4ed1e75ae78))

## [2.0.52](https://github.com/scratchfoundation/scratch-render/compare/v2.0.51...v2.0.52) (2024-12-19)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.143 ([f31c4b7](https://github.com/scratchfoundation/scratch-render/commit/f31c4b7a12e170aa8fe13d35ac1d5c0c0141c8ed))

## [2.0.51](https://github.com/scratchfoundation/scratch-render/compare/v2.0.50...v2.0.51) (2024-12-19)


### Bug Fixes

* **deps:** lock file maintenance ([ac6b254](https://github.com/scratchfoundation/scratch-render/commit/ac6b25465412aa21916371526f69e0ff14b6392c))

## [2.0.50](https://github.com/scratchfoundation/scratch-render/compare/v2.0.49...v2.0.50) (2024-12-18)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.142 ([f8b5264](https://github.com/scratchfoundation/scratch-render/commit/f8b5264296b610aa69eaeec43e0f3188d61d39bb))

## [2.0.49](https://github.com/scratchfoundation/scratch-render/compare/v2.0.48...v2.0.49) (2024-12-18)


### Bug Fixes

* **deps:** lock file maintenance ([1cb2404](https://github.com/scratchfoundation/scratch-render/commit/1cb2404234570f56a93a16c0071cfe52b6655a09))

## [2.0.48](https://github.com/scratchfoundation/scratch-render/compare/v2.0.47...v2.0.48) (2024-12-17)


### Bug Fixes

* **deps:** lock file maintenance ([c5a9eeb](https://github.com/scratchfoundation/scratch-render/commit/c5a9eeb8c3def306e8ed17ad9c478e5ca9f8f28a))

## [2.0.47](https://github.com/scratchfoundation/scratch-render/compare/v2.0.46...v2.0.47) (2024-12-16)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.141 ([9de5972](https://github.com/scratchfoundation/scratch-render/commit/9de597240153a38e8fe3694bf616a209fd9ea878))

## [2.0.46](https://github.com/scratchfoundation/scratch-render/compare/v2.0.45...v2.0.46) (2024-12-16)


### Bug Fixes

* **deps:** lock file maintenance ([ed9f749](https://github.com/scratchfoundation/scratch-render/commit/ed9f7494f17771f05dd0bc3b06fd544ef5e9b888))

## [2.0.45](https://github.com/scratchfoundation/scratch-render/compare/v2.0.44...v2.0.45) (2024-12-16)


### Bug Fixes

* **deps:** lock file maintenance ([bd9d733](https://github.com/scratchfoundation/scratch-render/commit/bd9d733aad8eef92889948dea2f6e43eedb7d58e))

## [2.0.44](https://github.com/scratchfoundation/scratch-render/compare/v2.0.43...v2.0.44) (2024-12-15)


### Bug Fixes

* **deps:** lock file maintenance ([2d31ea2](https://github.com/scratchfoundation/scratch-render/commit/2d31ea2affba88ed950287b7d49619f80ead6fa9))

## [2.0.43](https://github.com/scratchfoundation/scratch-render/compare/v2.0.42...v2.0.43) (2024-12-15)


### Bug Fixes

* **deps:** lock file maintenance ([726f223](https://github.com/scratchfoundation/scratch-render/commit/726f2232eaaea9b09778aefad04d92d32df3a148))

## [2.0.42](https://github.com/scratchfoundation/scratch-render/compare/v2.0.41...v2.0.42) (2024-12-14)


### Bug Fixes

* **deps:** lock file maintenance ([c42075f](https://github.com/scratchfoundation/scratch-render/commit/c42075f39b7acd05398cc545209f529585480ec6))
* **deps:** update dependency scratch-render-fonts to v1.0.140 ([9332b10](https://github.com/scratchfoundation/scratch-render/commit/9332b101331de6918d21f3496b0d5bbbb0bdf3fc))

## [2.0.41](https://github.com/scratchfoundation/scratch-render/compare/v2.0.40...v2.0.41) (2024-12-14)


### Bug Fixes

* **deps:** lock file maintenance ([324fea8](https://github.com/scratchfoundation/scratch-render/commit/324fea8f9b7a209000274f3169841ed8fd26aa79))

## [2.0.40](https://github.com/scratchfoundation/scratch-render/compare/v2.0.39...v2.0.40) (2024-12-13)


### Bug Fixes

* **deps:** lock file maintenance ([286c138](https://github.com/scratchfoundation/scratch-render/commit/286c138904f47e0bde287d3be5ba169033e52236))

## [2.0.39](https://github.com/scratchfoundation/scratch-render/compare/v2.0.38...v2.0.39) (2024-12-13)


### Bug Fixes

* **deps:** lock file maintenance ([11fea51](https://github.com/scratchfoundation/scratch-render/commit/11fea51614665d6d40493149b3359a7253b6aaa5))

## [2.0.38](https://github.com/scratchfoundation/scratch-render/compare/v2.0.37...v2.0.38) (2024-12-12)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.139 ([c451eec](https://github.com/scratchfoundation/scratch-render/commit/c451eec88896980f469e612391ea02809f277864))

## [2.0.37](https://github.com/scratchfoundation/scratch-render/compare/v2.0.36...v2.0.37) (2024-12-12)


### Bug Fixes

* **deps:** lock file maintenance ([8ac8638](https://github.com/scratchfoundation/scratch-render/commit/8ac8638006bf8cc2cbe76ed55c07bbb51bb6e3b5))

## [2.0.36](https://github.com/scratchfoundation/scratch-render/compare/v2.0.35...v2.0.36) (2024-12-11)


### Bug Fixes

* **deps:** lock file maintenance ([130690c](https://github.com/scratchfoundation/scratch-render/commit/130690cb7f8f44c71f4057dd8381dba325fd1a08))

## [2.0.35](https://github.com/scratchfoundation/scratch-render/compare/v2.0.34...v2.0.35) (2024-12-11)


### Bug Fixes

* **deps:** lock file maintenance ([36f82dd](https://github.com/scratchfoundation/scratch-render/commit/36f82dd3d6db61c0eacdb0dbd9ab074e2216f98e))

## [2.0.34](https://github.com/scratchfoundation/scratch-render/compare/v2.0.33...v2.0.34) (2024-12-10)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.138 ([ed0b2f1](https://github.com/scratchfoundation/scratch-render/commit/ed0b2f17067a222103c10173235b99f7a7d6c0ef))

## [2.0.33](https://github.com/scratchfoundation/scratch-render/compare/v2.0.32...v2.0.33) (2024-12-10)


### Bug Fixes

* **deps:** lock file maintenance ([ed19d0a](https://github.com/scratchfoundation/scratch-render/commit/ed19d0aad8afe427f7ed2a2dd9ad0ac581ad6f52))

## [2.0.32](https://github.com/scratchfoundation/scratch-render/compare/v2.0.31...v2.0.32) (2024-12-10)


### Bug Fixes

* **deps:** lock file maintenance ([54e92a4](https://github.com/scratchfoundation/scratch-render/commit/54e92a4ffd25a32aa804e892e151f20522fd5b28))

## [2.0.31](https://github.com/scratchfoundation/scratch-render/compare/v2.0.30...v2.0.31) (2024-12-09)


### Bug Fixes

* **deps:** lock file maintenance ([8843f6b](https://github.com/scratchfoundation/scratch-render/commit/8843f6b9c8b88c302424706154a714c349197a44))

## [2.0.30](https://github.com/scratchfoundation/scratch-render/compare/v2.0.29...v2.0.30) (2024-12-08)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.137 ([d38364e](https://github.com/scratchfoundation/scratch-render/commit/d38364ea5a14f0c20932ce9daccf89b4f0ce8335))

## [2.0.29](https://github.com/scratchfoundation/scratch-render/compare/v2.0.28...v2.0.29) (2024-12-08)


### Bug Fixes

* **deps:** lock file maintenance ([fce55ee](https://github.com/scratchfoundation/scratch-render/commit/fce55ee70ac4efe63d927b6129ee80a07aac5aea))

## [2.0.28](https://github.com/scratchfoundation/scratch-render/compare/v2.0.27...v2.0.28) (2024-12-08)


### Bug Fixes

* **deps:** lock file maintenance ([2e5791e](https://github.com/scratchfoundation/scratch-render/commit/2e5791ef7cb07a1b8c1c3be6d69e565bf360a314))

## [2.0.27](https://github.com/scratchfoundation/scratch-render/compare/v2.0.26...v2.0.27) (2024-12-07)


### Bug Fixes

* **deps:** lock file maintenance ([8ff88fe](https://github.com/scratchfoundation/scratch-render/commit/8ff88feff0b854f1b724a68abe9ef3077e317e34))

## [2.0.26](https://github.com/scratchfoundation/scratch-render/compare/v2.0.25...v2.0.26) (2024-12-07)


### Bug Fixes

* **deps:** lock file maintenance ([64e65f7](https://github.com/scratchfoundation/scratch-render/commit/64e65f7095f448030fdc04fbce379a8ac85821d8))

## [2.0.25](https://github.com/scratchfoundation/scratch-render/compare/v2.0.24...v2.0.25) (2024-12-06)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.136 ([c52dd1c](https://github.com/scratchfoundation/scratch-render/commit/c52dd1c1277e4b448d7c78812417aebb0e100dda))

## [2.0.24](https://github.com/scratchfoundation/scratch-render/compare/v2.0.23...v2.0.24) (2024-12-06)


### Bug Fixes

* **deps:** lock file maintenance ([60397f7](https://github.com/scratchfoundation/scratch-render/commit/60397f733dd61bd2242b55f4659fad5690dad79d))

## [2.0.23](https://github.com/scratchfoundation/scratch-render/compare/v2.0.22...v2.0.23) (2024-12-06)


### Bug Fixes

* **deps:** lock file maintenance ([6395982](https://github.com/scratchfoundation/scratch-render/commit/6395982a69ff35320f25a8970d3e358bd2a86dbe))

## [2.0.22](https://github.com/scratchfoundation/scratch-render/compare/v2.0.21...v2.0.22) (2024-12-05)


### Bug Fixes

* **deps:** update dependency scratch-svg-renderer to v3 ([452556b](https://github.com/scratchfoundation/scratch-render/commit/452556b7e5487ea36c4a06d90d7ebf1fec063908))

## [2.0.21](https://github.com/scratchfoundation/scratch-render/compare/v2.0.20...v2.0.21) (2024-12-05)


### Bug Fixes

* **deps:** lock file maintenance ([08682c1](https://github.com/scratchfoundation/scratch-render/commit/08682c13f16f7bd8f6a3af181f24b330cc7bd8fe))

## [2.0.20](https://github.com/scratchfoundation/scratch-render/compare/v2.0.19...v2.0.20) (2024-12-05)


### Bug Fixes

* **deps:** lock file maintenance ([3be1522](https://github.com/scratchfoundation/scratch-render/commit/3be15225131adb530fc1c46cf538efc19697c127))

## [2.0.19](https://github.com/scratchfoundation/scratch-render/compare/v2.0.18...v2.0.19) (2024-12-04)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.135 ([e70f207](https://github.com/scratchfoundation/scratch-render/commit/e70f207d6772e32295ec1850071dc36d6a3caced))

## [2.0.18](https://github.com/scratchfoundation/scratch-render/compare/v2.0.17...v2.0.18) (2024-12-04)


### Bug Fixes

* **deps:** lock file maintenance ([b0c8a35](https://github.com/scratchfoundation/scratch-render/commit/b0c8a35aa2fc95f68fbd94ceceae928629f1a5a4))

## [2.0.17](https://github.com/scratchfoundation/scratch-render/compare/v2.0.16...v2.0.17) (2024-12-03)


### Bug Fixes

* **deps:** lock file maintenance ([e36766d](https://github.com/scratchfoundation/scratch-render/commit/e36766d600214b3671b1520cbac314e5d64c446a))

## [2.0.16](https://github.com/scratchfoundation/scratch-render/compare/v2.0.15...v2.0.16) (2024-12-03)


### Bug Fixes

* **deps:** lock file maintenance ([a7de2ef](https://github.com/scratchfoundation/scratch-render/commit/a7de2eff460fc5c196177ebe9112b8627d689278))

## [2.0.15](https://github.com/scratchfoundation/scratch-render/compare/v2.0.14...v2.0.15) (2024-12-02)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.134 ([f3fa6fa](https://github.com/scratchfoundation/scratch-render/commit/f3fa6fa328081c02ad4198209d5c4a72140a65f9))

## [2.0.14](https://github.com/scratchfoundation/scratch-render/compare/v2.0.13...v2.0.14) (2024-12-02)


### Bug Fixes

* **deps:** lock file maintenance ([9f6d49a](https://github.com/scratchfoundation/scratch-render/commit/9f6d49a4f41d95a6e12079181191b68630f34a60))

## [2.0.13](https://github.com/scratchfoundation/scratch-render/compare/v2.0.12...v2.0.13) (2024-12-01)


### Bug Fixes

* **deps:** lock file maintenance ([fa95ebe](https://github.com/scratchfoundation/scratch-render/commit/fa95ebe336ef53e3722ce7cbe568654b74837456))

## [2.0.12](https://github.com/scratchfoundation/scratch-render/compare/v2.0.11...v2.0.12) (2024-12-01)


### Bug Fixes

* **deps:** lock file maintenance ([99d8520](https://github.com/scratchfoundation/scratch-render/commit/99d8520e24b62cdcc4bf338956a2713627e47dc7))

## [2.0.11](https://github.com/scratchfoundation/scratch-render/compare/v2.0.10...v2.0.11) (2024-11-30)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.133 ([9df6ef9](https://github.com/scratchfoundation/scratch-render/commit/9df6ef994d0991808ed3c5da091dca68c4e9de29))

## [2.0.10](https://github.com/scratchfoundation/scratch-render/compare/v2.0.9...v2.0.10) (2024-11-30)


### Bug Fixes

* **deps:** lock file maintenance ([8183fb6](https://github.com/scratchfoundation/scratch-render/commit/8183fb68d8505c975cbf388fab17bbee07ed2b15))

## [2.0.9](https://github.com/scratchfoundation/scratch-render/compare/v2.0.8...v2.0.9) (2024-11-30)


### Bug Fixes

* **deps:** lock file maintenance ([86df73c](https://github.com/scratchfoundation/scratch-render/commit/86df73c2ceab85aa125efc1a3e201b72d6daf7dd))

## [2.0.8](https://github.com/scratchfoundation/scratch-render/compare/v2.0.7...v2.0.8) (2024-11-29)


### Bug Fixes

* **deps:** lock file maintenance ([008035c](https://github.com/scratchfoundation/scratch-render/commit/008035c0637517b4a5d93cd457e9bf886419a8e7))

## [2.0.7](https://github.com/scratchfoundation/scratch-render/compare/v2.0.6...v2.0.7) (2024-11-29)


### Bug Fixes

* **deps:** lock file maintenance ([743cbaa](https://github.com/scratchfoundation/scratch-render/commit/743cbaa8739ebc7850e4ba835f5822c51f6a5043))

## [2.0.6](https://github.com/scratchfoundation/scratch-render/compare/v2.0.5...v2.0.6) (2024-11-28)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.132 ([696e7b8](https://github.com/scratchfoundation/scratch-render/commit/696e7b890be287a01306f8899ac16125cbcf224b))

## [2.0.5](https://github.com/scratchfoundation/scratch-render/compare/v2.0.4...v2.0.5) (2024-11-28)


### Bug Fixes

* **deps:** lock file maintenance ([32ad52c](https://github.com/scratchfoundation/scratch-render/commit/32ad52c544d4eaa0e044625db2b865a9e10f9535))

## [2.0.4](https://github.com/scratchfoundation/scratch-render/compare/v2.0.3...v2.0.4) (2024-11-28)


### Bug Fixes

* **deps:** lock file maintenance ([4d9b53e](https://github.com/scratchfoundation/scratch-render/commit/4d9b53e699e29689cd1407bfe50dda96b9acda04))

## [2.0.3](https://github.com/scratchfoundation/scratch-render/compare/v2.0.2...v2.0.3) (2024-11-27)


### Bug Fixes

* **deps:** lock file maintenance ([0b14a0a](https://github.com/scratchfoundation/scratch-render/commit/0b14a0a2f9d85248071e7132f32550b7e5aec401))

## [2.0.2](https://github.com/scratchfoundation/scratch-render/compare/v2.0.1...v2.0.2) (2024-11-27)


### Bug Fixes

* **deps:** lock file maintenance ([0f52119](https://github.com/scratchfoundation/scratch-render/commit/0f521197e01b0fbe318a983a546900c17cb527b4))

## [2.0.1](https://github.com/scratchfoundation/scratch-render/compare/v2.0.0...v2.0.1) (2024-11-26)


### Bug Fixes

* **deps:** lock file maintenance ([5ee3063](https://github.com/scratchfoundation/scratch-render/commit/5ee30631d98e77c42e2a99dbf82e61208e22e553))

# [2.0.0](https://github.com/scratchfoundation/scratch-render/compare/v1.2.126...v2.0.0) (2024-11-25)


* chore!: set license to AGPL-3.0-only ([27efa60](https://github.com/scratchfoundation/scratch-render/commit/27efa6029e8f60e10f4dc8b4f164861d95b6656a))


### BREAKING CHANGES

* This project is now licensed under the AGPL version 3.0

See https://www.scratchfoundation.org/open-source-license

## [1.2.126](https://github.com/scratchfoundation/scratch-render/compare/v1.2.125...v1.2.126) (2024-11-25)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.131 ([25befe3](https://github.com/scratchfoundation/scratch-render/commit/25befe300fc0fde7271e2bb8bac90c1351d67f50))

## [1.2.125](https://github.com/scratchfoundation/scratch-render/compare/v1.2.124...v1.2.125) (2024-11-25)


### Bug Fixes

* **deps:** lock file maintenance ([0201d97](https://github.com/scratchfoundation/scratch-render/commit/0201d975767c0b1d1b51394ad87f97ec11bb5102))

## [1.2.124](https://github.com/scratchfoundation/scratch-render/compare/v1.2.123...v1.2.124) (2024-11-25)


### Bug Fixes

* **deps:** lock file maintenance ([7256b9f](https://github.com/scratchfoundation/scratch-render/commit/7256b9f435f7d7f9ad39ec12252c3a7cfd8f5fc8))

## [1.2.123](https://github.com/scratchfoundation/scratch-render/compare/v1.2.122...v1.2.123) (2024-11-24)


### Bug Fixes

* **deps:** lock file maintenance ([e16cb18](https://github.com/scratchfoundation/scratch-render/commit/e16cb18378e30e1cdd5984b5e999e3b7b1de3d58))

## [1.2.122](https://github.com/scratchfoundation/scratch-render/compare/v1.2.121...v1.2.122) (2024-11-24)


### Bug Fixes

* **deps:** lock file maintenance ([da7bb1f](https://github.com/scratchfoundation/scratch-render/commit/da7bb1f0550b9f2bd186a0d0644face87328a2a1))

## [1.2.121](https://github.com/scratchfoundation/scratch-render/compare/v1.2.120...v1.2.121) (2024-11-23)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.130 ([c11a387](https://github.com/scratchfoundation/scratch-render/commit/c11a387acd584d5c4cd7592037d230e5d6bad9ef))

## [1.2.120](https://github.com/scratchfoundation/scratch-render/compare/v1.2.119...v1.2.120) (2024-11-23)


### Bug Fixes

* **deps:** lock file maintenance ([621f734](https://github.com/scratchfoundation/scratch-render/commit/621f73460cb5f47cca23e48132f35c43726a3acd))

## [1.2.119](https://github.com/scratchfoundation/scratch-render/compare/v1.2.118...v1.2.119) (2024-11-23)


### Bug Fixes

* **deps:** lock file maintenance ([77ca87c](https://github.com/scratchfoundation/scratch-render/commit/77ca87c72e8f8941fbdfd6b097a5cc789e6ec478))

## [1.2.118](https://github.com/scratchfoundation/scratch-render/compare/v1.2.117...v1.2.118) (2024-11-22)


### Bug Fixes

* **deps:** lock file maintenance ([0aaa42e](https://github.com/scratchfoundation/scratch-render/commit/0aaa42e82aa695ecbe539626dcf318ecfb53beeb))

## [1.2.117](https://github.com/scratchfoundation/scratch-render/compare/v1.2.116...v1.2.117) (2024-11-22)


### Bug Fixes

* **deps:** lock file maintenance ([e1cb532](https://github.com/scratchfoundation/scratch-render/commit/e1cb5325b33eef4f0de929f0f276c30c6ce3e07a))

## [1.2.116](https://github.com/scratchfoundation/scratch-render/compare/v1.2.115...v1.2.116) (2024-11-21)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.129 ([4323785](https://github.com/scratchfoundation/scratch-render/commit/4323785b895c189bfda56e22b6d1be35bdaba2e6))

## [1.2.115](https://github.com/scratchfoundation/scratch-render/compare/v1.2.114...v1.2.115) (2024-11-21)


### Bug Fixes

* **deps:** lock file maintenance ([175d2be](https://github.com/scratchfoundation/scratch-render/commit/175d2be17be20b5f9287b468560381856bb48586))

## [1.2.114](https://github.com/scratchfoundation/scratch-render/compare/v1.2.113...v1.2.114) (2024-11-21)


### Bug Fixes

* **deps:** lock file maintenance ([8be9c45](https://github.com/scratchfoundation/scratch-render/commit/8be9c451dafc36d00adab85369ae54cf9d51e9b5))

## [1.2.113](https://github.com/scratchfoundation/scratch-render/compare/v1.2.112...v1.2.113) (2024-11-20)


### Bug Fixes

* **deps:** lock file maintenance ([a33953c](https://github.com/scratchfoundation/scratch-render/commit/a33953c26d12a478d32895cf8238f28ec0c8ae3a))

## [1.2.112](https://github.com/scratchfoundation/scratch-render/compare/v1.2.111...v1.2.112) (2024-11-20)


### Bug Fixes

* **deps:** lock file maintenance ([466ca0c](https://github.com/scratchfoundation/scratch-render/commit/466ca0cde6bcaaac3e999f80d3184f0f55489dd4))

## [1.2.111](https://github.com/scratchfoundation/scratch-render/compare/v1.2.110...v1.2.111) (2024-11-19)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.128 ([135785a](https://github.com/scratchfoundation/scratch-render/commit/135785a7ba61668b9779048871f1b42da1656e83))

## [1.2.110](https://github.com/scratchfoundation/scratch-render/compare/v1.2.109...v1.2.110) (2024-11-19)


### Bug Fixes

* **deps:** lock file maintenance ([f9db43f](https://github.com/scratchfoundation/scratch-render/commit/f9db43f02abbbc9fa61b7e740ff11638140f4666))

## [1.2.109](https://github.com/scratchfoundation/scratch-render/compare/v1.2.108...v1.2.109) (2024-11-18)


### Bug Fixes

* **deps:** lock file maintenance ([2e2f353](https://github.com/scratchfoundation/scratch-render/commit/2e2f35332c80fc2a0ed1183e30c0e95327ca6875))

## [1.2.108](https://github.com/scratchfoundation/scratch-render/compare/v1.2.107...v1.2.108) (2024-11-18)


### Bug Fixes

* **deps:** lock file maintenance ([2d5f93f](https://github.com/scratchfoundation/scratch-render/commit/2d5f93fb5f5cfcbaa186733fb8c14248df67995f))

## [1.2.107](https://github.com/scratchfoundation/scratch-render/compare/v1.2.106...v1.2.107) (2024-11-17)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.127 ([d4a3954](https://github.com/scratchfoundation/scratch-render/commit/d4a395498a967f65441f67068f49b46e99869638))

## [1.2.106](https://github.com/scratchfoundation/scratch-render/compare/v1.2.105...v1.2.106) (2024-11-17)


### Bug Fixes

* **deps:** lock file maintenance ([ec29e20](https://github.com/scratchfoundation/scratch-render/commit/ec29e20e4130cf5c5aba80795a2c7fb0979c6d0e))

## [1.2.105](https://github.com/scratchfoundation/scratch-render/compare/v1.2.104...v1.2.105) (2024-11-17)


### Bug Fixes

* **deps:** lock file maintenance ([09bf11c](https://github.com/scratchfoundation/scratch-render/commit/09bf11c4f1692c24190e88fe403a0042f4b902a8))

## [1.2.104](https://github.com/scratchfoundation/scratch-render/compare/v1.2.103...v1.2.104) (2024-11-16)


### Bug Fixes

* **deps:** lock file maintenance ([1b5ab8d](https://github.com/scratchfoundation/scratch-render/commit/1b5ab8d421cac5400e08e8717bbb84ba91adc2b1))

## [1.2.103](https://github.com/scratchfoundation/scratch-render/compare/v1.2.102...v1.2.103) (2024-11-16)


### Bug Fixes

* **deps:** lock file maintenance ([0fc1020](https://github.com/scratchfoundation/scratch-render/commit/0fc10209c6454e7bc4a8608f27ca47e67849f517))

## [1.2.102](https://github.com/scratchfoundation/scratch-render/compare/v1.2.101...v1.2.102) (2024-11-15)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.126 ([1513098](https://github.com/scratchfoundation/scratch-render/commit/1513098bb9d1b2130c40a3340061d03d224624d6))

## [1.2.101](https://github.com/scratchfoundation/scratch-render/compare/v1.2.100...v1.2.101) (2024-11-15)


### Bug Fixes

* **deps:** lock file maintenance ([8d9cf21](https://github.com/scratchfoundation/scratch-render/commit/8d9cf2107df840e007aaa233f72fd0ad6ea1fc43))

## [1.2.100](https://github.com/scratchfoundation/scratch-render/compare/v1.2.99...v1.2.100) (2024-11-15)


### Bug Fixes

* **deps:** lock file maintenance ([378db7c](https://github.com/scratchfoundation/scratch-render/commit/378db7c9a4fedd1d6c4db19262ebcc09372cb863))

## [1.2.99](https://github.com/scratchfoundation/scratch-render/compare/v1.2.98...v1.2.99) (2024-11-14)


### Bug Fixes

* **deps:** lock file maintenance ([c2a4c2d](https://github.com/scratchfoundation/scratch-render/commit/c2a4c2dc19e7814fc5f04169de53c32c9176bfc4))

## [1.2.98](https://github.com/scratchfoundation/scratch-render/compare/v1.2.97...v1.2.98) (2024-11-14)


### Bug Fixes

* **deps:** lock file maintenance ([385bbfa](https://github.com/scratchfoundation/scratch-render/commit/385bbfaed396c82ce625b9af49a194fe047f908e))

## [1.2.97](https://github.com/scratchfoundation/scratch-render/compare/v1.2.96...v1.2.97) (2024-11-13)


### Bug Fixes

* **deps:** lock file maintenance ([265c557](https://github.com/scratchfoundation/scratch-render/commit/265c5573f890a70ec2048d25348ddf39b62c9f7f))

## [1.2.96](https://github.com/scratchfoundation/scratch-render/compare/v1.2.95...v1.2.96) (2024-11-13)


### Bug Fixes

* **deps:** lock file maintenance ([31b8e02](https://github.com/scratchfoundation/scratch-render/commit/31b8e02dc7f8ef26ff6c171bb429523ed66cd631))

## [1.2.95](https://github.com/scratchfoundation/scratch-render/compare/v1.2.94...v1.2.95) (2024-11-12)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.125 ([7fe8b19](https://github.com/scratchfoundation/scratch-render/commit/7fe8b19ab669ada3e2c526f7cc0e75b008633ab7))

## [1.2.94](https://github.com/scratchfoundation/scratch-render/compare/v1.2.93...v1.2.94) (2024-11-12)


### Bug Fixes

* **deps:** lock file maintenance ([0714ebd](https://github.com/scratchfoundation/scratch-render/commit/0714ebd986b5b67592eff8104296845987f16888))

## [1.2.93](https://github.com/scratchfoundation/scratch-render/compare/v1.2.92...v1.2.93) (2024-11-12)


### Bug Fixes

* **deps:** lock file maintenance ([ed3efc2](https://github.com/scratchfoundation/scratch-render/commit/ed3efc291f20c0b6ee4be5910689921da0eb363c))

## [1.2.92](https://github.com/scratchfoundation/scratch-render/compare/v1.2.91...v1.2.92) (2024-11-11)


### Bug Fixes

* **deps:** lock file maintenance ([8d18377](https://github.com/scratchfoundation/scratch-render/commit/8d18377647ab510f77f865108f107185aae91ca1))
* **deps:** update dependency scratch-render-fonts to v1.0.124 ([01235ae](https://github.com/scratchfoundation/scratch-render/commit/01235aed356531e085f7dd61fd146a6b8a5db19d))

## [1.2.91](https://github.com/scratchfoundation/scratch-render/compare/v1.2.90...v1.2.91) (2024-11-11)


### Bug Fixes

* **deps:** lock file maintenance ([a35df9c](https://github.com/scratchfoundation/scratch-render/commit/a35df9cbaf3bca3d6a2e0a648bb27e9ee1b2312d))

## [1.2.90](https://github.com/scratchfoundation/scratch-render/compare/v1.2.89...v1.2.90) (2024-11-10)


### Bug Fixes

* **deps:** lock file maintenance ([744581c](https://github.com/scratchfoundation/scratch-render/commit/744581c3d0cf99990b99b4a86c2f2764c4943775))

## [1.2.89](https://github.com/scratchfoundation/scratch-render/compare/v1.2.88...v1.2.89) (2024-11-10)


### Bug Fixes

* **deps:** lock file maintenance ([372f121](https://github.com/scratchfoundation/scratch-render/commit/372f121f0b5f76e44eef4ed0694e88024a5730cc))

## [1.2.88](https://github.com/scratchfoundation/scratch-render/compare/v1.2.87...v1.2.88) (2024-11-09)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.123 ([6638b1f](https://github.com/scratchfoundation/scratch-render/commit/6638b1fe7c6baaa78bc01f42cb531acc5cb0d07b))

## [1.2.87](https://github.com/scratchfoundation/scratch-render/compare/v1.2.86...v1.2.87) (2024-11-09)


### Bug Fixes

* **deps:** lock file maintenance ([9101578](https://github.com/scratchfoundation/scratch-render/commit/91015782842bd20767a34a06fbaac8f6c75263a0))

## [1.2.86](https://github.com/scratchfoundation/scratch-render/compare/v1.2.85...v1.2.86) (2024-11-09)


### Bug Fixes

* **deps:** lock file maintenance ([7c0b2a9](https://github.com/scratchfoundation/scratch-render/commit/7c0b2a9a439a214344d569ce29c47ac0a1b1efef))

## [1.2.85](https://github.com/scratchfoundation/scratch-render/compare/v1.2.84...v1.2.85) (2024-11-08)


### Bug Fixes

* **deps:** lock file maintenance ([7e02402](https://github.com/scratchfoundation/scratch-render/commit/7e024027c344e8b4ac06f86bada29884310e751f))

## [1.2.84](https://github.com/scratchfoundation/scratch-render/compare/v1.2.83...v1.2.84) (2024-11-08)


### Bug Fixes

* **deps:** lock file maintenance ([5b0c437](https://github.com/scratchfoundation/scratch-render/commit/5b0c43735bd20f85b9406a1fb3ab88d4f3c8ebf8))

## [1.2.83](https://github.com/scratchfoundation/scratch-render/compare/v1.2.82...v1.2.83) (2024-11-07)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.122 ([fd86c23](https://github.com/scratchfoundation/scratch-render/commit/fd86c237d2e577b0e86f8d01b42e7e0c2c2da651))

## [1.2.82](https://github.com/scratchfoundation/scratch-render/compare/v1.2.81...v1.2.82) (2024-11-07)


### Bug Fixes

* **deps:** lock file maintenance ([c587bac](https://github.com/scratchfoundation/scratch-render/commit/c587bac072441fa1ecad63c6e70dec785d4341df))

## [1.2.81](https://github.com/scratchfoundation/scratch-render/compare/v1.2.80...v1.2.81) (2024-11-06)


### Bug Fixes

* **deps:** lock file maintenance ([8ed7cb8](https://github.com/scratchfoundation/scratch-render/commit/8ed7cb847358907171eb42426efbc943438d0ba3))

## [1.2.80](https://github.com/scratchfoundation/scratch-render/compare/v1.2.79...v1.2.80) (2024-11-05)


### Bug Fixes

* **deps:** lock file maintenance ([134af45](https://github.com/scratchfoundation/scratch-render/commit/134af4594b73d3e4d6ff07607198467b68811d08))
* **deps:** update dependency scratch-render-fonts to v1.0.121 ([575e920](https://github.com/scratchfoundation/scratch-render/commit/575e92026f77aec95ea47597b876cac8f12e4b2b))

## [1.2.79](https://github.com/scratchfoundation/scratch-render/compare/v1.2.78...v1.2.79) (2024-11-05)


### Bug Fixes

* **deps:** lock file maintenance ([c08c309](https://github.com/scratchfoundation/scratch-render/commit/c08c309f95d0f99917a16165b206e52fb057d750))

## [1.2.78](https://github.com/scratchfoundation/scratch-render/compare/v1.2.77...v1.2.78) (2024-11-04)


### Bug Fixes

* **deps:** lock file maintenance ([79af5ac](https://github.com/scratchfoundation/scratch-render/commit/79af5aca6fbe7733912259986b197abde971922a))

## [1.2.77](https://github.com/scratchfoundation/scratch-render/compare/v1.2.76...v1.2.77) (2024-11-04)


### Bug Fixes

* **deps:** lock file maintenance ([e35435e](https://github.com/scratchfoundation/scratch-render/commit/e35435e6407778dcd4b2e3d1f7ac12a83ccb7512))

## [1.2.76](https://github.com/scratchfoundation/scratch-render/compare/v1.2.75...v1.2.76) (2024-11-03)


### Bug Fixes

* **deps:** lock file maintenance ([ade4a59](https://github.com/scratchfoundation/scratch-render/commit/ade4a5921f776b69c6353bdcd9722a0f3673d1da))
* **deps:** update dependency scratch-render-fonts to v1.0.120 ([68282be](https://github.com/scratchfoundation/scratch-render/commit/68282bed18d525defed2497248a9c6cb51ed3c52))

## [1.2.75](https://github.com/scratchfoundation/scratch-render/compare/v1.2.74...v1.2.75) (2024-11-03)


### Bug Fixes

* **deps:** lock file maintenance ([bcb5098](https://github.com/scratchfoundation/scratch-render/commit/bcb50981cbc6cc0fe4c61c8f4bc735bda6885f49))

## [1.2.74](https://github.com/scratchfoundation/scratch-render/compare/v1.2.73...v1.2.74) (2024-11-02)


### Bug Fixes

* **deps:** lock file maintenance ([0fc2b65](https://github.com/scratchfoundation/scratch-render/commit/0fc2b65db33ceb3ff2731d8d3cdba219d7405251))

## [1.2.73](https://github.com/scratchfoundation/scratch-render/compare/v1.2.72...v1.2.73) (2024-11-02)


### Bug Fixes

* **deps:** lock file maintenance ([6e8efe4](https://github.com/scratchfoundation/scratch-render/commit/6e8efe4e9ce507da708383e30c8fd0ad24b0db20))

## [1.2.72](https://github.com/scratchfoundation/scratch-render/compare/v1.2.71...v1.2.72) (2024-11-01)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.119 ([3764d32](https://github.com/scratchfoundation/scratch-render/commit/3764d326a7113a835bd506451478b6a43d3b7ffd))

## [1.2.71](https://github.com/scratchfoundation/scratch-render/compare/v1.2.70...v1.2.71) (2024-11-01)


### Bug Fixes

* **deps:** lock file maintenance ([1bfec4a](https://github.com/scratchfoundation/scratch-render/commit/1bfec4ac2af779ce0f150fe64ac6aa811214a29d))

## [1.2.70](https://github.com/scratchfoundation/scratch-render/compare/v1.2.69...v1.2.70) (2024-10-31)


### Bug Fixes

* **deps:** lock file maintenance ([3c2aeb1](https://github.com/scratchfoundation/scratch-render/commit/3c2aeb1b9ed276259a089e91053aff14ada36c03))

## [1.2.69](https://github.com/scratchfoundation/scratch-render/compare/v1.2.68...v1.2.69) (2024-10-31)


### Bug Fixes

* **deps:** lock file maintenance ([79e8b53](https://github.com/scratchfoundation/scratch-render/commit/79e8b53930981a150bb668e9f5cb5aacf014a1fa))

## [1.2.68](https://github.com/scratchfoundation/scratch-render/compare/v1.2.67...v1.2.68) (2024-10-30)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.118 ([d637b2d](https://github.com/scratchfoundation/scratch-render/commit/d637b2d574bfbc52ac778c3c2953de85bb9645a2))

## [1.2.67](https://github.com/scratchfoundation/scratch-render/compare/v1.2.66...v1.2.67) (2024-10-30)


### Bug Fixes

* **deps:** lock file maintenance ([325f58c](https://github.com/scratchfoundation/scratch-render/commit/325f58ce6a26110112039f1038617c2e5c98ab8a))

## [1.2.66](https://github.com/scratchfoundation/scratch-render/compare/v1.2.65...v1.2.66) (2024-10-29)


### Bug Fixes

* **deps:** lock file maintenance ([7654f60](https://github.com/scratchfoundation/scratch-render/commit/7654f606d30450dbc741c256210af6fc3f9a654f))

## [1.2.65](https://github.com/scratchfoundation/scratch-render/compare/v1.2.64...v1.2.65) (2024-10-28)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.117 ([8838739](https://github.com/scratchfoundation/scratch-render/commit/8838739728fba471c4de9b3359081425d58faac9))

## [1.2.64](https://github.com/scratchfoundation/scratch-render/compare/v1.2.63...v1.2.64) (2024-10-28)


### Bug Fixes

* **deps:** lock file maintenance ([b0d8131](https://github.com/scratchfoundation/scratch-render/commit/b0d81316d3a33ac8e4908a3bde507915e5f18266))

## [1.2.63](https://github.com/scratchfoundation/scratch-render/compare/v1.2.62...v1.2.63) (2024-10-27)


### Bug Fixes

* **deps:** lock file maintenance ([5bcdba9](https://github.com/scratchfoundation/scratch-render/commit/5bcdba993e3be42f30d7b07d13de6b6990d63417))

## [1.2.62](https://github.com/scratchfoundation/scratch-render/compare/v1.2.61...v1.2.62) (2024-10-26)


### Bug Fixes

* **deps:** lock file maintenance ([778b9c8](https://github.com/scratchfoundation/scratch-render/commit/778b9c8a4c80fe2c7b5a795ca3cbe01b3bdd188a))

## [1.2.61](https://github.com/scratchfoundation/scratch-render/compare/v1.2.60...v1.2.61) (2024-10-25)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.116 ([8a52b33](https://github.com/scratchfoundation/scratch-render/commit/8a52b3320ba0f13b2fafe9c7a9065d135b59c9bb))

## [1.2.60](https://github.com/scratchfoundation/scratch-render/compare/v1.2.59...v1.2.60) (2024-10-25)


### Bug Fixes

* **deps:** lock file maintenance ([056eaf6](https://github.com/scratchfoundation/scratch-render/commit/056eaf6ba8bb1e78178b68e3a01d69d958741b65))

## [1.2.59](https://github.com/scratchfoundation/scratch-render/compare/v1.2.58...v1.2.59) (2024-10-24)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.115 ([359f821](https://github.com/scratchfoundation/scratch-render/commit/359f82171b8e636495dac3f6230eebb42285c144))

## [1.2.58](https://github.com/scratchfoundation/scratch-render/compare/v1.2.57...v1.2.58) (2024-10-24)


### Bug Fixes

* **deps:** lock file maintenance ([611a653](https://github.com/scratchfoundation/scratch-render/commit/611a65303298b33fc2e1eb976e4b17c336044e96))

## [1.2.57](https://github.com/scratchfoundation/scratch-render/compare/v1.2.56...v1.2.57) (2024-10-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.284 ([0d640ed](https://github.com/scratchfoundation/scratch-render/commit/0d640ed95dcfee5d12c2813ae5a90cec970a5e6d))

## [1.2.56](https://github.com/scratchfoundation/scratch-render/compare/v1.2.55...v1.2.56) (2024-10-23)


### Bug Fixes

* **deps:** lock file maintenance ([59d4c26](https://github.com/scratchfoundation/scratch-render/commit/59d4c2670e5a7359eac410ee3236c44dd75353a8))

## [1.2.55](https://github.com/scratchfoundation/scratch-render/compare/v1.2.54...v1.2.55) (2024-10-22)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.283 ([0441c92](https://github.com/scratchfoundation/scratch-render/commit/0441c927215861dea3e8e94a67aa2fb4ad808a68))

## [1.2.54](https://github.com/scratchfoundation/scratch-render/compare/v1.2.53...v1.2.54) (2024-10-22)


### Bug Fixes

* **deps:** lock file maintenance ([c19a839](https://github.com/scratchfoundation/scratch-render/commit/c19a839810f7e0569b2b4278669e98f716df9c5a))

## [1.2.53](https://github.com/scratchfoundation/scratch-render/compare/v1.2.52...v1.2.53) (2024-10-21)


### Bug Fixes

* **deps:** lock file maintenance ([3e1f9ed](https://github.com/scratchfoundation/scratch-render/commit/3e1f9ed92c62c53917c7bfa0a760cf2e68e46d4a))
* **deps:** update dependency scratch-render-fonts to v1.0.114 ([6d7e4ad](https://github.com/scratchfoundation/scratch-render/commit/6d7e4ada328a113ac28eab4660370f9a11bfa5ee))

## [1.2.52](https://github.com/scratchfoundation/scratch-render/compare/v1.2.51...v1.2.52) (2024-10-21)


### Bug Fixes

* **deps:** lock file maintenance ([d38e3b9](https://github.com/scratchfoundation/scratch-render/commit/d38e3b92319dba69cc80beda32e041521141640d))

## [1.2.51](https://github.com/scratchfoundation/scratch-render/compare/v1.2.50...v1.2.51) (2024-10-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.282 ([c3b85ae](https://github.com/scratchfoundation/scratch-render/commit/c3b85aef796223417246f7719c3200ffb86697cc))

## [1.2.50](https://github.com/scratchfoundation/scratch-render/compare/v1.2.49...v1.2.50) (2024-10-20)


### Bug Fixes

* **deps:** lock file maintenance ([fad598f](https://github.com/scratchfoundation/scratch-render/commit/fad598f058aa69b0ca051d3430eb32f81d65a5a9))

## [1.2.49](https://github.com/scratchfoundation/scratch-render/compare/v1.2.48...v1.2.49) (2024-10-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.281 ([80e4367](https://github.com/scratchfoundation/scratch-render/commit/80e4367fac14679049adbb6e4c732ce53442a7bb))

## [1.2.48](https://github.com/scratchfoundation/scratch-render/compare/v1.2.47...v1.2.48) (2024-10-19)


### Bug Fixes

* **deps:** lock file maintenance ([54af812](https://github.com/scratchfoundation/scratch-render/commit/54af812a24a257def8a5d179b8deb19954dfb5ed))

## [1.2.47](https://github.com/scratchfoundation/scratch-render/compare/v1.2.46...v1.2.47) (2024-10-19)


### Bug Fixes

* **deps:** lock file maintenance ([0d1388b](https://github.com/scratchfoundation/scratch-render/commit/0d1388b2401fd5ac49c7b18c02fe13a928b5655b))
* **deps:** lock file maintenance ([61037e5](https://github.com/scratchfoundation/scratch-render/commit/61037e5234c158ad9648d2a76380b850b07430bd))
* **deps:** lock file maintenance ([37b1db3](https://github.com/scratchfoundation/scratch-render/commit/37b1db364fc4d3a95ddb0f847bc3cf16bc869a67))
* **deps:** lock file maintenance ([c456aa0](https://github.com/scratchfoundation/scratch-render/commit/c456aa05206c59993fd8227966ece62667da254e))
* **deps:** lock file maintenance ([2dc3557](https://github.com/scratchfoundation/scratch-render/commit/2dc3557123c5d4e86f01cbaf0a647cfd7556bd66))
* **deps:** update dependency scratch-storage to v2.3.280 ([0bde4be](https://github.com/scratchfoundation/scratch-render/commit/0bde4be6abd5a58abe74a5e3d092e4598151059e))

## [1.2.46](https://github.com/scratchfoundation/scratch-render/compare/v1.2.45...v1.2.46) (2024-10-15)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.113 ([90f835c](https://github.com/scratchfoundation/scratch-render/commit/90f835c0b7709dd66f1bd669a8a289657db3239d))

## [1.2.45](https://github.com/scratchfoundation/scratch-render/compare/v1.2.44...v1.2.45) (2024-10-15)


### Bug Fixes

* **deps:** lock file maintenance ([5f10d56](https://github.com/scratchfoundation/scratch-render/commit/5f10d5680310b0776e96508c5b9ca60781230537))

## [1.2.44](https://github.com/scratchfoundation/scratch-render/compare/v1.2.43...v1.2.44) (2024-10-14)


### Bug Fixes

* **deps:** lock file maintenance ([b01ae0f](https://github.com/scratchfoundation/scratch-render/commit/b01ae0f6e1204f8ad6c9cbe2d72cb448290827bb))

## [1.2.43](https://github.com/scratchfoundation/scratch-render/compare/v1.2.42...v1.2.43) (2024-10-13)


### Bug Fixes

* **deps:** lock file maintenance ([ec0adf9](https://github.com/scratchfoundation/scratch-render/commit/ec0adf9bc507c1cafe1b6dbaa49f1af9fa58c2e2))

## [1.2.42](https://github.com/scratchfoundation/scratch-render/compare/v1.2.41...v1.2.42) (2024-10-12)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.112 ([e9e0cde](https://github.com/scratchfoundation/scratch-render/commit/e9e0cdea1afabc6125a00a08bdd646072d4ee076))

## [1.2.41](https://github.com/scratchfoundation/scratch-render/compare/v1.2.40...v1.2.41) (2024-10-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.278 ([cef9c57](https://github.com/scratchfoundation/scratch-render/commit/cef9c57257266a8e362a76b89a3df42f02376a54))

## [1.2.40](https://github.com/scratchfoundation/scratch-render/compare/v1.2.39...v1.2.40) (2024-10-12)


### Bug Fixes

* **deps:** lock file maintenance ([8514a31](https://github.com/scratchfoundation/scratch-render/commit/8514a317564aafda4a69d1ac066c848e5d495fb9))

## [1.2.39](https://github.com/scratchfoundation/scratch-render/compare/v1.2.38...v1.2.39) (2024-10-12)


### Bug Fixes

* **deps:** lock file maintenance ([90b7502](https://github.com/scratchfoundation/scratch-render/commit/90b7502c2f3291078b08fca76e1f1ddeeb8923a2))

## [1.2.38](https://github.com/scratchfoundation/scratch-render/compare/v1.2.37...v1.2.38) (2024-10-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.277 ([241b7c2](https://github.com/scratchfoundation/scratch-render/commit/241b7c23a6f6f1ce24f8ce145a6c578e9bf954a4))

## [1.2.37](https://github.com/scratchfoundation/scratch-render/compare/v1.2.36...v1.2.37) (2024-10-11)


### Bug Fixes

* **deps:** lock file maintenance ([3e4cc39](https://github.com/scratchfoundation/scratch-render/commit/3e4cc395d81e84a14bbed06b67fb91e793514647))

## [1.2.36](https://github.com/scratchfoundation/scratch-render/compare/v1.2.35...v1.2.36) (2024-10-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.276 ([92693b2](https://github.com/scratchfoundation/scratch-render/commit/92693b2c7820a2f99a5231011c147a98beddb234))

## [1.2.35](https://github.com/scratchfoundation/scratch-render/compare/v1.2.34...v1.2.35) (2024-10-10)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.111 ([acb805e](https://github.com/scratchfoundation/scratch-render/commit/acb805ee7dfa08f77ef2b58891d9dcb3173b40bf))

## [1.2.34](https://github.com/scratchfoundation/scratch-render/compare/v1.2.33...v1.2.34) (2024-10-10)


### Bug Fixes

* **deps:** lock file maintenance ([0800b25](https://github.com/scratchfoundation/scratch-render/commit/0800b25e48681503635340e79214acf197d6fa75))

## [1.2.33](https://github.com/scratchfoundation/scratch-render/compare/v1.2.32...v1.2.33) (2024-10-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.275 ([602c82b](https://github.com/scratchfoundation/scratch-render/commit/602c82bfb3ec3b106ce65764c1101df3e5d789b5))

## [1.2.32](https://github.com/scratchfoundation/scratch-render/compare/v1.2.31...v1.2.32) (2024-10-09)


### Bug Fixes

* **deps:** lock file maintenance ([4431607](https://github.com/scratchfoundation/scratch-render/commit/44316076b01ae718889b28cb9d6fc4d96bb036ac))

## [1.2.31](https://github.com/scratchfoundation/scratch-render/compare/v1.2.30...v1.2.31) (2024-10-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.274 ([421bcf3](https://github.com/scratchfoundation/scratch-render/commit/421bcf30064fcf28eb05b86d109aad699815872d))

## [1.2.30](https://github.com/scratchfoundation/scratch-render/compare/v1.2.29...v1.2.30) (2024-10-08)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.110 ([ecedd14](https://github.com/scratchfoundation/scratch-render/commit/ecedd14592e2af9ad2786dc01f40e61c05b4fdaa))

## [1.2.29](https://github.com/scratchfoundation/scratch-render/compare/v1.2.28...v1.2.29) (2024-10-08)


### Bug Fixes

* **deps:** lock file maintenance ([f8448d0](https://github.com/scratchfoundation/scratch-render/commit/f8448d0f07b29b335e97db42c9596da2a68a4c2f))

## [1.2.28](https://github.com/scratchfoundation/scratch-render/compare/v1.2.27...v1.2.28) (2024-10-07)


### Bug Fixes

* **deps:** lock file maintenance ([b7cf541](https://github.com/scratchfoundation/scratch-render/commit/b7cf541b0310b46fd47e5273c21f895cc2de75e0))

## [1.2.27](https://github.com/scratchfoundation/scratch-render/compare/v1.2.26...v1.2.27) (2024-10-07)


### Bug Fixes

* **deps:** lock file maintenance ([4bea090](https://github.com/scratchfoundation/scratch-render/commit/4bea090d3c4252c40802f2fd1433bce8cbb2ed2b))

## [1.2.26](https://github.com/scratchfoundation/scratch-render/compare/v1.2.25...v1.2.26) (2024-10-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.273 ([e7949ed](https://github.com/scratchfoundation/scratch-render/commit/e7949ed961482c4c2fd1d9083fb88837251ea953))

## [1.2.25](https://github.com/scratchfoundation/scratch-render/compare/v1.2.24...v1.2.25) (2024-10-06)


### Bug Fixes

* **deps:** lock file maintenance ([9b23b88](https://github.com/scratchfoundation/scratch-render/commit/9b23b8838a690e9277569b70c3a5c6a93c1dfb7d))

## [1.2.24](https://github.com/scratchfoundation/scratch-render/compare/v1.2.23...v1.2.24) (2024-10-05)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.109 ([a563bd4](https://github.com/scratchfoundation/scratch-render/commit/a563bd43ca0ac6093d55c8fb533d36cecbf3aa6a))

## [1.2.23](https://github.com/scratchfoundation/scratch-render/compare/v1.2.22...v1.2.23) (2024-10-05)


### Bug Fixes

* **deps:** lock file maintenance ([827be76](https://github.com/scratchfoundation/scratch-render/commit/827be762bff1c898e0af7947e9c08de1579c6829))

## [1.2.22](https://github.com/scratchfoundation/scratch-render/compare/v1.2.21...v1.2.22) (2024-10-04)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.271 ([fc922fe](https://github.com/scratchfoundation/scratch-render/commit/fc922fe41249bc7aa24ce02d61d3053f157bc4c5))

## [1.2.21](https://github.com/scratchfoundation/scratch-render/compare/v1.2.20...v1.2.21) (2024-10-04)


### Bug Fixes

* **deps:** lock file maintenance ([1ac5212](https://github.com/scratchfoundation/scratch-render/commit/1ac521278362973c2f1275d09449a00aebb0276b))

## [1.2.20](https://github.com/scratchfoundation/scratch-render/compare/v1.2.19...v1.2.20) (2024-10-04)


### Bug Fixes

* **deps:** lock file maintenance ([ba1f8da](https://github.com/scratchfoundation/scratch-render/commit/ba1f8da8578aa34da1c253bfaa16a0ebb8eb99c0))

## [1.2.19](https://github.com/scratchfoundation/scratch-render/compare/v1.2.18...v1.2.19) (2024-10-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.269 ([df2c002](https://github.com/scratchfoundation/scratch-render/commit/df2c00275a3455945e7b1cd0850dd661611c9bdd))

## [1.2.18](https://github.com/scratchfoundation/scratch-render/compare/v1.2.17...v1.2.18) (2024-10-03)


### Bug Fixes

* **deps:** lock file maintenance ([40e5fb7](https://github.com/scratchfoundation/scratch-render/commit/40e5fb70760f4e1989c08f06e440bb4e9c36a1e6))
* **deps:** update dependency scratch-render-fonts to v1.0.108 ([4f5ef70](https://github.com/scratchfoundation/scratch-render/commit/4f5ef70bcc907a5aa44c79533b31b6b147f10c9a))

## [1.2.17](https://github.com/scratchfoundation/scratch-render/compare/v1.2.16...v1.2.17) (2024-10-03)


### Bug Fixes

* **deps:** lock file maintenance ([04f1ea3](https://github.com/scratchfoundation/scratch-render/commit/04f1ea3757625826f30a690b81684357e18aef62))

## [1.2.16](https://github.com/scratchfoundation/scratch-render/compare/v1.2.15...v1.2.16) (2024-10-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.268 ([0da4723](https://github.com/scratchfoundation/scratch-render/commit/0da4723b870689838e733afa47ac4bf4ef8be955))

## [1.2.15](https://github.com/scratchfoundation/scratch-render/compare/v1.2.14...v1.2.15) (2024-10-02)


### Bug Fixes

* **deps:** lock file maintenance ([f99d826](https://github.com/scratchfoundation/scratch-render/commit/f99d826667c8b197a2978e14ea756c4df2c84c90))

## [1.2.14](https://github.com/scratchfoundation/scratch-render/compare/v1.2.13...v1.2.14) (2024-10-01)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.107 ([0598e3f](https://github.com/scratchfoundation/scratch-render/commit/0598e3f929e0c7eb8504ff6f667c35ffe0214c81))

## [1.2.13](https://github.com/scratchfoundation/scratch-render/compare/v1.2.12...v1.2.13) (2024-10-01)


### Bug Fixes

* **deps:** lock file maintenance ([5c29510](https://github.com/scratchfoundation/scratch-render/commit/5c29510f3835888ab9d2c33eb6a1eccef7a9860a))
* **deps:** update dependency scratch-storage to v2.3.266 ([a89f619](https://github.com/scratchfoundation/scratch-render/commit/a89f619af8aa0319b44d16cdc9b26d886a523f9d))

## [1.2.12](https://github.com/scratchfoundation/scratch-render/compare/v1.2.11...v1.2.12) (2024-10-01)


### Bug Fixes

* **deps:** lock file maintenance ([c95e6b2](https://github.com/scratchfoundation/scratch-render/commit/c95e6b21d8b388ba7cc3e53a78f9daeff10b229c))

## [1.2.11](https://github.com/scratchfoundation/scratch-render/compare/v1.2.10...v1.2.11) (2024-09-30)


### Bug Fixes

* **deps:** lock file maintenance ([06680b4](https://github.com/scratchfoundation/scratch-render/commit/06680b4c19e16ca032850467e794d697af68b496))

## [1.2.10](https://github.com/scratchfoundation/scratch-render/compare/v1.2.9...v1.2.10) (2024-09-29)


### Bug Fixes

* **deps:** lock file maintenance ([2f1c937](https://github.com/scratchfoundation/scratch-render/commit/2f1c937000398fcdcaeda7bd8e52c28d2c61c0e0))
* **deps:** update dependency scratch-render-fonts to v1.0.106 ([c537150](https://github.com/scratchfoundation/scratch-render/commit/c537150048e54315442f6199083413069a9c080d))

## [1.2.9](https://github.com/scratchfoundation/scratch-render/compare/v1.2.8...v1.2.9) (2024-09-29)


### Bug Fixes

* **deps:** lock file maintenance ([5a73a62](https://github.com/scratchfoundation/scratch-render/commit/5a73a62af02fa22e5851b55b40bb48be6de6534d))

## [1.2.8](https://github.com/scratchfoundation/scratch-render/compare/v1.2.7...v1.2.8) (2024-09-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.265 ([794fe98](https://github.com/scratchfoundation/scratch-render/commit/794fe98c3d3789406954d1406aa60fe79df4ab42))

## [1.2.7](https://github.com/scratchfoundation/scratch-render/compare/v1.2.6...v1.2.7) (2024-09-28)


### Bug Fixes

* **deps:** lock file maintenance ([3ce6b7f](https://github.com/scratchfoundation/scratch-render/commit/3ce6b7f264e6f50fea54c88ba5b4493881f125f8))

## [1.2.6](https://github.com/scratchfoundation/scratch-render/compare/v1.2.5...v1.2.6) (2024-09-28)


### Bug Fixes

* **deps:** lock file maintenance ([9a1e1ab](https://github.com/scratchfoundation/scratch-render/commit/9a1e1ab4cadf5a0b92aeabd243d7a94043c8b24b))

## [1.2.5](https://github.com/scratchfoundation/scratch-render/compare/v1.2.4...v1.2.5) (2024-09-27)


### Bug Fixes

* **deps:** lock file maintenance ([a7fff42](https://github.com/scratchfoundation/scratch-render/commit/a7fff42245f8b507acdb5d2750fa5d68050d61ac))
* **deps:** update dependency scratch-storage to v2.3.263 ([cfa1c6e](https://github.com/scratchfoundation/scratch-render/commit/cfa1c6e6f354951301e5775700910e9a80149a5d))

## [1.2.4](https://github.com/scratchfoundation/scratch-render/compare/v1.2.3...v1.2.4) (2024-09-27)


### Bug Fixes

* **deps:** lock file maintenance ([5eb3012](https://github.com/scratchfoundation/scratch-render/commit/5eb30126b338a9a9d123151b284b50ca5c312529))

## [1.2.3](https://github.com/scratchfoundation/scratch-render/compare/v1.2.2...v1.2.3) (2024-09-26)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.105 ([23e644f](https://github.com/scratchfoundation/scratch-render/commit/23e644f1e5c6cfa2b39976497107cf8fa2231ee7))

## [1.2.2](https://github.com/scratchfoundation/scratch-render/compare/v1.2.1...v1.2.2) (2024-09-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.262 ([8c09a91](https://github.com/scratchfoundation/scratch-render/commit/8c09a91d31b2f46f336ea9785820106ce51a3b4d))

## [1.2.1](https://github.com/scratchfoundation/scratch-render/compare/v1.2.0...v1.2.1) (2024-09-26)


### Bug Fixes

* **deps:** lock file maintenance ([3743a9b](https://github.com/scratchfoundation/scratch-render/commit/3743a9b36edd76b53e52a8054df4311c743ca4c4))

# [1.2.0](https://github.com/scratchfoundation/scratch-render/compare/v1.1.53...v1.2.0) (2024-09-25)


### Features

* **deps:** update dependency scratch-webpack-configuration to v1.6.0 ([94a542a](https://github.com/scratchfoundation/scratch-render/commit/94a542a3f28d360e3f20dc2156b47ea8b15e2968))

## [1.1.53](https://github.com/scratchfoundation/scratch-render/compare/v1.1.52...v1.1.53) (2024-09-25)


### Bug Fixes

* **deps:** lock file maintenance ([2d8a700](https://github.com/scratchfoundation/scratch-render/commit/2d8a700ba549f96fb5b015fc5cb7bd3db7e91ce8))
* **deps:** update dependency scratch-storage to v2.3.261 ([af1293c](https://github.com/scratchfoundation/scratch-render/commit/af1293c690194fe4f25856d8e5e956a137d12d63))

## [1.1.52](https://github.com/scratchfoundation/scratch-render/compare/v1.1.51...v1.1.52) (2024-09-25)


### Bug Fixes

* **deps:** lock file maintenance ([88ad4ba](https://github.com/scratchfoundation/scratch-render/commit/88ad4ba83bcb7c3029590cf2382a95a2a17ddd26))

## [1.1.51](https://github.com/scratchfoundation/scratch-render/compare/v1.1.50...v1.1.51) (2024-09-24)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.260 ([cc86971](https://github.com/scratchfoundation/scratch-render/commit/cc86971a7d391f616a7beb0c119407fe48171ffd))

## [1.1.50](https://github.com/scratchfoundation/scratch-render/compare/v1.1.49...v1.1.50) (2024-09-24)


### Bug Fixes

* **deps:** lock file maintenance ([fc5f2aa](https://github.com/scratchfoundation/scratch-render/commit/fc5f2aadeb2437a7787d58187f1ae0429aa61db4))

## [1.1.49](https://github.com/scratchfoundation/scratch-render/compare/v1.1.48...v1.1.49) (2024-09-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.258 ([eb3c942](https://github.com/scratchfoundation/scratch-render/commit/eb3c942c19b31c0eb4af4c1f5f1f09b76a0962c7))

## [1.1.48](https://github.com/scratchfoundation/scratch-render/compare/v1.1.47...v1.1.48) (2024-09-23)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.104 ([5c932ba](https://github.com/scratchfoundation/scratch-render/commit/5c932ba4ff8faba14d096a0bfd5471c4f2ccfd54))

## [1.1.47](https://github.com/scratchfoundation/scratch-render/compare/v1.1.46...v1.1.47) (2024-09-23)


### Bug Fixes

* **deps:** lock file maintenance ([fdf58ea](https://github.com/scratchfoundation/scratch-render/commit/fdf58ea0bb967ab9fb267210a61fceb10640d56b))

## [1.1.46](https://github.com/scratchfoundation/scratch-render/compare/v1.1.45...v1.1.46) (2024-09-22)


### Bug Fixes

* **deps:** lock file maintenance ([790f559](https://github.com/scratchfoundation/scratch-render/commit/790f559ef3fbf9ea28f4586adbb39d87e131ae5d))

## [1.1.45](https://github.com/scratchfoundation/scratch-render/compare/v1.1.44...v1.1.45) (2024-09-22)


### Bug Fixes

* **deps:** lock file maintenance ([522a45c](https://github.com/scratchfoundation/scratch-render/commit/522a45c5143148cc6d74b7eb973279fc64543bdf))

## [1.1.44](https://github.com/scratchfoundation/scratch-render/compare/v1.1.43...v1.1.44) (2024-09-21)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.257 ([1d72650](https://github.com/scratchfoundation/scratch-render/commit/1d726505cc2a9f9c4e6432b0844bb3b3d85725b7))

## [1.1.43](https://github.com/scratchfoundation/scratch-render/compare/v1.1.42...v1.1.43) (2024-09-21)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.103 ([e42a728](https://github.com/scratchfoundation/scratch-render/commit/e42a7282f59125fec3920c9371f6c51d02615f41))

## [1.1.42](https://github.com/scratchfoundation/scratch-render/compare/v1.1.41...v1.1.42) (2024-09-21)


### Bug Fixes

* **deps:** lock file maintenance ([cd640ba](https://github.com/scratchfoundation/scratch-render/commit/cd640ba8e33f7fdceb927403c4da3d2db35f3e95))

## [1.1.41](https://github.com/scratchfoundation/scratch-render/compare/v1.1.40...v1.1.41) (2024-09-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.255 ([92decf1](https://github.com/scratchfoundation/scratch-render/commit/92decf166ac5fb02698c9bcb3fd8fed8e9b56e8e))

## [1.1.40](https://github.com/scratchfoundation/scratch-render/compare/v1.1.39...v1.1.40) (2024-09-20)


### Bug Fixes

* **deps:** lock file maintenance ([c435a40](https://github.com/scratchfoundation/scratch-render/commit/c435a404357e1003233f80e4558c9d4027c87ef8))

## [1.1.39](https://github.com/scratchfoundation/scratch-render/compare/v1.1.38...v1.1.39) (2024-09-20)


### Bug Fixes

* **deps:** lock file maintenance ([ff28d60](https://github.com/scratchfoundation/scratch-render/commit/ff28d604f375880f9497ccf0e8bf018a25ba8418))

## [1.1.38](https://github.com/scratchfoundation/scratch-render/compare/v1.1.37...v1.1.38) (2024-09-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.254 ([b9810a0](https://github.com/scratchfoundation/scratch-render/commit/b9810a0a2445d648bbaf0fea88ca2c0d8a6ab3b5))

## [1.1.37](https://github.com/scratchfoundation/scratch-render/compare/v1.1.36...v1.1.37) (2024-09-19)


### Bug Fixes

* **deps:** lock file maintenance ([8954e45](https://github.com/scratchfoundation/scratch-render/commit/8954e451c27bfbc5c28d274033148ce7f2d3db6a))

## [1.1.36](https://github.com/scratchfoundation/scratch-render/compare/v1.1.35...v1.1.36) (2024-09-19)


### Bug Fixes

* **deps:** lock file maintenance ([3d6f453](https://github.com/scratchfoundation/scratch-render/commit/3d6f45340513ec643ccf5908b2603a87f800cc58))

## [1.1.35](https://github.com/scratchfoundation/scratch-render/compare/v1.1.34...v1.1.35) (2024-09-18)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.102 ([70d4659](https://github.com/scratchfoundation/scratch-render/commit/70d4659251ad5d8633c727d6f0af094eea9851a6))

## [1.1.34](https://github.com/scratchfoundation/scratch-render/compare/v1.1.33...v1.1.34) (2024-09-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.253 ([101c412](https://github.com/scratchfoundation/scratch-render/commit/101c412f3d9fa86eb6665543f1a3d63cc98fde9c))

## [1.1.33](https://github.com/scratchfoundation/scratch-render/compare/v1.1.32...v1.1.33) (2024-09-18)


### Bug Fixes

* **deps:** lock file maintenance ([cc6ca73](https://github.com/scratchfoundation/scratch-render/commit/cc6ca7396a08d57cf0f765d818c32a54d1b8a35d))

## [1.1.32](https://github.com/scratchfoundation/scratch-render/compare/v1.1.31...v1.1.32) (2024-09-17)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.252 ([9e00264](https://github.com/scratchfoundation/scratch-render/commit/9e00264cc57eb7c65b6ddf2aad1d54b6670179e5))

## [1.1.31](https://github.com/scratchfoundation/scratch-render/compare/v1.1.30...v1.1.31) (2024-09-17)


### Bug Fixes

* **deps:** lock file maintenance ([ffc072a](https://github.com/scratchfoundation/scratch-render/commit/ffc072a24f60b4d090237763c800b6fe40187ff6))

## [1.1.30](https://github.com/scratchfoundation/scratch-render/compare/v1.1.29...v1.1.30) (2024-09-16)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.251 ([def70f7](https://github.com/scratchfoundation/scratch-render/commit/def70f78d09b339c0ea394581ba4db258b22df20))

## [1.1.29](https://github.com/scratchfoundation/scratch-render/compare/v1.1.28...v1.1.29) (2024-09-16)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.101 ([9c2a155](https://github.com/scratchfoundation/scratch-render/commit/9c2a15581184e01237e5db1c567746fd3b21e176))

## [1.1.28](https://github.com/scratchfoundation/scratch-render/compare/v1.1.27...v1.1.28) (2024-09-16)


### Bug Fixes

* **deps:** lock file maintenance ([c287a56](https://github.com/scratchfoundation/scratch-render/commit/c287a56aa887d18481ca84a414ecefc716c1ecd8))

## [1.1.27](https://github.com/scratchfoundation/scratch-render/compare/v1.1.26...v1.1.27) (2024-09-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.250 ([01523d1](https://github.com/scratchfoundation/scratch-render/commit/01523d1c35d855769c01550204c6f2dde917b743))

## [1.1.26](https://github.com/scratchfoundation/scratch-render/compare/v1.1.25...v1.1.26) (2024-09-15)


### Bug Fixes

* **deps:** lock file maintenance ([b5b7fac](https://github.com/scratchfoundation/scratch-render/commit/b5b7fac472adf0f7cd44edb1a00fd0887cea72f0))

## [1.1.25](https://github.com/scratchfoundation/scratch-render/compare/v1.1.24...v1.1.25) (2024-09-15)


### Bug Fixes

* **deps:** lock file maintenance ([71684d3](https://github.com/scratchfoundation/scratch-render/commit/71684d3f48d0e2268ccfb646bef4b47b5b0779cc))

## [1.1.24](https://github.com/scratchfoundation/scratch-render/compare/v1.1.23...v1.1.24) (2024-09-14)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.249 ([a250785](https://github.com/scratchfoundation/scratch-render/commit/a2507855737c0a57fc1231cad1f42db18b189333))

## [1.1.23](https://github.com/scratchfoundation/scratch-render/compare/v1.1.22...v1.1.23) (2024-09-14)


### Bug Fixes

* **deps:** lock file maintenance ([308745b](https://github.com/scratchfoundation/scratch-render/commit/308745b1b6be36dc922613bd03f15643a5d7f445))

## [1.1.22](https://github.com/scratchfoundation/scratch-render/compare/v1.1.21...v1.1.22) (2024-09-14)


### Bug Fixes

* **deps:** lock file maintenance ([69e23f9](https://github.com/scratchfoundation/scratch-render/commit/69e23f9b767734e59cbf05a4aa14883b37e3adee))

## [1.1.21](https://github.com/scratchfoundation/scratch-render/compare/v1.1.20...v1.1.21) (2024-09-13)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.248 ([e00c7b0](https://github.com/scratchfoundation/scratch-render/commit/e00c7b0e24124564b4f9a6000ff9960b244d5d6a))

## [1.1.20](https://github.com/scratchfoundation/scratch-render/compare/v1.1.19...v1.1.20) (2024-09-13)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.100 ([405dc52](https://github.com/scratchfoundation/scratch-render/commit/405dc5246fa7fed386dd15505fee2c9903ba6209))

## [1.1.19](https://github.com/scratchfoundation/scratch-render/compare/v1.1.18...v1.1.19) (2024-09-13)


### Bug Fixes

* **deps:** lock file maintenance ([20931f5](https://github.com/scratchfoundation/scratch-render/commit/20931f59d099e329d39c72bf46b094ed7b29d8bb))

## [1.1.18](https://github.com/scratchfoundation/scratch-render/compare/v1.1.17...v1.1.18) (2024-09-13)


### Bug Fixes

* **deps:** lock file maintenance ([8af4a3f](https://github.com/scratchfoundation/scratch-render/commit/8af4a3fe84fb8f2b9e65dd24bcef950a263a6de8))

## [1.1.17](https://github.com/scratchfoundation/scratch-render/compare/v1.1.16...v1.1.17) (2024-09-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.246 ([fb633cc](https://github.com/scratchfoundation/scratch-render/commit/fb633ccf0e9be35055623d5b1253131cba3350fe))

## [1.1.16](https://github.com/scratchfoundation/scratch-render/compare/v1.1.15...v1.1.16) (2024-09-12)


### Bug Fixes

* **deps:** lock file maintenance ([10b2db5](https://github.com/scratchfoundation/scratch-render/commit/10b2db57b2795f4e7bc0f434958f16d3ee70f2e0))

## [1.1.15](https://github.com/scratchfoundation/scratch-render/compare/v1.1.14...v1.1.15) (2024-09-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.244 ([fe32148](https://github.com/scratchfoundation/scratch-render/commit/fe32148eb3489b33f69bb7b49cab43340677b3bd))

## [1.1.14](https://github.com/scratchfoundation/scratch-render/compare/v1.1.13...v1.1.14) (2024-09-11)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.99 ([4edbadd](https://github.com/scratchfoundation/scratch-render/commit/4edbadd45cf28d66ea4b27fc61e0cc33da8bc12e))

## [1.1.13](https://github.com/scratchfoundation/scratch-render/compare/v1.1.12...v1.1.13) (2024-09-11)


### Bug Fixes

* **deps:** update dependency scratch-webpack-configuration to v1.5.1 ([81187f9](https://github.com/scratchfoundation/scratch-render/commit/81187f98fe30d45e18be0498214c1d090d10d3b5))

## [1.1.12](https://github.com/scratchfoundation/scratch-render/compare/v1.1.11...v1.1.12) (2024-09-11)


### Bug Fixes

* **deps:** lock file maintenance ([897f6b2](https://github.com/scratchfoundation/scratch-render/commit/897f6b282d64831e12f5434c5e5717c6d315e2d8))
* **deps:** update dependency scratch-semantic-release-config to v1.0.16 ([56e0a7a](https://github.com/scratchfoundation/scratch-render/commit/56e0a7ad4529012f060393143efbbfbc95c1175a))

## [1.1.11](https://github.com/scratchfoundation/scratch-render/compare/v1.1.10...v1.1.11) (2024-09-11)


### Bug Fixes

* **deps:** lock file maintenance ([8682c96](https://github.com/scratchfoundation/scratch-render/commit/8682c96f4ea33a98557aa12d9cd0088a30011739))
* **deps:** update dependency scratch-render-fonts to v1.0.98 ([ebe8520](https://github.com/scratchfoundation/scratch-render/commit/ebe8520c32c7787b9af3cc2a0408c2c6536ecce6))

## [1.1.10](https://github.com/scratchfoundation/scratch-render/compare/v1.1.9...v1.1.10) (2024-09-11)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.15 ([6fd958a](https://github.com/scratchfoundation/scratch-render/commit/6fd958adaedccbdd62ad38d9076314b70e1d7243))

## [1.1.9](https://github.com/scratchfoundation/scratch-render/compare/v1.1.8...v1.1.9) (2024-09-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.241 ([e1b8ff8](https://github.com/scratchfoundation/scratch-render/commit/e1b8ff85e8feef3e3676bcbfaf1f5cba49d3020f))

## [1.1.8](https://github.com/scratchfoundation/scratch-render/compare/v1.1.7...v1.1.8) (2024-09-10)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.97 ([3679881](https://github.com/scratchfoundation/scratch-render/commit/367988176fc5bfcca46f6c5ceccd802f674adc5b))

## [1.1.7](https://github.com/scratchfoundation/scratch-render/compare/v1.1.6...v1.1.7) (2024-09-10)


### Bug Fixes

* **deps:** lock file maintenance ([e6d0eba](https://github.com/scratchfoundation/scratch-render/commit/e6d0ebac43762c84d47d32ee2f4f59bba3320f6d))

## [1.1.6](https://github.com/scratchfoundation/scratch-render/compare/v1.1.5...v1.1.6) (2024-09-10)


### Bug Fixes

* **deps:** lock file maintenance ([8de962f](https://github.com/scratchfoundation/scratch-render/commit/8de962f0f52121ca73b4c2ec7b3e7dea4be4822c))

## [1.1.5](https://github.com/scratchfoundation/scratch-render/compare/v1.1.4...v1.1.5) (2024-09-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.239 ([f28b647](https://github.com/scratchfoundation/scratch-render/commit/f28b647af1e0c0a14d2a0d2543043e52b5bf3e13))

## [1.1.4](https://github.com/scratchfoundation/scratch-render/compare/v1.1.3...v1.1.4) (2024-09-09)


### Bug Fixes

* **deps:** lock file maintenance ([8cd340c](https://github.com/scratchfoundation/scratch-render/commit/8cd340c00b074ac34a993ad698b0b313e728c5f4))

## [1.1.3](https://github.com/scratchfoundation/scratch-render/compare/v1.1.2...v1.1.3) (2024-09-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.238 ([100c643](https://github.com/scratchfoundation/scratch-render/commit/100c643347b61748c68f84c3d5aaf6725b721725))

## [1.1.2](https://github.com/scratchfoundation/scratch-render/compare/v1.1.1...v1.1.2) (2024-09-08)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.96 ([4ddc21c](https://github.com/scratchfoundation/scratch-render/commit/4ddc21c615a4a03ceae24276d9c276cb85650cc6))

## [1.1.1](https://github.com/scratchfoundation/scratch-render/compare/v1.1.0...v1.1.1) (2024-09-08)


### Bug Fixes

* **deps:** lock file maintenance ([fef38c3](https://github.com/scratchfoundation/scratch-render/commit/fef38c3f1830a2b298f99d693a8711ef8ddb0e1c))

# [1.1.0](https://github.com/scratchfoundation/scratch-render/compare/v1.0.336...v1.1.0) (2024-09-07)


### Features

* **deps:** update dependency scratch-webpack-configuration to v1.5.0 ([5f490d3](https://github.com/scratchfoundation/scratch-render/commit/5f490d3057f18c28bcf1b4751c2c180541db9d42))

## [1.0.336](https://github.com/scratchfoundation/scratch-render/compare/v1.0.335...v1.0.336) (2024-09-07)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.237 ([b1e03cc](https://github.com/scratchfoundation/scratch-render/commit/b1e03ccba3e629b19e13b64bb963cc7e5e0313d4))

## [1.0.335](https://github.com/scratchfoundation/scratch-render/compare/v1.0.334...v1.0.335) (2024-09-07)


### Bug Fixes

* **deps:** lock file maintenance ([55cf01e](https://github.com/scratchfoundation/scratch-render/commit/55cf01e8fd14f1818ce68691711c169455d5460e))

## [1.0.334](https://github.com/scratchfoundation/scratch-render/compare/v1.0.333...v1.0.334) (2024-09-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.236 ([c598901](https://github.com/scratchfoundation/scratch-render/commit/c598901ce98081fe3fd16214b484ee0e183b6a66))

## [1.0.333](https://github.com/scratchfoundation/scratch-render/compare/v1.0.332...v1.0.333) (2024-09-06)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.95 ([2021aa3](https://github.com/scratchfoundation/scratch-render/commit/2021aa37f70d2785977621edc3f44147ed401b6c))

## [1.0.332](https://github.com/scratchfoundation/scratch-render/compare/v1.0.331...v1.0.332) (2024-09-06)


### Bug Fixes

* **deps:** lock file maintenance ([4d69851](https://github.com/scratchfoundation/scratch-render/commit/4d69851afe7df59ce4f4b92fc2093c54b316db9e))

## [1.0.331](https://github.com/scratchfoundation/scratch-render/compare/v1.0.330...v1.0.331) (2024-09-06)


### Bug Fixes

* **deps:** lock file maintenance ([9e43a89](https://github.com/scratchfoundation/scratch-render/commit/9e43a896ccde46476aef79173fb3a30eb80a780c))

## [1.0.330](https://github.com/scratchfoundation/scratch-render/compare/v1.0.329...v1.0.330) (2024-09-06)


### Bug Fixes

* **deps:** lock file maintenance ([e8e73a5](https://github.com/scratchfoundation/scratch-render/commit/e8e73a5642d869e6241e67588bb73b861931fa90))

## [1.0.329](https://github.com/scratchfoundation/scratch-render/compare/v1.0.328...v1.0.329) (2024-09-05)


### Bug Fixes

* **deps:** lock file maintenance ([118198e](https://github.com/scratchfoundation/scratch-render/commit/118198ecda323728b904b85d2c42243607885e89))
* **deps:** update dependency scratch-storage to v2.3.235 ([4c8bb92](https://github.com/scratchfoundation/scratch-render/commit/4c8bb92149f27815bab7de037abf2eb3a269946b))

## [1.0.328](https://github.com/scratchfoundation/scratch-render/compare/v1.0.327...v1.0.328) (2024-09-05)


### Bug Fixes

* **deps:** lock file maintenance ([4b573ed](https://github.com/scratchfoundation/scratch-render/commit/4b573edaae86db2d51b45c0ba82348cb70e3bdfc))

## [1.0.327](https://github.com/scratchfoundation/scratch-render/compare/v1.0.326...v1.0.327) (2024-09-04)


### Bug Fixes

* **deps:** lock file maintenance ([5744735](https://github.com/scratchfoundation/scratch-render/commit/5744735b2b8645a4c890811d68f6cf075329f147))
* **deps:** update dependency scratch-storage to v2.3.234 ([4730cce](https://github.com/scratchfoundation/scratch-render/commit/4730ccef6f7fbf563a614ee719853df8645e1312))

## [1.0.326](https://github.com/scratchfoundation/scratch-render/compare/v1.0.325...v1.0.326) (2024-09-04)


### Bug Fixes

* **deps:** lock file maintenance ([cb4c088](https://github.com/scratchfoundation/scratch-render/commit/cb4c088f9af88296bf13ebe0a085ebe6b3e70e2e))

## [1.0.325](https://github.com/scratchfoundation/scratch-render/compare/v1.0.324...v1.0.325) (2024-09-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.233 ([5badceb](https://github.com/scratchfoundation/scratch-render/commit/5badceb5643760514f77fbc552d8d8201eaa1106))

## [1.0.324](https://github.com/scratchfoundation/scratch-render/compare/v1.0.323...v1.0.324) (2024-09-03)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.94 ([a4f8796](https://github.com/scratchfoundation/scratch-render/commit/a4f879620c9ddde48f43b3516a135d71a6e87b53))

## [1.0.323](https://github.com/scratchfoundation/scratch-render/compare/v1.0.322...v1.0.323) (2024-09-03)


### Bug Fixes

* **deps:** lock file maintenance ([fec2886](https://github.com/scratchfoundation/scratch-render/commit/fec28868bed2fbdb2ed952491f0f3549208f99da))

## [1.0.322](https://github.com/scratchfoundation/scratch-render/compare/v1.0.321...v1.0.322) (2024-09-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.232 ([c0abe1d](https://github.com/scratchfoundation/scratch-render/commit/c0abe1d57783f3be276e98dd459b4cd1c32d8021))

## [1.0.321](https://github.com/scratchfoundation/scratch-render/compare/v1.0.320...v1.0.321) (2024-09-02)


### Bug Fixes

* **deps:** lock file maintenance ([4efff11](https://github.com/scratchfoundation/scratch-render/commit/4efff117630973e300e7aef7978575617f38f03f))

## [1.0.320](https://github.com/scratchfoundation/scratch-render/compare/v1.0.319...v1.0.320) (2024-09-01)


### Bug Fixes

* **deps:** lock file maintenance ([933c39b](https://github.com/scratchfoundation/scratch-render/commit/933c39b363f0f6907bf8f8b60adfe73b24718cb5))

## [1.0.319](https://github.com/scratchfoundation/scratch-render/compare/v1.0.318...v1.0.319) (2024-09-01)


### Bug Fixes

* **deps:** lock file maintenance ([5f1adec](https://github.com/scratchfoundation/scratch-render/commit/5f1adeca46201fdaee67cd8fe3de60ecf01e7c4d))

## [1.0.318](https://github.com/scratchfoundation/scratch-render/compare/v1.0.317...v1.0.318) (2024-08-31)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.231 ([d18bfd9](https://github.com/scratchfoundation/scratch-render/commit/d18bfd91f5cb722b4d3f18a42bbfa5bb6cd02eab))

## [1.0.317](https://github.com/scratchfoundation/scratch-render/compare/v1.0.316...v1.0.317) (2024-08-31)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.93 ([9058fe3](https://github.com/scratchfoundation/scratch-render/commit/9058fe32cbda1a8621d1362f674aa42cc15ec2ca))

## [1.0.316](https://github.com/scratchfoundation/scratch-render/compare/v1.0.315...v1.0.316) (2024-08-31)


### Bug Fixes

* **deps:** lock file maintenance ([5b6b1b0](https://github.com/scratchfoundation/scratch-render/commit/5b6b1b00b4a51e1fdec2ad595df72bde67d32ce0))

## [1.0.315](https://github.com/scratchfoundation/scratch-render/compare/v1.0.314...v1.0.315) (2024-08-30)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.230 ([48fa6b8](https://github.com/scratchfoundation/scratch-render/commit/48fa6b8c3ab7f92af20734717efcf3a1436233fd))

## [1.0.314](https://github.com/scratchfoundation/scratch-render/compare/v1.0.313...v1.0.314) (2024-08-30)


### Bug Fixes

* **deps:** lock file maintenance ([057d0ac](https://github.com/scratchfoundation/scratch-render/commit/057d0acf95724a094a1bbaee87eef18f4082bd23))

## [1.0.313](https://github.com/scratchfoundation/scratch-render/compare/v1.0.312...v1.0.313) (2024-08-29)


### Bug Fixes

* **deps:** lock file maintenance ([bca071b](https://github.com/scratchfoundation/scratch-render/commit/bca071b35c2f3bed7abd27cc4662761972a39683))

## [1.0.312](https://github.com/scratchfoundation/scratch-render/compare/v1.0.311...v1.0.312) (2024-08-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.229 ([8d9b7e8](https://github.com/scratchfoundation/scratch-render/commit/8d9b7e8e76c62c33ea180df31b68115a9d4d2b48))

## [1.0.311](https://github.com/scratchfoundation/scratch-render/compare/v1.0.310...v1.0.311) (2024-08-28)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.92 ([ab7c3e4](https://github.com/scratchfoundation/scratch-render/commit/ab7c3e48eba5065c4d809a2ed98693fa3eff3b64))

## [1.0.310](https://github.com/scratchfoundation/scratch-render/compare/v1.0.309...v1.0.310) (2024-08-28)


### Bug Fixes

* **deps:** lock file maintenance ([6d1e49b](https://github.com/scratchfoundation/scratch-render/commit/6d1e49b089c4a0d67cefe01478094343fb75485d))

## [1.0.309](https://github.com/scratchfoundation/scratch-render/compare/v1.0.308...v1.0.309) (2024-08-27)


### Bug Fixes

* **deps:** lock file maintenance ([7d177dc](https://github.com/scratchfoundation/scratch-render/commit/7d177dc568f66baf91f6b9f3d1e5813f6c6b1390))

## [1.0.308](https://github.com/scratchfoundation/scratch-render/compare/v1.0.307...v1.0.308) (2024-08-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.228 ([57c8b9e](https://github.com/scratchfoundation/scratch-render/commit/57c8b9ecd4afa9f3f31ee3bd4cb9acf023b27178))

## [1.0.307](https://github.com/scratchfoundation/scratch-render/compare/v1.0.306...v1.0.307) (2024-08-26)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.91 ([b1656e5](https://github.com/scratchfoundation/scratch-render/commit/b1656e5dbef089bee1b1bac8c9c4fd11708f4494))

## [1.0.306](https://github.com/scratchfoundation/scratch-render/compare/v1.0.305...v1.0.306) (2024-08-26)


### Bug Fixes

* **deps:** lock file maintenance ([ce81031](https://github.com/scratchfoundation/scratch-render/commit/ce81031238bc5136dc2d4911d705f235f8785014))

## [1.0.305](https://github.com/scratchfoundation/scratch-render/compare/v1.0.304...v1.0.305) (2024-08-25)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.227 ([2492307](https://github.com/scratchfoundation/scratch-render/commit/249230700bbaefd9382e35318b076e8bfd0566eb))

## [1.0.304](https://github.com/scratchfoundation/scratch-render/compare/v1.0.303...v1.0.304) (2024-08-25)


### Bug Fixes

* **deps:** lock file maintenance ([ad62fbb](https://github.com/scratchfoundation/scratch-render/commit/ad62fbb2ac837601c9ad07f40011d30cfa1a69b4))

## [1.0.303](https://github.com/scratchfoundation/scratch-render/compare/v1.0.302...v1.0.303) (2024-08-25)


### Bug Fixes

* **deps:** lock file maintenance ([d744359](https://github.com/scratchfoundation/scratch-render/commit/d7443592c2de4bc7cd90bf87cca15a6c7d7bacc4))

## [1.0.302](https://github.com/scratchfoundation/scratch-render/compare/v1.0.301...v1.0.302) (2024-08-24)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.226 ([33a85c7](https://github.com/scratchfoundation/scratch-render/commit/33a85c7702aa7151121fc02e1f0a8e33930f09cb))

## [1.0.301](https://github.com/scratchfoundation/scratch-render/compare/v1.0.300...v1.0.301) (2024-08-24)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.90 ([d7158dc](https://github.com/scratchfoundation/scratch-render/commit/d7158dc5e33f10dc86051725cc60faa5af89a809))

## [1.0.300](https://github.com/scratchfoundation/scratch-render/compare/v1.0.299...v1.0.300) (2024-08-24)


### Bug Fixes

* **deps:** lock file maintenance ([d7bb58c](https://github.com/scratchfoundation/scratch-render/commit/d7bb58c879842d87c80630ead30bd62b11f916eb))

## [1.0.299](https://github.com/scratchfoundation/scratch-render/compare/v1.0.298...v1.0.299) (2024-08-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.224 ([cc4ef44](https://github.com/scratchfoundation/scratch-render/commit/cc4ef44adb65d16900367b1934c2a6c71406a330))

## [1.0.298](https://github.com/scratchfoundation/scratch-render/compare/v1.0.297...v1.0.298) (2024-08-23)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.89 ([3ca201a](https://github.com/scratchfoundation/scratch-render/commit/3ca201a8a00c248be325a6a24bf5d19b8338ef90))

## [1.0.297](https://github.com/scratchfoundation/scratch-render/compare/v1.0.296...v1.0.297) (2024-08-23)


### Bug Fixes

* **deps:** lock file maintenance ([b92abf7](https://github.com/scratchfoundation/scratch-render/commit/b92abf7bbc8e0777025606566f7237318d48842b))

## [1.0.296](https://github.com/scratchfoundation/scratch-render/compare/v1.0.295...v1.0.296) (2024-08-22)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.223 ([f4af15c](https://github.com/scratchfoundation/scratch-render/commit/f4af15c8988de5525ec45b21cebc6c07fd1a2423))

## [1.0.295](https://github.com/scratchfoundation/scratch-render/compare/v1.0.294...v1.0.295) (2024-08-22)


### Bug Fixes

* **deps:** lock file maintenance ([0c83574](https://github.com/scratchfoundation/scratch-render/commit/0c83574097e134d7c1b12f3818e0d3ac12e5c716))

## [1.0.294](https://github.com/scratchfoundation/scratch-render/compare/v1.0.293...v1.0.294) (2024-08-22)


### Bug Fixes

* **deps:** lock file maintenance ([12132da](https://github.com/scratchfoundation/scratch-render/commit/12132daffc433586980821c6815cdade4a33f278))

## [1.0.293](https://github.com/scratchfoundation/scratch-render/compare/v1.0.292...v1.0.293) (2024-08-21)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.221 ([1567745](https://github.com/scratchfoundation/scratch-render/commit/156774524d4c692d7568a6d833d0c92d6fbc0be0))

## [1.0.292](https://github.com/scratchfoundation/scratch-render/compare/v1.0.291...v1.0.292) (2024-08-21)


### Bug Fixes

* **deps:** lock file maintenance ([043f4ee](https://github.com/scratchfoundation/scratch-render/commit/043f4ee08a7bb52912920758b7ebc510c0de533f))

## [1.0.291](https://github.com/scratchfoundation/scratch-render/compare/v1.0.290...v1.0.291) (2024-08-20)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.88 ([6ab13f3](https://github.com/scratchfoundation/scratch-render/commit/6ab13f380394806867e18e7c1d56754edbc42ef7))

## [1.0.290](https://github.com/scratchfoundation/scratch-render/compare/v1.0.289...v1.0.290) (2024-08-20)


### Bug Fixes

* **deps:** lock file maintenance ([a7f6b6c](https://github.com/scratchfoundation/scratch-render/commit/a7f6b6cd1dd6b838db7d4add7d808fedcc3d1fc0))
* **deps:** update dependency scratch-storage to v2.3.219 ([d6f0bf1](https://github.com/scratchfoundation/scratch-render/commit/d6f0bf14eecb7fd34bf2a73ee23e115a38cfeae1))

## [1.0.289](https://github.com/scratchfoundation/scratch-render/compare/v1.0.288...v1.0.289) (2024-08-20)


### Bug Fixes

* **deps:** lock file maintenance ([3e66e1b](https://github.com/scratchfoundation/scratch-render/commit/3e66e1b0d6cddc064022cab82bef6db35d8d5a79))

## [1.0.288](https://github.com/scratchfoundation/scratch-render/compare/v1.0.287...v1.0.288) (2024-08-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.218 ([fc141c1](https://github.com/scratchfoundation/scratch-render/commit/fc141c198680e454c80a1d4fe9652d4235d18684))

## [1.0.287](https://github.com/scratchfoundation/scratch-render/compare/v1.0.286...v1.0.287) (2024-08-19)


### Bug Fixes

* **deps:** lock file maintenance ([a1bfcbb](https://github.com/scratchfoundation/scratch-render/commit/a1bfcbb9e790fba6b5162703da22c0ef412c0d87))

## [1.0.286](https://github.com/scratchfoundation/scratch-render/compare/v1.0.285...v1.0.286) (2024-08-18)


### Bug Fixes

* **deps:** lock file maintenance ([700987a](https://github.com/scratchfoundation/scratch-render/commit/700987af38369caa21394e4ea2d04011bc11b84a))
* **deps:** update dependency scratch-storage to v2.3.217 ([3c3d724](https://github.com/scratchfoundation/scratch-render/commit/3c3d72414824c87e2323b88b9f2892e0dabde49f))

## [1.0.285](https://github.com/scratchfoundation/scratch-render/compare/v1.0.284...v1.0.285) (2024-08-18)


### Bug Fixes

* **deps:** lock file maintenance ([f315fa0](https://github.com/scratchfoundation/scratch-render/commit/f315fa0882608d795749b9654251e207fb18fb2f))

## [1.0.284](https://github.com/scratchfoundation/scratch-render/compare/v1.0.283...v1.0.284) (2024-08-18)


### Bug Fixes

* **deps:** lock file maintenance ([74ccf3d](https://github.com/scratchfoundation/scratch-render/commit/74ccf3de621a78367c7ecbfe84563a68abe1b70b))

## [1.0.283](https://github.com/scratchfoundation/scratch-render/compare/v1.0.282...v1.0.283) (2024-08-17)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.87 ([2cc3a04](https://github.com/scratchfoundation/scratch-render/commit/2cc3a04143c7ab1cc0ba39d14bac996d81ed7a40))

## [1.0.282](https://github.com/scratchfoundation/scratch-render/compare/v1.0.281...v1.0.282) (2024-08-17)


### Bug Fixes

* **deps:** lock file maintenance ([4422c6c](https://github.com/scratchfoundation/scratch-render/commit/4422c6c08aa57cc52ef9a0cd6906c54549aea915))
* **deps:** update dependency scratch-storage to v2.3.215 ([95d03a1](https://github.com/scratchfoundation/scratch-render/commit/95d03a1de0bc6cebec74cb5fcbf0e9aed37dcf19))

## [1.0.281](https://github.com/scratchfoundation/scratch-render/compare/v1.0.280...v1.0.281) (2024-08-17)


### Bug Fixes

* **deps:** lock file maintenance ([f37ba73](https://github.com/scratchfoundation/scratch-render/commit/f37ba73915ae44279a3356b8ec6af0ca677f2571))

## [1.0.280](https://github.com/scratchfoundation/scratch-render/compare/v1.0.279...v1.0.280) (2024-08-16)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.214 ([fc23777](https://github.com/scratchfoundation/scratch-render/commit/fc2377703136cacace7c6afa2fe71b3d84f0fab6))

## [1.0.279](https://github.com/scratchfoundation/scratch-render/compare/v1.0.278...v1.0.279) (2024-08-16)


### Bug Fixes

* **deps:** lock file maintenance ([0ed02f2](https://github.com/scratchfoundation/scratch-render/commit/0ed02f20fb3d76dc255d11deba1531368bb505cb))

## [1.0.278](https://github.com/scratchfoundation/scratch-render/compare/v1.0.277...v1.0.278) (2024-08-16)


### Bug Fixes

* **deps:** lock file maintenance ([f70df1c](https://github.com/scratchfoundation/scratch-render/commit/f70df1c2e1a5f6f4bd41cdbcea9ecdd5f55eb7e8))

## [1.0.277](https://github.com/scratchfoundation/scratch-render/compare/v1.0.276...v1.0.277) (2024-08-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.213 ([cf9c63e](https://github.com/scratchfoundation/scratch-render/commit/cf9c63e2db295abf4c5a6a75839dc9065e59520d))

## [1.0.276](https://github.com/scratchfoundation/scratch-render/compare/v1.0.275...v1.0.276) (2024-08-15)


### Bug Fixes

* **deps:** lock file maintenance ([08062e0](https://github.com/scratchfoundation/scratch-render/commit/08062e0580137f8771ead3e3d3e17eb59d42d90b))

## [1.0.275](https://github.com/scratchfoundation/scratch-render/compare/v1.0.274...v1.0.275) (2024-08-15)


### Bug Fixes

* **deps:** lock file maintenance ([b8aef50](https://github.com/scratchfoundation/scratch-render/commit/b8aef501d8eb6c6a1aecb33025dd7cd958ca219b))

## [1.0.274](https://github.com/scratchfoundation/scratch-render/compare/v1.0.273...v1.0.274) (2024-08-14)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.86 ([7c4dedc](https://github.com/scratchfoundation/scratch-render/commit/7c4dedc9260d8f14b736331073164e66aa31d5e0))

## [1.0.273](https://github.com/scratchfoundation/scratch-render/compare/v1.0.272...v1.0.273) (2024-08-14)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.211 ([36be4c9](https://github.com/scratchfoundation/scratch-render/commit/36be4c90f5d7e54900853bc9e059e2c5549b9db4))

## [1.0.272](https://github.com/scratchfoundation/scratch-render/compare/v1.0.271...v1.0.272) (2024-08-14)


### Bug Fixes

* **deps:** lock file maintenance ([fb9d774](https://github.com/scratchfoundation/scratch-render/commit/fb9d774f656d3f5f37c685e9a67a9caf04381754))

## [1.0.271](https://github.com/scratchfoundation/scratch-render/compare/v1.0.270...v1.0.271) (2024-08-13)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.210 ([0a0e0d0](https://github.com/scratchfoundation/scratch-render/commit/0a0e0d0934946b183b28b5cdf442da972e2dbb54))

## [1.0.270](https://github.com/scratchfoundation/scratch-render/compare/v1.0.269...v1.0.270) (2024-08-13)


### Bug Fixes

* **deps:** lock file maintenance ([2e9d7e2](https://github.com/scratchfoundation/scratch-render/commit/2e9d7e2c738d00f4db7b09639c51e046f022a540))

## [1.0.269](https://github.com/scratchfoundation/scratch-render/compare/v1.0.268...v1.0.269) (2024-08-13)


### Bug Fixes

* **deps:** lock file maintenance ([8f74364](https://github.com/scratchfoundation/scratch-render/commit/8f74364e29de7467c85123316208523e0d39fce3))

## [1.0.268](https://github.com/scratchfoundation/scratch-render/compare/v1.0.267...v1.0.268) (2024-08-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.209 ([2c733ab](https://github.com/scratchfoundation/scratch-render/commit/2c733ab30671970ca00a8be262fac97b02fe4f03))

## [1.0.267](https://github.com/scratchfoundation/scratch-render/compare/v1.0.266...v1.0.267) (2024-08-12)


### Bug Fixes

* **deps:** lock file maintenance ([ab693c3](https://github.com/scratchfoundation/scratch-render/commit/ab693c33c1f072df3f7cb4ed92d2fde52278f447))

## [1.0.266](https://github.com/scratchfoundation/scratch-render/compare/v1.0.265...v1.0.266) (2024-08-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.208 ([9a468eb](https://github.com/scratchfoundation/scratch-render/commit/9a468eb7f19e6655b21fd809e2dfcad063be81c3))

## [1.0.265](https://github.com/scratchfoundation/scratch-render/compare/v1.0.264...v1.0.265) (2024-08-11)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.85 ([0bfe9c8](https://github.com/scratchfoundation/scratch-render/commit/0bfe9c854bec7ed2920bb8b15ca4fa6365d16ece))

## [1.0.264](https://github.com/scratchfoundation/scratch-render/compare/v1.0.263...v1.0.264) (2024-08-11)


### Bug Fixes

* **deps:** lock file maintenance ([c15c53a](https://github.com/scratchfoundation/scratch-render/commit/c15c53ac2f75d22a3c6de3c6d8ceb59a9faad2db))

## [1.0.263](https://github.com/scratchfoundation/scratch-render/compare/v1.0.262...v1.0.263) (2024-08-11)


### Bug Fixes

* **deps:** lock file maintenance ([054d8b4](https://github.com/scratchfoundation/scratch-render/commit/054d8b420ae193e3a8063fa6e6c9c3447c20b023))

## [1.0.262](https://github.com/scratchfoundation/scratch-render/compare/v1.0.261...v1.0.262) (2024-08-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.207 ([12846a3](https://github.com/scratchfoundation/scratch-render/commit/12846a3b0a4fc83274a99bc76568df0d1159198c))

## [1.0.261](https://github.com/scratchfoundation/scratch-render/compare/v1.0.260...v1.0.261) (2024-08-10)


### Bug Fixes

* **deps:** lock file maintenance ([d2998a9](https://github.com/scratchfoundation/scratch-render/commit/d2998a99cb236036e0c1b3aba26c4fd2b026298b))

## [1.0.260](https://github.com/scratchfoundation/scratch-render/compare/v1.0.259...v1.0.260) (2024-08-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.206 ([2d60425](https://github.com/scratchfoundation/scratch-render/commit/2d604250bda53bc1e96096f4266a392611100a05))

## [1.0.259](https://github.com/scratchfoundation/scratch-render/compare/v1.0.258...v1.0.259) (2024-08-09)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.84 ([1456b0b](https://github.com/scratchfoundation/scratch-render/commit/1456b0b9289602595da33226d751d558fddc59c4))

## [1.0.258](https://github.com/scratchfoundation/scratch-render/compare/v1.0.257...v1.0.258) (2024-08-09)


### Bug Fixes

* **deps:** lock file maintenance ([b580090](https://github.com/scratchfoundation/scratch-render/commit/b58009091f17b294f38a0db9babfd6f92feab220))

## [1.0.257](https://github.com/scratchfoundation/scratch-render/compare/v1.0.256...v1.0.257) (2024-08-09)


### Bug Fixes

* **deps:** lock file maintenance ([2645bc3](https://github.com/scratchfoundation/scratch-render/commit/2645bc38ac79ad66d4db6e8de02ea9925893e2c0))

## [1.0.256](https://github.com/scratchfoundation/scratch-render/compare/v1.0.255...v1.0.256) (2024-08-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.205 ([0385067](https://github.com/scratchfoundation/scratch-render/commit/0385067f751dd6c6e488ba91da90edae8d9a1c15))

## [1.0.255](https://github.com/scratchfoundation/scratch-render/compare/v1.0.254...v1.0.255) (2024-08-08)


### Bug Fixes

* **deps:** lock file maintenance ([583b761](https://github.com/scratchfoundation/scratch-render/commit/583b761a0f524108845a513868bb5b3ae33e2f92))

## [1.0.254](https://github.com/scratchfoundation/scratch-render/compare/v1.0.253...v1.0.254) (2024-08-07)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.83 ([bf651f2](https://github.com/scratchfoundation/scratch-render/commit/bf651f2068fedb9ced7f809dbb55f07e155f8566))

## [1.0.253](https://github.com/scratchfoundation/scratch-render/compare/v1.0.252...v1.0.253) (2024-08-07)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.204 ([34e21d0](https://github.com/scratchfoundation/scratch-render/commit/34e21d08e7dd50fc14a5d9e7d0255b7b5aae84fc))

## [1.0.252](https://github.com/scratchfoundation/scratch-render/compare/v1.0.251...v1.0.252) (2024-08-07)


### Bug Fixes

* **deps:** lock file maintenance ([30c3d33](https://github.com/scratchfoundation/scratch-render/commit/30c3d33ea19760653744febd6ff34e0109d5a2e7))

## [1.0.251](https://github.com/scratchfoundation/scratch-render/compare/v1.0.250...v1.0.251) (2024-08-06)


### Bug Fixes

* **deps:** lock file maintenance ([24e6c62](https://github.com/scratchfoundation/scratch-render/commit/24e6c621320610003333ae6ca4800e12a6c3b20c))

## [1.0.250](https://github.com/scratchfoundation/scratch-render/compare/v1.0.249...v1.0.250) (2024-08-06)


### Bug Fixes

* **deps:** lock file maintenance ([aa961cd](https://github.com/scratchfoundation/scratch-render/commit/aa961cddf7e32325734455f7723901f54c537ba6))

## [1.0.249](https://github.com/scratchfoundation/scratch-render/compare/v1.0.248...v1.0.249) (2024-08-05)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.203 ([d908260](https://github.com/scratchfoundation/scratch-render/commit/d9082604c68c5bab6643bbb18f208875b56bf629))

## [1.0.248](https://github.com/scratchfoundation/scratch-render/compare/v1.0.247...v1.0.248) (2024-08-05)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.82 ([856958b](https://github.com/scratchfoundation/scratch-render/commit/856958be58a414d6d8fcd08f52a1a09e812ef155))

## [1.0.247](https://github.com/scratchfoundation/scratch-render/compare/v1.0.246...v1.0.247) (2024-08-05)


### Bug Fixes

* **deps:** lock file maintenance ([8cbe257](https://github.com/scratchfoundation/scratch-render/commit/8cbe25728833ae394a7cdfb237b4ae1a94e05d88))

## [1.0.246](https://github.com/scratchfoundation/scratch-render/compare/v1.0.245...v1.0.246) (2024-08-04)


### Bug Fixes

* **deps:** lock file maintenance ([2e17b27](https://github.com/scratchfoundation/scratch-render/commit/2e17b27f18c7a975c4a26844d8679c2af3fe659f))

## [1.0.245](https://github.com/scratchfoundation/scratch-render/compare/v1.0.244...v1.0.245) (2024-08-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.201 ([1f56593](https://github.com/scratchfoundation/scratch-render/commit/1f5659380bb9e3493d0d82cd9a6b685e109d8d10))

## [1.0.244](https://github.com/scratchfoundation/scratch-render/compare/v1.0.243...v1.0.244) (2024-08-03)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.81 ([8a45fc3](https://github.com/scratchfoundation/scratch-render/commit/8a45fc3eb735e434da2404b4615955a75aa24221))

## [1.0.243](https://github.com/scratchfoundation/scratch-render/compare/v1.0.242...v1.0.243) (2024-08-03)


### Bug Fixes

* **deps:** lock file maintenance ([cfb8049](https://github.com/scratchfoundation/scratch-render/commit/cfb8049479d1fb40bd152aa8e71670a3afd5dcb9))

## [1.0.242](https://github.com/scratchfoundation/scratch-render/compare/v1.0.241...v1.0.242) (2024-08-03)


### Bug Fixes

* **deps:** lock file maintenance ([944cff3](https://github.com/scratchfoundation/scratch-render/commit/944cff39e807b826acac5ef78d9607c708e05636))

## [1.0.241](https://github.com/scratchfoundation/scratch-render/compare/v1.0.240...v1.0.241) (2024-08-02)


### Bug Fixes

* **deps:** lock file maintenance ([3120aec](https://github.com/scratchfoundation/scratch-render/commit/3120aec26777800fe6aa0bbed3b80a5435519b3a))

## [1.0.240](https://github.com/scratchfoundation/scratch-render/compare/v1.0.239...v1.0.240) (2024-08-01)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.80 ([42376a5](https://github.com/scratchfoundation/scratch-render/commit/42376a557dbb2081dee86771448b828cda915149))

## [1.0.239](https://github.com/scratchfoundation/scratch-render/compare/v1.0.238...v1.0.239) (2024-08-01)


### Bug Fixes

* **deps:** lock file maintenance ([128ece1](https://github.com/scratchfoundation/scratch-render/commit/128ece1ff607b124c03fc0f2d5a2c5b57ee929e6))

## [1.0.238](https://github.com/scratchfoundation/scratch-render/compare/v1.0.237...v1.0.238) (2024-07-31)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.199 ([76b2f3d](https://github.com/scratchfoundation/scratch-render/commit/76b2f3d354559e73ef9db35522484206a3c71679))

## [1.0.237](https://github.com/scratchfoundation/scratch-render/compare/v1.0.236...v1.0.237) (2024-07-31)


### Bug Fixes

* **deps:** lock file maintenance ([89cf430](https://github.com/scratchfoundation/scratch-render/commit/89cf430ac7904f8af19224ecbb4ebe3df007bb97))
* fixed hull.js version to 0.2.10 because of breaking changes ([d3000e7](https://github.com/scratchfoundation/scratch-render/commit/d3000e7a8fc110e50ed576b247ff206f9cf85ee4))

## [1.0.236](https://github.com/scratchfoundation/scratch-render/compare/v1.0.235...v1.0.236) (2024-07-30)


### Bug Fixes

* **deps:** lock file maintenance ([616e893](https://github.com/scratchfoundation/scratch-render/commit/616e8933ed23d0231a8d930e162c13f1033268de))

## [1.0.235](https://github.com/scratchfoundation/scratch-render/compare/v1.0.234...v1.0.235) (2024-07-30)


### Bug Fixes

* **deps:** lock file maintenance ([f6d8ba1](https://github.com/scratchfoundation/scratch-render/commit/f6d8ba12514babe842d21b7044edb38c88a3b0b6))

## [1.0.234](https://github.com/scratchfoundation/scratch-render/compare/v1.0.233...v1.0.234) (2024-07-29)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.198 ([81f37eb](https://github.com/scratchfoundation/scratch-render/commit/81f37ebfa58de9c81c1acdd02230785ee833e146))

## [1.0.233](https://github.com/scratchfoundation/scratch-render/compare/v1.0.232...v1.0.233) (2024-07-29)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.79 ([b8eaf02](https://github.com/scratchfoundation/scratch-render/commit/b8eaf02136ba3f81e7aa753d4e9729cc2cf96b4b))

## [1.0.232](https://github.com/scratchfoundation/scratch-render/compare/v1.0.231...v1.0.232) (2024-07-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.197 ([100841e](https://github.com/scratchfoundation/scratch-render/commit/100841eaf097046362fbea974e5a34478ee5969f))

## [1.0.231](https://github.com/scratchfoundation/scratch-render/compare/v1.0.230...v1.0.231) (2024-07-27)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.195 ([8422a73](https://github.com/scratchfoundation/scratch-render/commit/8422a73d697514255392f645faeb7fcbe22902e9))

## [1.0.230](https://github.com/scratchfoundation/scratch-render/compare/v1.0.229...v1.0.230) (2024-07-27)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.78 ([3be2d83](https://github.com/scratchfoundation/scratch-render/commit/3be2d836aa63f5613043487c4dc81ed92c8b59c5))

## [1.0.229](https://github.com/scratchfoundation/scratch-render/compare/v1.0.228...v1.0.229) (2024-07-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.194 ([f0d6905](https://github.com/scratchfoundation/scratch-render/commit/f0d6905a7ceee899b1a3b94f6d4ca92530cfb4c5))

## [1.0.228](https://github.com/scratchfoundation/scratch-render/compare/v1.0.227...v1.0.228) (2024-07-25)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.193 ([bab162e](https://github.com/scratchfoundation/scratch-render/commit/bab162ef62943864d822df23320c22b766c58b4e))

## [1.0.227](https://github.com/scratchfoundation/scratch-render/compare/v1.0.226...v1.0.227) (2024-07-24)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.192 ([90a62c0](https://github.com/scratchfoundation/scratch-render/commit/90a62c0e007db30e3dd329426dde1901eae692d5))

## [1.0.226](https://github.com/scratchfoundation/scratch-render/compare/v1.0.225...v1.0.226) (2024-07-24)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.77 ([88b7ba9](https://github.com/scratchfoundation/scratch-render/commit/88b7ba9b02e9edc3760caa870005475e299ed036))

## [1.0.225](https://github.com/scratchfoundation/scratch-render/compare/v1.0.224...v1.0.225) (2024-07-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.190 ([8250a2d](https://github.com/scratchfoundation/scratch-render/commit/8250a2d7b59e63291daa35630a8957b03b762b50))

## [1.0.224](https://github.com/scratchfoundation/scratch-render/compare/v1.0.223...v1.0.224) (2024-07-23)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.76 ([b40a88b](https://github.com/scratchfoundation/scratch-render/commit/b40a88bba7e43e81e90618cfa3fc748b2e0f7815))

## [1.0.223](https://github.com/scratchfoundation/scratch-render/compare/v1.0.222...v1.0.223) (2024-07-22)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.189 ([ace477e](https://github.com/scratchfoundation/scratch-render/commit/ace477ebdb0e1235f8c77d6856c62ffefc395b21))

## [1.0.222](https://github.com/scratchfoundation/scratch-render/compare/v1.0.221...v1.0.222) (2024-07-21)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.188 ([83b2780](https://github.com/scratchfoundation/scratch-render/commit/83b27806f80fe565f39f6d72205cd3287551893e))

## [1.0.221](https://github.com/scratchfoundation/scratch-render/compare/v1.0.220...v1.0.221) (2024-07-21)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.75 ([dcf0a8b](https://github.com/scratchfoundation/scratch-render/commit/dcf0a8b6a292f0d8ff8953ce65b8c26e67f0a07c))

## [1.0.220](https://github.com/scratchfoundation/scratch-render/compare/v1.0.219...v1.0.220) (2024-07-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.187 ([5304ade](https://github.com/scratchfoundation/scratch-render/commit/5304adee76b5ca6e9d588b26c49dd7bfe9104c67))

## [1.0.219](https://github.com/scratchfoundation/scratch-render/compare/v1.0.218...v1.0.219) (2024-07-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.186 ([84a2399](https://github.com/scratchfoundation/scratch-render/commit/84a2399c5d8eef1d42d3214a89fb1a9d0aefecd3))

## [1.0.218](https://github.com/scratchfoundation/scratch-render/compare/v1.0.217...v1.0.218) (2024-07-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.185 ([f356bd8](https://github.com/scratchfoundation/scratch-render/commit/f356bd8d510224fd2789e2eda85847d055dc4a01))

## [1.0.217](https://github.com/scratchfoundation/scratch-render/compare/v1.0.216...v1.0.217) (2024-07-18)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.74 ([a987d98](https://github.com/scratchfoundation/scratch-render/commit/a987d98e555e3830272de172303ce67097da73de))

## [1.0.216](https://github.com/scratchfoundation/scratch-render/compare/v1.0.215...v1.0.216) (2024-07-17)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.184 ([627a647](https://github.com/scratchfoundation/scratch-render/commit/627a647ab22dba835d0cb993e77320ec5c839678))

## [1.0.215](https://github.com/scratchfoundation/scratch-render/compare/v1.0.214...v1.0.215) (2024-07-16)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.183 ([ca9f13b](https://github.com/scratchfoundation/scratch-render/commit/ca9f13b6eca80a7596b4733b7bbd823ade0bea81))

## [1.0.214](https://github.com/scratchfoundation/scratch-render/compare/v1.0.213...v1.0.214) (2024-07-16)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.73 ([ebc93af](https://github.com/scratchfoundation/scratch-render/commit/ebc93af7355fd9d42fcf00439547d5334c11ac7f))

## [1.0.213](https://github.com/scratchfoundation/scratch-render/compare/v1.0.212...v1.0.213) (2024-07-14)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.181 ([b9c7c8c](https://github.com/scratchfoundation/scratch-render/commit/b9c7c8ca528273faf7d33aed34eb655439beed32))

## [1.0.212](https://github.com/scratchfoundation/scratch-render/compare/v1.0.211...v1.0.212) (2024-07-14)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.72 ([fd3491b](https://github.com/scratchfoundation/scratch-render/commit/fd3491bf984dee2183c94946aa8c51ab61f9afbf))

## [1.0.211](https://github.com/scratchfoundation/scratch-render/compare/v1.0.210...v1.0.211) (2024-07-13)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.180 ([25d7d13](https://github.com/scratchfoundation/scratch-render/commit/25d7d13206f5b2bd7ffd046026136b8216292667))

## [1.0.210](https://github.com/scratchfoundation/scratch-render/compare/v1.0.209...v1.0.210) (2024-07-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.179 ([571f7c6](https://github.com/scratchfoundation/scratch-render/commit/571f7c6ff1dbe7fb0ab2b6a7db3da67128c9aa3d))

## [1.0.209](https://github.com/scratchfoundation/scratch-render/compare/v1.0.208...v1.0.209) (2024-07-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.177 ([d65f064](https://github.com/scratchfoundation/scratch-render/commit/d65f0647062fefad704e332498a4132e9f3ef1cf))

## [1.0.208](https://github.com/scratchfoundation/scratch-render/compare/v1.0.207...v1.0.208) (2024-07-11)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.71 ([afd4853](https://github.com/scratchfoundation/scratch-render/commit/afd4853a383493d92cdae15bb7c4b99b685a6078))

## [1.0.207](https://github.com/scratchfoundation/scratch-render/compare/v1.0.206...v1.0.207) (2024-07-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.176 ([70a85b8](https://github.com/scratchfoundation/scratch-render/commit/70a85b88549bd2450299b00d61caa1c939b3def5))

## [1.0.206](https://github.com/scratchfoundation/scratch-render/compare/v1.0.205...v1.0.206) (2024-07-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.175 ([8ff9593](https://github.com/scratchfoundation/scratch-render/commit/8ff959369c2309080a4afe453df2ee0d22f1d175))

## [1.0.205](https://github.com/scratchfoundation/scratch-render/compare/v1.0.204...v1.0.205) (2024-07-09)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.70 ([52410ad](https://github.com/scratchfoundation/scratch-render/commit/52410ad32d6f4c963db87ff3ec42d2fc170f800a))

## [1.0.204](https://github.com/scratchfoundation/scratch-render/compare/v1.0.203...v1.0.204) (2024-07-07)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.173 ([bf3c99d](https://github.com/scratchfoundation/scratch-render/commit/bf3c99d6576b67ec705f95950a1a38899e7ceaf7))

## [1.0.203](https://github.com/scratchfoundation/scratch-render/compare/v1.0.202...v1.0.203) (2024-07-06)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.69 ([b566a27](https://github.com/scratchfoundation/scratch-render/commit/b566a27c73c1864086a9cecdd0d14fe424b1b190))

## [1.0.202](https://github.com/scratchfoundation/scratch-render/compare/v1.0.201...v1.0.202) (2024-07-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.172 ([5878c4f](https://github.com/scratchfoundation/scratch-render/commit/5878c4fe1d5179dc548e306629cda8e4f5235bb5))

## [1.0.201](https://github.com/scratchfoundation/scratch-render/compare/v1.0.200...v1.0.201) (2024-07-04)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.68 ([b2d13fb](https://github.com/scratchfoundation/scratch-render/commit/b2d13fb90f24fe50e94e379c6df70ff324fa8e9f))

## [1.0.200](https://github.com/scratchfoundation/scratch-render/compare/v1.0.199...v1.0.200) (2024-07-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.171 ([fdfd115](https://github.com/scratchfoundation/scratch-render/commit/fdfd11532316e9ba5bf8f46bffdad48a4afa1107))

## [1.0.199](https://github.com/scratchfoundation/scratch-render/compare/v1.0.198...v1.0.199) (2024-07-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.169 ([22ebdb5](https://github.com/scratchfoundation/scratch-render/commit/22ebdb56b695a1c5d49bd7859e11ea84dd89c94f))

## [1.0.198](https://github.com/scratchfoundation/scratch-render/compare/v1.0.197...v1.0.198) (2024-07-01)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.167 ([69f0bba](https://github.com/scratchfoundation/scratch-render/commit/69f0bba26487747241c48cba44c98acd03554ef7))

## [1.0.197](https://github.com/scratchfoundation/scratch-render/compare/v1.0.196...v1.0.197) (2024-07-01)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.67 ([3b5deea](https://github.com/scratchfoundation/scratch-render/commit/3b5deea8a87434faab051ace419fae2101121696))

## [1.0.196](https://github.com/scratchfoundation/scratch-render/compare/v1.0.195...v1.0.196) (2024-06-30)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.166 ([efdbee7](https://github.com/scratchfoundation/scratch-render/commit/efdbee71bdce6bdbb183d5e5b41e3b9efccbb23b))

## [1.0.195](https://github.com/scratchfoundation/scratch-render/compare/v1.0.194...v1.0.195) (2024-06-29)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.165 ([cd22bf4](https://github.com/scratchfoundation/scratch-render/commit/cd22bf40c82addb699212ce9a895dde947cde17b))

## [1.0.194](https://github.com/scratchfoundation/scratch-render/compare/v1.0.193...v1.0.194) (2024-06-29)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.66 ([4b4f874](https://github.com/scratchfoundation/scratch-render/commit/4b4f874857b30f4c3f6fe5c678d9f4fca3bace9c))

## [1.0.193](https://github.com/scratchfoundation/scratch-render/compare/v1.0.192...v1.0.193) (2024-06-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.164 ([8650e3c](https://github.com/scratchfoundation/scratch-render/commit/8650e3c85c3589fa1ebccbd4699812a832429a95))

## [1.0.192](https://github.com/scratchfoundation/scratch-render/compare/v1.0.191...v1.0.192) (2024-06-27)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.163 ([19b00f6](https://github.com/scratchfoundation/scratch-render/commit/19b00f6077b69908faea4a6ce9503bd5a8101be5))

## [1.0.191](https://github.com/scratchfoundation/scratch-render/compare/v1.0.190...v1.0.191) (2024-06-27)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.65 ([a82336f](https://github.com/scratchfoundation/scratch-render/commit/a82336f36badfb1ecc2ac2bfcae4e146f32d8c44))

## [1.0.190](https://github.com/scratchfoundation/scratch-render/compare/v1.0.189...v1.0.190) (2024-06-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.162 ([443b7c4](https://github.com/scratchfoundation/scratch-render/commit/443b7c42cbac6c092a523726d3d83dcdf0d3ba86))

## [1.0.189](https://github.com/scratchfoundation/scratch-render/compare/v1.0.188...v1.0.189) (2024-06-25)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.160 ([9d8ad66](https://github.com/scratchfoundation/scratch-render/commit/9d8ad66c668d4886017052589a2d29847ceba7e3))

## [1.0.188](https://github.com/scratchfoundation/scratch-render/compare/v1.0.187...v1.0.188) (2024-06-25)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.64 ([12e4f33](https://github.com/scratchfoundation/scratch-render/commit/12e4f3375249559d3d989a79b08e86e960914b50))

## [1.0.187](https://github.com/scratchfoundation/scratch-render/compare/v1.0.186...v1.0.187) (2024-06-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.159 ([f98faf5](https://github.com/scratchfoundation/scratch-render/commit/f98faf50a1c46b01aec778fa9ceed8d0c3cc49de))

## [1.0.186](https://github.com/scratchfoundation/scratch-render/compare/v1.0.185...v1.0.186) (2024-06-22)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.158 ([07b5b55](https://github.com/scratchfoundation/scratch-render/commit/07b5b55419e983fb730da5d1d83f39409aaf94d2))

## [1.0.185](https://github.com/scratchfoundation/scratch-render/compare/v1.0.184...v1.0.185) (2024-06-22)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.63 ([847cc92](https://github.com/scratchfoundation/scratch-render/commit/847cc926f929f0fbfa000728211c809c3e50b44e))

## [1.0.184](https://github.com/scratchfoundation/scratch-render/compare/v1.0.183...v1.0.184) (2024-06-21)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.155 ([dff985c](https://github.com/scratchfoundation/scratch-render/commit/dff985c35c20b00e7ec1c52ecc074e500fe5bbea))

## [1.0.183](https://github.com/scratchfoundation/scratch-render/compare/v1.0.182...v1.0.183) (2024-06-20)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.62 ([3355948](https://github.com/scratchfoundation/scratch-render/commit/33559480e082792a757acdcd4ffc9c9f90d9ca42))

## [1.0.182](https://github.com/scratchfoundation/scratch-render/compare/v1.0.181...v1.0.182) (2024-06-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.154 ([821446b](https://github.com/scratchfoundation/scratch-render/commit/821446b8986faac17593f12d2e1e5bfd806077c4))

## [1.0.181](https://github.com/scratchfoundation/scratch-render/compare/v1.0.180...v1.0.181) (2024-06-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.153 ([6d577c7](https://github.com/scratchfoundation/scratch-render/commit/6d577c7533a66aef13ad6d35f685b665e4529046))

## [1.0.180](https://github.com/scratchfoundation/scratch-render/compare/v1.0.179...v1.0.180) (2024-06-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.152 ([a717ef6](https://github.com/scratchfoundation/scratch-render/commit/a717ef60ca768f7a645b7ffd433da4ea204240e3))

## [1.0.179](https://github.com/scratchfoundation/scratch-render/compare/v1.0.178...v1.0.179) (2024-06-18)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.61 ([07b1d0c](https://github.com/scratchfoundation/scratch-render/commit/07b1d0cfa6dad7610fc514341e8d419e88f4be73))

## [1.0.178](https://github.com/scratchfoundation/scratch-render/compare/v1.0.177...v1.0.178) (2024-06-17)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.151 ([11ac00a](https://github.com/scratchfoundation/scratch-render/commit/11ac00ae78db56fb9f8d64491bcebcbd469b26fe))

## [1.0.177](https://github.com/scratchfoundation/scratch-render/compare/v1.0.176...v1.0.177) (2024-06-16)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.60 ([7067091](https://github.com/scratchfoundation/scratch-render/commit/7067091cd0b5ab92792b62dbf873af9e79902c62))

## [1.0.176](https://github.com/scratchfoundation/scratch-render/compare/v1.0.175...v1.0.176) (2024-06-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.150 ([d36e168](https://github.com/scratchfoundation/scratch-render/commit/d36e168c6455bce1046acb901f9f9085bd61cdc4))

## [1.0.175](https://github.com/scratchfoundation/scratch-render/compare/v1.0.174...v1.0.175) (2024-06-14)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.149 ([7e095fd](https://github.com/scratchfoundation/scratch-render/commit/7e095fd594e338cf2193880228a60c6c45d9ac91))

## [1.0.174](https://github.com/scratchfoundation/scratch-render/compare/v1.0.173...v1.0.174) (2024-06-13)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.147 ([8d74fec](https://github.com/scratchfoundation/scratch-render/commit/8d74fec3bbe1a2f4bf0d28c19dedef1ab02512ab))

## [1.0.173](https://github.com/scratchfoundation/scratch-render/compare/v1.0.172...v1.0.173) (2024-06-13)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.59 ([6d2d368](https://github.com/scratchfoundation/scratch-render/commit/6d2d36849f267b7786d333be0ba0b9d789e24b90))

## [1.0.172](https://github.com/scratchfoundation/scratch-render/compare/v1.0.171...v1.0.172) (2024-06-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.145 ([d9b9139](https://github.com/scratchfoundation/scratch-render/commit/d9b913931f068b529e8247299402946e4c9ee297))

## [1.0.171](https://github.com/scratchfoundation/scratch-render/compare/v1.0.170...v1.0.171) (2024-06-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.144 ([1342af0](https://github.com/scratchfoundation/scratch-render/commit/1342af0e33330595d74c446d72414de711e197e5))

## [1.0.170](https://github.com/scratchfoundation/scratch-render/compare/v1.0.169...v1.0.170) (2024-06-11)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.58 ([6e7b83c](https://github.com/scratchfoundation/scratch-render/commit/6e7b83c238dab7c96f33d51989184cb891c62c5c))

## [1.0.169](https://github.com/scratchfoundation/scratch-render/compare/v1.0.168...v1.0.169) (2024-06-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.143 ([92a653f](https://github.com/scratchfoundation/scratch-render/commit/92a653f2b1b3c33ca6de3a107ba2ef6bf4145077))

## [1.0.168](https://github.com/scratchfoundation/scratch-render/compare/v1.0.167...v1.0.168) (2024-06-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.141 ([09e86c7](https://github.com/scratchfoundation/scratch-render/commit/09e86c7f63ad884187dd03751478878e81777578))

## [1.0.167](https://github.com/scratchfoundation/scratch-render/compare/v1.0.166...v1.0.167) (2024-06-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.140 ([31fd97e](https://github.com/scratchfoundation/scratch-render/commit/31fd97eba6414fc204c0f020125699c2f02e353c))

## [1.0.166](https://github.com/scratchfoundation/scratch-render/compare/v1.0.165...v1.0.166) (2024-06-08)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.57 ([c1e4d0a](https://github.com/scratchfoundation/scratch-render/commit/c1e4d0a1bea1a2249a20bc0697b4c5daf03f8918))

## [1.0.165](https://github.com/scratchfoundation/scratch-render/compare/v1.0.164...v1.0.165) (2024-06-07)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.139 ([e670e72](https://github.com/scratchfoundation/scratch-render/commit/e670e72612f1ffa903f0f3c161694b70ae066622))

## [1.0.164](https://github.com/scratchfoundation/scratch-render/compare/v1.0.163...v1.0.164) (2024-06-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.137 ([1a9133d](https://github.com/scratchfoundation/scratch-render/commit/1a9133d7b344ee765da58e238e4669f2e462e7a1))

## [1.0.163](https://github.com/scratchfoundation/scratch-render/compare/v1.0.162...v1.0.163) (2024-06-05)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.136 ([67a7c03](https://github.com/scratchfoundation/scratch-render/commit/67a7c034f7d6cce6b5f8b63f609ec7ba7ff00286))

## [1.0.162](https://github.com/scratchfoundation/scratch-render/compare/v1.0.161...v1.0.162) (2024-06-05)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.56 ([f54582d](https://github.com/scratchfoundation/scratch-render/commit/f54582deb39dab1fc10aff0d9a56d2ffe395ac94))

## [1.0.161](https://github.com/scratchfoundation/scratch-render/compare/v1.0.160...v1.0.161) (2024-06-04)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.135 ([ea5a639](https://github.com/scratchfoundation/scratch-render/commit/ea5a639284cf97e769a967832e3a658234ebe862))

## [1.0.160](https://github.com/scratchfoundation/scratch-render/compare/v1.0.159...v1.0.160) (2024-06-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.134 ([4cd3f2e](https://github.com/scratchfoundation/scratch-render/commit/4cd3f2eb963e35f07d551c912266aa7acc0892f7))

## [1.0.159](https://github.com/scratchfoundation/scratch-render/compare/v1.0.158...v1.0.159) (2024-06-03)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.55 ([d7f8ac4](https://github.com/scratchfoundation/scratch-render/commit/d7f8ac42a6a99de3834e7939140239eb39fceb65))

## [1.0.158](https://github.com/scratchfoundation/scratch-render/compare/v1.0.157...v1.0.158) (2024-06-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.133 ([ac2b405](https://github.com/scratchfoundation/scratch-render/commit/ac2b4052e57569eaf410f3ad3e90a4f362ed513e))

## [1.0.157](https://github.com/scratchfoundation/scratch-render/compare/v1.0.156...v1.0.157) (2024-06-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.132 ([426a002](https://github.com/scratchfoundation/scratch-render/commit/426a0024d39bd4056891c57775a950a020d213ee))

## [1.0.156](https://github.com/scratchfoundation/scratch-render/compare/v1.0.155...v1.0.156) (2024-06-01)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.131 ([2858f10](https://github.com/scratchfoundation/scratch-render/commit/2858f103ab27a361270e165ac1c786efeb473686))

## [1.0.155](https://github.com/scratchfoundation/scratch-render/compare/v1.0.154...v1.0.155) (2024-06-01)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.54 ([36a4a56](https://github.com/scratchfoundation/scratch-render/commit/36a4a56b6aa3eac64d3a1066ddaa2a4f583a93d5))

## [1.0.154](https://github.com/scratchfoundation/scratch-render/compare/v1.0.153...v1.0.154) (2024-05-31)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.130 ([bc27599](https://github.com/scratchfoundation/scratch-render/commit/bc27599109d4dc0d356d3c799105221796116b55))

## [1.0.153](https://github.com/scratchfoundation/scratch-render/compare/v1.0.152...v1.0.153) (2024-05-30)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.128 ([c366b8e](https://github.com/scratchfoundation/scratch-render/commit/c366b8ed63e88cb85228e9510413ed465c9d2dbd))

## [1.0.152](https://github.com/scratchfoundation/scratch-render/compare/v1.0.151...v1.0.152) (2024-05-29)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.53 ([d6c725c](https://github.com/scratchfoundation/scratch-render/commit/d6c725cd8f621ccd446e4c70f38569fd330c0f9e))

## [1.0.151](https://github.com/scratchfoundation/scratch-render/compare/v1.0.150...v1.0.151) (2024-05-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.127 ([28d1e19](https://github.com/scratchfoundation/scratch-render/commit/28d1e19c6515d8d8d1343fbfc41288d79f4b2691))

## [1.0.150](https://github.com/scratchfoundation/scratch-render/compare/v1.0.149...v1.0.150) (2024-05-27)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.126 ([7b9fd75](https://github.com/scratchfoundation/scratch-render/commit/7b9fd75592cdd325ad42b062949daeefddf0aed1))

## [1.0.149](https://github.com/scratchfoundation/scratch-render/compare/v1.0.148...v1.0.149) (2024-05-27)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.52 ([27a4f09](https://github.com/scratchfoundation/scratch-render/commit/27a4f09fb4a50b077b39e541121458bf0b9de9ae))

## [1.0.148](https://github.com/scratchfoundation/scratch-render/compare/v1.0.147...v1.0.148) (2024-05-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.125 ([9945c70](https://github.com/scratchfoundation/scratch-render/commit/9945c70f1dd7606dead978b5baf3d450b54da8b7))

## [1.0.147](https://github.com/scratchfoundation/scratch-render/compare/v1.0.146...v1.0.147) (2024-05-25)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.51 ([41bc0ce](https://github.com/scratchfoundation/scratch-render/commit/41bc0ce786a157f656cd19bc34b1442099db2c18))

## [1.0.146](https://github.com/scratchfoundation/scratch-render/compare/v1.0.145...v1.0.146) (2024-05-25)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.124 ([9ad74a7](https://github.com/scratchfoundation/scratch-render/commit/9ad74a7742958e056e6a69f32b83158f9c7f82c9))

## [1.0.145](https://github.com/scratchfoundation/scratch-render/compare/v1.0.144...v1.0.145) (2024-05-24)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.122 ([19ba9d3](https://github.com/scratchfoundation/scratch-render/commit/19ba9d36efa44b3f79fd02bc69391adec6d7e46d))

## [1.0.144](https://github.com/scratchfoundation/scratch-render/compare/v1.0.143...v1.0.144) (2024-05-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.120 ([c293cd1](https://github.com/scratchfoundation/scratch-render/commit/c293cd126a2bf9e85983bc4f7d1910c499ed19e1))

## [1.0.143](https://github.com/scratchfoundation/scratch-render/compare/v1.0.142...v1.0.143) (2024-05-23)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.50 ([c039114](https://github.com/scratchfoundation/scratch-render/commit/c0391142ea62fb54b94bd21cc57b1bfd21cafdb1))

## [1.0.142](https://github.com/scratchfoundation/scratch-render/compare/v1.0.141...v1.0.142) (2024-05-22)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.118 ([1b25380](https://github.com/scratchfoundation/scratch-render/commit/1b2538086615b7d4d430c40fba95632533d4d011))

## [1.0.141](https://github.com/scratchfoundation/scratch-render/compare/v1.0.140...v1.0.141) (2024-05-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.117 ([3d5cdaf](https://github.com/scratchfoundation/scratch-render/commit/3d5cdafc77242f19c660a92c5b70c649ab0a5df8))

## [1.0.140](https://github.com/scratchfoundation/scratch-render/compare/v1.0.139...v1.0.140) (2024-05-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.116 ([d9dde6b](https://github.com/scratchfoundation/scratch-render/commit/d9dde6bd97a19aed1df222ff136cd047296dbf89))

## [1.0.139](https://github.com/scratchfoundation/scratch-render/compare/v1.0.138...v1.0.139) (2024-05-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.115 ([005ab24](https://github.com/scratchfoundation/scratch-render/commit/005ab248168a8591855e2fe0382a55a4b03ca68d))

## [1.0.138](https://github.com/scratchfoundation/scratch-render/compare/v1.0.137...v1.0.138) (2024-05-18)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.49 ([6feb390](https://github.com/scratchfoundation/scratch-render/commit/6feb3907176bb2839b1d1216464ee638464518a2))

## [1.0.137](https://github.com/scratchfoundation/scratch-render/compare/v1.0.136...v1.0.137) (2024-05-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.114 ([7b0b8ff](https://github.com/scratchfoundation/scratch-render/commit/7b0b8ff2357704c97e0dfd71274e902a8094482e))

## [1.0.136](https://github.com/scratchfoundation/scratch-render/compare/v1.0.135...v1.0.136) (2024-05-14)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.113 ([a45b322](https://github.com/scratchfoundation/scratch-render/commit/a45b3225526ade1316bfec890806e9dcfa7381e6))

## [1.0.135](https://github.com/scratchfoundation/scratch-render/compare/v1.0.134...v1.0.135) (2024-05-13)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.111 ([faa00f2](https://github.com/scratchfoundation/scratch-render/commit/faa00f2a2276a2686a076f800bdfe700e431add5))

## [1.0.134](https://github.com/scratchfoundation/scratch-render/compare/v1.0.133...v1.0.134) (2024-05-13)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.48 ([cc9c03a](https://github.com/scratchfoundation/scratch-render/commit/cc9c03ab386ffb9be419a46e9061ec75e733b824))

## [1.0.133](https://github.com/scratchfoundation/scratch-render/compare/v1.0.132...v1.0.133) (2024-05-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.110 ([df89263](https://github.com/scratchfoundation/scratch-render/commit/df892637c1538b38e78caa813f17aea016c140e1))

## [1.0.132](https://github.com/scratchfoundation/scratch-render/compare/v1.0.131...v1.0.132) (2024-05-10)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.47 ([8a1adcb](https://github.com/scratchfoundation/scratch-render/commit/8a1adcb3b66d28f512d3bd3b892263db08c24df6))

## [1.0.131](https://github.com/scratchfoundation/scratch-render/compare/v1.0.130...v1.0.131) (2024-05-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.108 ([17310b9](https://github.com/scratchfoundation/scratch-render/commit/17310b96d390126adfd0edad51dc0c359eddf2f5))

## [1.0.130](https://github.com/scratchfoundation/scratch-render/compare/v1.0.129...v1.0.130) (2024-05-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.107 ([243fb19](https://github.com/scratchfoundation/scratch-render/commit/243fb198bdf929f2f7d5e79290e98776adf0fc7d))

## [1.0.129](https://github.com/scratchfoundation/scratch-render/compare/v1.0.128...v1.0.129) (2024-05-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.106 ([a99e5a7](https://github.com/scratchfoundation/scratch-render/commit/a99e5a7b4e540365b1c1c14e2414a87490d51ef1))

## [1.0.128](https://github.com/scratchfoundation/scratch-render/compare/v1.0.127...v1.0.128) (2024-05-08)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.46 ([171a697](https://github.com/scratchfoundation/scratch-render/commit/171a697423a0699cacf8a6fd362a9895421deb11))

## [1.0.127](https://github.com/scratchfoundation/scratch-render/compare/v1.0.126...v1.0.127) (2024-05-07)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.105 ([da024ff](https://github.com/scratchfoundation/scratch-render/commit/da024ff4912381feee6e2d80f1f7979a6e1d2cf9))

## [1.0.126](https://github.com/scratchfoundation/scratch-render/compare/v1.0.125...v1.0.126) (2024-05-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.104 ([b759c0e](https://github.com/scratchfoundation/scratch-render/commit/b759c0e7249a5aa56191a1b5c832b0cf81b964a8))

## [1.0.125](https://github.com/scratchfoundation/scratch-render/compare/v1.0.124...v1.0.125) (2024-05-06)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.45 ([b5fed50](https://github.com/scratchfoundation/scratch-render/commit/b5fed500a4f5bc21e26331caf55d5eef831faeb2))

## [1.0.124](https://github.com/scratchfoundation/scratch-render/compare/v1.0.123...v1.0.124) (2024-05-05)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.103 ([463b54a](https://github.com/scratchfoundation/scratch-render/commit/463b54a02e82d4f5f03ab4d149b56c981862350d))

## [1.0.123](https://github.com/scratchfoundation/scratch-render/compare/v1.0.122...v1.0.123) (2024-05-04)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.102 ([125e668](https://github.com/scratchfoundation/scratch-render/commit/125e668b940c7992b20cc45bc4f3fb3c65d89114))

## [1.0.122](https://github.com/scratchfoundation/scratch-render/compare/v1.0.121...v1.0.122) (2024-05-04)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.44 ([c9653c1](https://github.com/scratchfoundation/scratch-render/commit/c9653c1f077391af702953da3731e84716ef562f))

## [1.0.121](https://github.com/scratchfoundation/scratch-render/compare/v1.0.120...v1.0.121) (2024-05-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.101 ([589342a](https://github.com/scratchfoundation/scratch-render/commit/589342aa7969f04a47fa6650868895847fd9bcdf))

## [1.0.120](https://github.com/scratchfoundation/scratch-render/compare/v1.0.119...v1.0.120) (2024-05-02)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.43 ([f0fe263](https://github.com/scratchfoundation/scratch-render/commit/f0fe263f679a809ca58e472db89d42af7469489b))

## [1.0.119](https://github.com/scratchfoundation/scratch-render/compare/v1.0.118...v1.0.119) (2024-05-01)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.99 ([1bf8173](https://github.com/scratchfoundation/scratch-render/commit/1bf817345487a14f616682b0b5673c1df7178d5c))

## [1.0.118](https://github.com/scratchfoundation/scratch-render/compare/v1.0.117...v1.0.118) (2024-04-30)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.98 ([12ac437](https://github.com/scratchfoundation/scratch-render/commit/12ac437fd32bb93b6b79a53c4d361215d0fe1b8d))

## [1.0.117](https://github.com/scratchfoundation/scratch-render/compare/v1.0.116...v1.0.117) (2024-04-29)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.96 ([ceadde2](https://github.com/scratchfoundation/scratch-render/commit/ceadde29061fa307baac237d4f34f8bfc25f5cb9))

## [1.0.116](https://github.com/scratchfoundation/scratch-render/compare/v1.0.115...v1.0.116) (2024-04-29)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.42 ([1a00680](https://github.com/scratchfoundation/scratch-render/commit/1a006809b245e6e743bb90ab4deafd32e0df3861))

## [1.0.115](https://github.com/scratchfoundation/scratch-render/compare/v1.0.114...v1.0.115) (2024-04-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.95 ([ad745e3](https://github.com/scratchfoundation/scratch-render/commit/ad745e33d754c597db1517f1e73b0dfd282e4bbf))

## [1.0.114](https://github.com/scratchfoundation/scratch-render/compare/v1.0.113...v1.0.114) (2024-04-27)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.94 ([7e140b5](https://github.com/scratchfoundation/scratch-render/commit/7e140b5a02ba312e1e261d9ac46748e9f0c015ed))

## [1.0.113](https://github.com/scratchfoundation/scratch-render/compare/v1.0.112...v1.0.113) (2024-04-27)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.41 ([0ab9c1c](https://github.com/scratchfoundation/scratch-render/commit/0ab9c1c9cccdf9cf1738f89d1111f96728336953))

## [1.0.112](https://github.com/scratchfoundation/scratch-render/compare/v1.0.111...v1.0.112) (2024-04-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.93 ([9a55af2](https://github.com/scratchfoundation/scratch-render/commit/9a55af2fb4436467e93d51b91ba489899435d7dd))

## [1.0.111](https://github.com/scratchfoundation/scratch-render/compare/v1.0.110...v1.0.111) (2024-04-25)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.92 ([09e7425](https://github.com/scratchfoundation/scratch-render/commit/09e7425b8ba359cf3bece77b1b17daff8e5fcb1f))

## [1.0.110](https://github.com/scratchfoundation/scratch-render/compare/v1.0.109...v1.0.110) (2024-04-25)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.40 ([1e1b5cd](https://github.com/scratchfoundation/scratch-render/commit/1e1b5cdb6846bdb19ff22335647ce05e2447c147))
* **deps:** update dependency scratch-storage to v2.3.91 ([dbd90cc](https://github.com/scratchfoundation/scratch-render/commit/dbd90cc087357afaa86abe76b2bc75ce5c998c92))

## [1.0.109](https://github.com/scratchfoundation/scratch-render/compare/v1.0.108...v1.0.109) (2024-04-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.90 ([a2c5189](https://github.com/scratchfoundation/scratch-render/commit/a2c5189705d2d2ead4ccbaf0ce0acd4f2b003593))

## [1.0.108](https://github.com/scratchfoundation/scratch-render/compare/v1.0.107...v1.0.108) (2024-04-22)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.39 ([76c598b](https://github.com/scratchfoundation/scratch-render/commit/76c598b2141aa2a31b5f7aded677380ba5d55743))

## [1.0.107](https://github.com/scratchfoundation/scratch-render/compare/v1.0.106...v1.0.107) (2024-04-22)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.89 ([66b18c6](https://github.com/scratchfoundation/scratch-render/commit/66b18c6e97663edda9e68abf0ae155c280664ab9))

## [1.0.106](https://github.com/scratchfoundation/scratch-render/compare/v1.0.105...v1.0.106) (2024-04-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.88 ([37a50ab](https://github.com/scratchfoundation/scratch-render/commit/37a50ab432f1629713f51433c2b81bc726ab4501))

## [1.0.105](https://github.com/scratchfoundation/scratch-render/compare/v1.0.104...v1.0.105) (2024-04-20)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.38 ([46be439](https://github.com/scratchfoundation/scratch-render/commit/46be439145ab7c29ab1621021f9f738f3a03cee0))

## [1.0.104](https://github.com/scratchfoundation/scratch-render/compare/v1.0.103...v1.0.104) (2024-04-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.87 ([37d25b1](https://github.com/scratchfoundation/scratch-render/commit/37d25b1ec744276b6850015b4fc07cd7e2944d9b))

## [1.0.103](https://github.com/scratchfoundation/scratch-render/compare/v1.0.102...v1.0.103) (2024-04-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.85 ([056e3fb](https://github.com/scratchfoundation/scratch-render/commit/056e3fb72a452bf92a503c7fcbd0a9d202a0de12))

## [1.0.102](https://github.com/scratchfoundation/scratch-render/compare/v1.0.101...v1.0.102) (2024-04-18)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.37 ([47758f3](https://github.com/scratchfoundation/scratch-render/commit/47758f32438f16cc0afdb46c1a0ec5f0adaef320))

## [1.0.101](https://github.com/scratchfoundation/scratch-render/compare/v1.0.100...v1.0.101) (2024-04-17)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.84 ([88039a6](https://github.com/scratchfoundation/scratch-render/commit/88039a6638fe9f30c7e03bd6475c3c99f19dd8bf))

## [1.0.100](https://github.com/scratchfoundation/scratch-render/compare/v1.0.99...v1.0.100) (2024-04-16)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.36 ([a3e4def](https://github.com/scratchfoundation/scratch-render/commit/a3e4defbeacff66840c2e84a84c1641dc69922e3))

## [1.0.99](https://github.com/scratchfoundation/scratch-render/compare/v1.0.98...v1.0.99) (2024-04-16)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.83 ([4896b68](https://github.com/scratchfoundation/scratch-render/commit/4896b6814e55dd6ba3c3092e8ff39266e322b566))

## [1.0.98](https://github.com/scratchfoundation/scratch-render/compare/v1.0.97...v1.0.98) (2024-04-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.82 ([bb976db](https://github.com/scratchfoundation/scratch-render/commit/bb976dbbd48f48e442ddb26b30034b8546e97984))

## [1.0.97](https://github.com/scratchfoundation/scratch-render/compare/v1.0.96...v1.0.97) (2024-04-14)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.35 ([35de7be](https://github.com/scratchfoundation/scratch-render/commit/35de7be99a7d5c21d3a290df8babae01020eed7b))

## [1.0.96](https://github.com/scratchfoundation/scratch-render/compare/v1.0.95...v1.0.96) (2024-04-13)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.81 ([b7a9029](https://github.com/scratchfoundation/scratch-render/commit/b7a90296cfe862c731ac107cddefe9b16c66aed9))

## [1.0.95](https://github.com/scratchfoundation/scratch-render/compare/v1.0.94...v1.0.95) (2024-04-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.79 ([ed46a61](https://github.com/scratchfoundation/scratch-render/commit/ed46a613fa9089463989e9bd4d75a188c521003d))

## [1.0.94](https://github.com/scratchfoundation/scratch-render/compare/v1.0.93...v1.0.94) (2024-04-11)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.34 ([6074256](https://github.com/scratchfoundation/scratch-render/commit/607425690769329f3e5f905947f4228df348c560))

## [1.0.93](https://github.com/scratchfoundation/scratch-render/compare/v1.0.92...v1.0.93) (2024-04-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.78 ([6c9c80d](https://github.com/scratchfoundation/scratch-render/commit/6c9c80d7db833e016da4911c018594ea61d899d6))

## [1.0.92](https://github.com/scratchfoundation/scratch-render/compare/v1.0.91...v1.0.92) (2024-04-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.77 ([db0b039](https://github.com/scratchfoundation/scratch-render/commit/db0b039945cb39c1a01b36a3810a2c25b5ba9cd6))

## [1.0.91](https://github.com/scratchfoundation/scratch-render/compare/v1.0.90...v1.0.91) (2024-04-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.76 ([15a7e58](https://github.com/scratchfoundation/scratch-render/commit/15a7e58a8b6274ea36393eca722534be7eb36fe7))

## [1.0.90](https://github.com/scratchfoundation/scratch-render/compare/v1.0.89...v1.0.90) (2024-04-09)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.33 ([86becff](https://github.com/scratchfoundation/scratch-render/commit/86becff2d0ad6a58cf9ed17ec16dd4af4cee0e45))

## [1.0.89](https://github.com/scratchfoundation/scratch-render/compare/v1.0.88...v1.0.89) (2024-04-07)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.32 ([d8fd1be](https://github.com/scratchfoundation/scratch-render/commit/d8fd1beb6cc5e3b29889e97af0118946e743c4ce))

## [1.0.88](https://github.com/scratchfoundation/scratch-render/compare/v1.0.87...v1.0.88) (2024-04-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.75 ([46f6f7d](https://github.com/scratchfoundation/scratch-render/commit/46f6f7d85bd12d7dd7455e1872aa093f35305667))

## [1.0.87](https://github.com/scratchfoundation/scratch-render/compare/v1.0.86...v1.0.87) (2024-04-04)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.73 ([871844b](https://github.com/scratchfoundation/scratch-render/commit/871844b423a7d211b73914015a06e08608666184))

## [1.0.86](https://github.com/scratchfoundation/scratch-render/compare/v1.0.85...v1.0.86) (2024-04-03)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.71 ([49048f8](https://github.com/scratchfoundation/scratch-render/commit/49048f8748d590a7e4c983d123a8e9fd8d5d4dd0))

## [1.0.85](https://github.com/scratchfoundation/scratch-render/compare/v1.0.84...v1.0.85) (2024-04-03)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.31 ([b07a1a6](https://github.com/scratchfoundation/scratch-render/commit/b07a1a6053923ee69320ffe36c2421a3bac0d8ab))

## [1.0.84](https://github.com/scratchfoundation/scratch-render/compare/v1.0.83...v1.0.84) (2024-04-02)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.70 ([21c5109](https://github.com/scratchfoundation/scratch-render/commit/21c510902b3f90e6396f66057cfc7c9a00726867))

## [1.0.83](https://github.com/scratchfoundation/scratch-render/compare/v1.0.82...v1.0.83) (2024-04-01)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.30 ([0bbc64b](https://github.com/scratchfoundation/scratch-render/commit/0bbc64b3b61a3e592b9a8711783031e55504b31a))

## [1.0.82](https://github.com/scratchfoundation/scratch-render/compare/v1.0.81...v1.0.82) (2024-03-31)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.68 ([e126a42](https://github.com/scratchfoundation/scratch-render/commit/e126a425f687a882e5c8dea9a20e287617fb3bad))

## [1.0.81](https://github.com/scratchfoundation/scratch-render/compare/v1.0.80...v1.0.81) (2024-03-30)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.67 ([aa9b86c](https://github.com/scratchfoundation/scratch-render/commit/aa9b86cb8a2aab5f693f110d0d41cca378652d2a))

## [1.0.80](https://github.com/scratchfoundation/scratch-render/compare/v1.0.79...v1.0.80) (2024-03-30)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.29 ([bc42047](https://github.com/scratchfoundation/scratch-render/commit/bc420478122099ef9f3099e427c763296c2a0b60))

## [1.0.79](https://github.com/scratchfoundation/scratch-render/compare/v1.0.78...v1.0.79) (2024-03-29)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.65 ([56cd44e](https://github.com/scratchfoundation/scratch-render/commit/56cd44ebcf9aa95154f2c0dfb3e2f5dc24372dbe))

## [1.0.78](https://github.com/scratchfoundation/scratch-render/compare/v1.0.77...v1.0.78) (2024-03-28)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.28 ([4e0488f](https://github.com/scratchfoundation/scratch-render/commit/4e0488f9a1f08a37209c79483f67b2ff0f708ebb))

## [1.0.77](https://github.com/scratchfoundation/scratch-render/compare/v1.0.76...v1.0.77) (2024-03-28)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.63 ([795f4e8](https://github.com/scratchfoundation/scratch-render/commit/795f4e8ad9c6e86dae926b0c304980c1c1f3e017))

## [1.0.76](https://github.com/scratchfoundation/scratch-render/compare/v1.0.75...v1.0.76) (2024-03-26)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.61 ([eb19011](https://github.com/scratchfoundation/scratch-render/commit/eb1901193ec034d4b2c367b3321d1679ba45275d))

## [1.0.75](https://github.com/scratchfoundation/scratch-render/compare/v1.0.74...v1.0.75) (2024-03-25)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.59 ([e60ef8e](https://github.com/scratchfoundation/scratch-render/commit/e60ef8ef2d9ebd61c5c451091669ae588c346b49))

## [1.0.74](https://github.com/scratchfoundation/scratch-render/compare/v1.0.73...v1.0.74) (2024-03-25)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.27 ([3599d0e](https://github.com/scratchfoundation/scratch-render/commit/3599d0e9bcf247e7783b0c8aec22130d6858ade1))

## [1.0.73](https://github.com/scratchfoundation/scratch-render/compare/v1.0.72...v1.0.73) (2024-03-23)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.58 ([89bc00a](https://github.com/scratchfoundation/scratch-render/commit/89bc00acbac92ef6adb7d439b5f072b31d952bab))

## [1.0.72](https://github.com/scratchfoundation/scratch-render/compare/v1.0.71...v1.0.72) (2024-03-22)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.56 ([b82d12f](https://github.com/scratchfoundation/scratch-render/commit/b82d12fcc6f182e9418648caefbca9d48ecf95a1))

## [1.0.71](https://github.com/scratchfoundation/scratch-render/compare/v1.0.70...v1.0.71) (2024-03-22)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.26 ([01b31d7](https://github.com/scratchfoundation/scratch-render/commit/01b31d71d01b699c46fb933863801812d85e811b))

## [1.0.70](https://github.com/scratchfoundation/scratch-render/compare/v1.0.69...v1.0.70) (2024-03-21)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.54 ([e7edd10](https://github.com/scratchfoundation/scratch-render/commit/e7edd103c7e2a3a686296e6b9b296ffea81ef978))

## [1.0.69](https://github.com/scratchfoundation/scratch-render/compare/v1.0.68...v1.0.69) (2024-03-20)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.52 ([ea2401a](https://github.com/scratchfoundation/scratch-render/commit/ea2401adf774a86d33f9ec2819f2b1bb97e6fcf3))

## [1.0.68](https://github.com/scratchfoundation/scratch-render/compare/v1.0.67...v1.0.68) (2024-03-20)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.25 ([c0faa6a](https://github.com/scratchfoundation/scratch-render/commit/c0faa6a3304f06befc1d75352d1952845914fd64))

## [1.0.67](https://github.com/scratchfoundation/scratch-render/compare/v1.0.66...v1.0.67) (2024-03-19)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.50 ([10daa61](https://github.com/scratchfoundation/scratch-render/commit/10daa61f15810a52542995496d48e28c37131ecc))

## [1.0.66](https://github.com/scratchfoundation/scratch-render/compare/v1.0.65...v1.0.66) (2024-03-18)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.48 ([19c1cb0](https://github.com/scratchfoundation/scratch-render/commit/19c1cb05dc1c2d7bce1affb8df3ab3ff0dd479b6))

## [1.0.65](https://github.com/scratchfoundation/scratch-render/compare/v1.0.64...v1.0.65) (2024-03-18)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.24 ([0915b04](https://github.com/scratchfoundation/scratch-render/commit/0915b04f58a5e2cf0579840ef45f8ebd5bfe8d99))

## [1.0.64](https://github.com/scratchfoundation/scratch-render/compare/v1.0.63...v1.0.64) (2024-03-17)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.47 ([4973177](https://github.com/scratchfoundation/scratch-render/commit/497317771a035078c41dd3c4cac4cd9c527c5788))

## [1.0.63](https://github.com/scratchfoundation/scratch-render/compare/v1.0.62...v1.0.63) (2024-03-16)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.46 ([cfa806f](https://github.com/scratchfoundation/scratch-render/commit/cfa806fe62060152abc7e47619a33142b09ccf93))

## [1.0.62](https://github.com/scratchfoundation/scratch-render/compare/v1.0.61...v1.0.62) (2024-03-15)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.45 ([52aeae2](https://github.com/scratchfoundation/scratch-render/commit/52aeae21e7bbe1cfa1703abc6b38839f696ad5f8))

## [1.0.61](https://github.com/scratchfoundation/scratch-render/compare/v1.0.60...v1.0.61) (2024-03-15)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.23 ([6e617dd](https://github.com/scratchfoundation/scratch-render/commit/6e617ddbd090ff93fe85288042851a445cd95a56))

## [1.0.60](https://github.com/scratchfoundation/scratch-render/compare/v1.0.59...v1.0.60) (2024-03-14)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.43 ([ac32c50](https://github.com/scratchfoundation/scratch-render/commit/ac32c5089128fd6d9d7b8bb27b9802b3794d15af))

## [1.0.59](https://github.com/scratchfoundation/scratch-render/compare/v1.0.58...v1.0.59) (2024-03-13)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.42 ([727b7b6](https://github.com/scratchfoundation/scratch-render/commit/727b7b698104f0704f1d30604645e74dfe241696))

## [1.0.58](https://github.com/scratchfoundation/scratch-render/compare/v1.0.57...v1.0.58) (2024-03-13)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.22 ([0c49583](https://github.com/scratchfoundation/scratch-render/commit/0c495835eb35242d8f4b5b8a4dfe82d6c991aee9))

## [1.0.57](https://github.com/scratchfoundation/scratch-render/compare/v1.0.56...v1.0.57) (2024-03-12)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.41 ([16ecdb8](https://github.com/scratchfoundation/scratch-render/commit/16ecdb8f1b77a91b09998dfdde3c165b377fc705))

## [1.0.56](https://github.com/scratchfoundation/scratch-render/compare/v1.0.55...v1.0.56) (2024-03-11)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.40 ([1a7fe41](https://github.com/scratchfoundation/scratch-render/commit/1a7fe41ef33f770847e160a83f81d1452580effe))

## [1.0.55](https://github.com/scratchfoundation/scratch-render/compare/v1.0.54...v1.0.55) (2024-03-10)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.21 ([e0947f2](https://github.com/scratchfoundation/scratch-render/commit/e0947f2ef2f6103b0b9c3957217bb6e066d99ffa))

## [1.0.54](https://github.com/scratchfoundation/scratch-render/compare/v1.0.53...v1.0.54) (2024-03-10)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.39 ([f07019c](https://github.com/scratchfoundation/scratch-render/commit/f07019c7cefd5db995dd4feb004fd3ccfba79ba9))

## [1.0.53](https://github.com/scratchfoundation/scratch-render/compare/v1.0.52...v1.0.53) (2024-03-09)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.38 ([81e6c95](https://github.com/scratchfoundation/scratch-render/commit/81e6c9546e499cea1994378f4bed651a6f0b08b1))

## [1.0.52](https://github.com/scratchfoundation/scratch-render/compare/v1.0.51...v1.0.52) (2024-03-09)


### Bug Fixes

* **deps:** lock file maintenance ([36c2acc](https://github.com/scratchfoundation/scratch-render/commit/36c2accb1a2eb27627805d7f06b79a3cb026b21b))

## [1.0.51](https://github.com/scratchfoundation/scratch-render/compare/v1.0.50...v1.0.51) (2024-03-08)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.36 ([a9d79a7](https://github.com/scratchfoundation/scratch-render/commit/a9d79a7966dc3bb9893d7856e201f6c211bdf8ad))

## [1.0.50](https://github.com/scratchfoundation/scratch-render/compare/v1.0.49...v1.0.50) (2024-03-08)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.20 ([3d015df](https://github.com/scratchfoundation/scratch-render/commit/3d015df19d8159de88f1f2a4cd9d88f86527f93b))

## [1.0.49](https://github.com/scratchfoundation/scratch-render/compare/v1.0.48...v1.0.49) (2024-03-08)


### Bug Fixes

* **deps:** lock file maintenance ([5d6ffb7](https://github.com/scratchfoundation/scratch-render/commit/5d6ffb77d5b8095b3b68adcaa5bc3920c6a96de9))

## [1.0.48](https://github.com/scratchfoundation/scratch-render/compare/v1.0.47...v1.0.48) (2024-03-07)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.34 ([5c586c6](https://github.com/scratchfoundation/scratch-render/commit/5c586c6cd48d649edfc66e1d56f033b6ff62f4e4))

## [1.0.47](https://github.com/scratchfoundation/scratch-render/compare/v1.0.46...v1.0.47) (2024-03-07)


### Bug Fixes

* **deps:** lock file maintenance ([a5730ca](https://github.com/scratchfoundation/scratch-render/commit/a5730ca43fa9fe0767a2a62f64deee78d33810ca))

## [1.0.46](https://github.com/scratchfoundation/scratch-render/compare/v1.0.45...v1.0.46) (2024-03-06)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.19 ([56c1b82](https://github.com/scratchfoundation/scratch-render/commit/56c1b8240fc272d7a682fa9f54f758ce8d5706f5))

## [1.0.45](https://github.com/scratchfoundation/scratch-render/compare/v1.0.44...v1.0.45) (2024-03-06)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.33 ([ea28e99](https://github.com/scratchfoundation/scratch-render/commit/ea28e9913a70941ec281240f17338bb19494bb4f))

## [1.0.44](https://github.com/scratchfoundation/scratch-render/compare/v1.0.43...v1.0.44) (2024-03-06)


### Bug Fixes

* **deps:** lock file maintenance ([908e5fa](https://github.com/scratchfoundation/scratch-render/commit/908e5fa6721d5a43301eeab0dd4403ac833a401f))

## [1.0.43](https://github.com/scratchfoundation/scratch-render/compare/v1.0.42...v1.0.43) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([5759d5c](https://github.com/scratchfoundation/scratch-render/commit/5759d5ce00612146f0973855229de5c7d23d56ed))
* **deps:** update dependency scratch-storage to v2.3.32 ([fa57654](https://github.com/scratchfoundation/scratch-render/commit/fa57654fd083f7c2e6d16b6451fdf4078f618752))

## [1.0.42](https://github.com/scratchfoundation/scratch-render/compare/v1.0.41...v1.0.42) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([f291459](https://github.com/scratchfoundation/scratch-render/commit/f2914593be7ed14048a67152b13f6cfa253b903f))
* **deps:** update dependency scratch-render-fonts to v1.0.18 ([85a5426](https://github.com/scratchfoundation/scratch-render/commit/85a542618ed1b2b47962b27e39e3a621cb97630b))

## [1.0.41](https://github.com/scratchfoundation/scratch-render/compare/v1.0.40...v1.0.41) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([1884913](https://github.com/scratchfoundation/scratch-render/commit/188491300270f101b43909d28d13fdbf01b9501f))
* **deps:** update dependency scratch-storage to v2.3.30 ([0392429](https://github.com/scratchfoundation/scratch-render/commit/03924293b4268c3ab9886a162ac972e1b5588823))

## [1.0.40](https://github.com/scratchfoundation/scratch-render/compare/v1.0.39...v1.0.40) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([9c759ba](https://github.com/scratchfoundation/scratch-render/commit/9c759ba41ad42c0c0fbb7132e8184f28b9f6ae4f))

## [1.0.39](https://github.com/scratchfoundation/scratch-render/compare/v1.0.38...v1.0.39) (2024-03-05)


### Bug Fixes

* **deps:** update dependency scratch-svg-renderer to v2 ([3e18201](https://github.com/scratchfoundation/scratch-render/commit/3e18201fdf65be045ebf541147e4988169054010))

## [1.0.38](https://github.com/scratchfoundation/scratch-render/compare/v1.0.37...v1.0.38) (2024-03-05)


### Bug Fixes

* **deps:** lock file maintenance ([198118a](https://github.com/scratchfoundation/scratch-render/commit/198118a10e49f6cf1e659860560a3585130d4db9))

## [1.0.37](https://github.com/scratchfoundation/scratch-render/compare/v1.0.36...v1.0.37) (2024-03-05)


### Bug Fixes

* **deps:** update dependency scratch-storage to v2.3.29 ([a714f7c](https://github.com/scratchfoundation/scratch-render/commit/a714f7cc6ea75a14dbd87b3cbb3988a6f9c553c9))

## [1.0.36](https://github.com/scratchfoundation/scratch-render/compare/v1.0.35...v1.0.36) (2024-03-04)


### Bug Fixes

* **deps:** lock file maintenance ([a75436a](https://github.com/scratchfoundation/scratch-render/commit/a75436a53939b570e33e58263791abc0ca9fcbd0))

## [1.0.35](https://github.com/scratchfoundation/scratch-render/compare/v1.0.34...v1.0.35) (2024-03-04)


### Bug Fixes

* **deps:** lock file maintenance ([3558fef](https://github.com/scratchfoundation/scratch-render/commit/3558fef772f6cf9ae1a1a79a3ee71c126cd75f15))

## [1.0.34](https://github.com/scratchfoundation/scratch-render/compare/v1.0.33...v1.0.34) (2024-03-04)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.17 ([717238e](https://github.com/scratchfoundation/scratch-render/commit/717238e1cc216465e9c4c16a280bc68a9c87cf7c))

## [1.0.33](https://github.com/scratchfoundation/scratch-render/compare/v1.0.32...v1.0.33) (2024-03-03)


### Bug Fixes

* **deps:** lock file maintenance ([769091b](https://github.com/scratchfoundation/scratch-render/commit/769091b6a39551ccf9474667f3c73ae054e3b7e9))

## [1.0.32](https://github.com/scratchfoundation/scratch-render/compare/v1.0.31...v1.0.32) (2024-03-03)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.16 ([01c0c16](https://github.com/scratchfoundation/scratch-render/commit/01c0c16edf1ed5a47972cd3efd4b459a6f007301))

## [1.0.31](https://github.com/scratchfoundation/scratch-render/compare/v1.0.30...v1.0.31) (2024-03-02)


### Bug Fixes

* **deps:** lock file maintenance ([0750f55](https://github.com/scratchfoundation/scratch-render/commit/0750f55fa27a64c7b8b5c859e07f397f85da39b4))

## [1.0.30](https://github.com/scratchfoundation/scratch-render/compare/v1.0.29...v1.0.30) (2024-03-02)


### Bug Fixes

* **deps:** lock file maintenance ([3ee9fea](https://github.com/scratchfoundation/scratch-render/commit/3ee9fea68d934526ff8d9ccb1e661f17e02c80bf))

## [1.0.29](https://github.com/scratchfoundation/scratch-render/compare/v1.0.28...v1.0.29) (2024-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([5844386](https://github.com/scratchfoundation/scratch-render/commit/58443865d0447ac2f0e5419c5635334aad02a7ff))

## [1.0.28](https://github.com/scratchfoundation/scratch-render/compare/v1.0.27...v1.0.28) (2024-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([29aacf9](https://github.com/scratchfoundation/scratch-render/commit/29aacf9e211316ff93191d501537c3a72d7f3ca6))

## [1.0.27](https://github.com/scratchfoundation/scratch-render/compare/v1.0.26...v1.0.27) (2024-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([f5e5501](https://github.com/scratchfoundation/scratch-render/commit/f5e5501117cbf6ce644a4d469ef8397490a82904))
* **deps:** update dependency scratch-render-fonts to v1.0.15 ([64a71fe](https://github.com/scratchfoundation/scratch-render/commit/64a71fe9931cdcb74385ad7f14046989fc1719ea))

## [1.0.26](https://github.com/scratchfoundation/scratch-render/compare/v1.0.25...v1.0.26) (2024-03-01)


### Bug Fixes

* **deps:** lock file maintenance ([9d9a0ef](https://github.com/scratchfoundation/scratch-render/commit/9d9a0ef7215c09d4086f03ee7f6f75ae7f0d684c))

## [1.0.25](https://github.com/scratchfoundation/scratch-render/compare/v1.0.24...v1.0.25) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([a69b7bc](https://github.com/scratchfoundation/scratch-render/commit/a69b7bceccccc809425967fe510ca7f8ae63da5d))

## [1.0.24](https://github.com/scratchfoundation/scratch-render/compare/v1.0.23...v1.0.24) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([105f790](https://github.com/scratchfoundation/scratch-render/commit/105f7906cbbb8d48be17b506569ca5a0878dc7a3))

## [1.0.23](https://github.com/scratchfoundation/scratch-render/compare/v1.0.22...v1.0.23) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([d92ba7e](https://github.com/scratchfoundation/scratch-render/commit/d92ba7eb9fed0c2c88438a0b221f22f5ac4a6ede))

## [1.0.22](https://github.com/scratchfoundation/scratch-render/compare/v1.0.21...v1.0.22) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([2519461](https://github.com/scratchfoundation/scratch-render/commit/25194617ff7191728f679cd07783acdef1de0d9b))

## [1.0.21](https://github.com/scratchfoundation/scratch-render/compare/v1.0.20...v1.0.21) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([47df5cd](https://github.com/scratchfoundation/scratch-render/commit/47df5cdc745f5097ecf9dac37072f2c990a1c51d))

## [1.0.20](https://github.com/scratchfoundation/scratch-render/compare/v1.0.19...v1.0.20) (2024-02-29)


### Bug Fixes

* **deps:** lock file maintenance ([14eca4c](https://github.com/scratchfoundation/scratch-render/commit/14eca4cfa1a23257e316b46eba590060f0661ad3))
* **deps:** update dependency scratch-render-fonts to v1.0.14 ([3f3fd28](https://github.com/scratchfoundation/scratch-render/commit/3f3fd28976130ae7ba5b7b973cb796bc305741f3))

## [1.0.19](https://github.com/scratchfoundation/scratch-render/compare/v1.0.18...v1.0.19) (2024-02-27)


### Bug Fixes

* **deps:** lock file maintenance ([80c6b89](https://github.com/scratchfoundation/scratch-render/commit/80c6b89dfa0a2c2c4b038370e1c6d185d275602c))
* **deps:** update dependency scratch-render-fonts to v1.0.10 ([d736d68](https://github.com/scratchfoundation/scratch-render/commit/d736d68f345395649277c62be5cdd4ac2f49822a))

## [1.0.18](https://github.com/scratchfoundation/scratch-render/compare/v1.0.17...v1.0.18) (2024-02-27)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.9 ([fea9051](https://github.com/scratchfoundation/scratch-render/commit/fea9051d160bb888221cf8f6efe9c717127893dc))

## [1.0.17](https://github.com/scratchfoundation/scratch-render/compare/v1.0.16...v1.0.17) (2024-02-26)


### Bug Fixes

* **deps:** lock file maintenance ([d287e2b](https://github.com/scratchfoundation/scratch-render/commit/d287e2b035e05f4192f5415ef284559702c59f77))

## [1.0.16](https://github.com/scratchfoundation/scratch-render/compare/v1.0.15...v1.0.16) (2024-02-26)


### Bug Fixes

* **deps:** lock file maintenance ([c504864](https://github.com/scratchfoundation/scratch-render/commit/c504864898e99ca56fd0083bba88bef21bf68825))

## [1.0.15](https://github.com/scratchfoundation/scratch-render/compare/v1.0.14...v1.0.15) (2024-02-24)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.7 ([417321a](https://github.com/scratchfoundation/scratch-render/commit/417321a695768c523b37e6ffd37dbf964c6b81ea))

## [1.0.14](https://github.com/scratchfoundation/scratch-render/compare/v1.0.13...v1.0.14) (2024-02-24)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.14 ([91f8737](https://github.com/scratchfoundation/scratch-render/commit/91f8737c5a4f5efed24c88f581eb891ee347e025))

## [1.0.13](https://github.com/scratchfoundation/scratch-render/compare/v1.0.12...v1.0.13) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.6 ([091bccd](https://github.com/scratchfoundation/scratch-render/commit/091bccdeb2645ae0d0f52fb040744a1749c854fb))

## [1.0.12](https://github.com/scratchfoundation/scratch-render/compare/v1.0.11...v1.0.12) (2024-02-22)


### Bug Fixes

* **deps:** move build-time dependencies out of 'dependencies' list ([219f958](https://github.com/scratchfoundation/scratch-render/commit/219f95865a9f88c372081a7634d9092c4c040be4))

## [1.0.11](https://github.com/scratchfoundation/scratch-render/compare/v1.0.10...v1.0.11) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.13 ([7d9e73a](https://github.com/scratchfoundation/scratch-render/commit/7d9e73a3d7376ca4f21214e65ae3e35c2fa72d03))

## [1.0.10](https://github.com/scratchfoundation/scratch-render/compare/v1.0.9...v1.0.10) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.5 ([2eead9f](https://github.com/scratchfoundation/scratch-render/commit/2eead9fd709410b5e2bbf685aa7ea9a1906be1ba))

## [1.0.9](https://github.com/scratchfoundation/scratch-render/compare/v1.0.8...v1.0.9) (2024-02-22)


### Bug Fixes

* **deps:** update dependency eslint-config-scratch to v9.0.6 ([7fb5ee3](https://github.com/scratchfoundation/scratch-render/commit/7fb5ee3ae074b4b07c356588f5b7f01d60df3b8e))

## [1.0.8](https://github.com/scratchfoundation/scratch-render/compare/v1.0.7...v1.0.8) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.12 ([58ef747](https://github.com/scratchfoundation/scratch-render/commit/58ef74753f620668f27a722111891bdf48ca37ce))

## [1.0.7](https://github.com/scratchfoundation/scratch-render/compare/v1.0.6...v1.0.7) (2024-02-22)


### Bug Fixes

* **deps:** update dependency eslint-config-scratch to v9.0.5 ([2d42150](https://github.com/scratchfoundation/scratch-render/commit/2d42150547b79457155dea6d6810afe59c97b4c4))

## [1.0.6](https://github.com/scratchfoundation/scratch-render/compare/v1.0.5...v1.0.6) (2024-02-22)


### Bug Fixes

* **deps:** update dependency scratch-semantic-release-config to v1.0.11 ([efaba35](https://github.com/scratchfoundation/scratch-render/commit/efaba353f973da7a4787cca28ebc7e0bd7f71c00))

## [1.0.5](https://github.com/scratchfoundation/scratch-render/compare/v1.0.4...v1.0.5) (2024-02-21)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.2 ([0ba8496](https://github.com/scratchfoundation/scratch-render/commit/0ba84961d4b5ba38c185a56e8843fd3a7bd25340))

## [1.0.4](https://github.com/scratchfoundation/scratch-render/compare/v1.0.3...v1.0.4) (2024-02-21)


### Bug Fixes

* **deps:** fix scratch-render-fonts peer dep ([e920e37](https://github.com/scratchfoundation/scratch-render/commit/e920e37a62aafc64cbccb447025b540c9ce9180f))

## [1.0.3](https://github.com/scratchfoundation/scratch-render/compare/v1.0.2...v1.0.3) (2024-02-21)


### Bug Fixes

* **deps:** lock file maintenance ([2c79a3b](https://github.com/scratchfoundation/scratch-render/commit/2c79a3b3ba87d276eaac4fd4c3b89ebe40f8cf33))

## [1.0.2](https://github.com/scratchfoundation/scratch-render/compare/v1.0.1...v1.0.2) (2024-02-20)


### Bug Fixes

* **deps:** update dependency ify-loader to v1.1.0 ([fa87f5a](https://github.com/scratchfoundation/scratch-render/commit/fa87f5a1932447b88fd346e0111b2585201a0719))

## [1.0.1](https://github.com/scratchfoundation/scratch-render/compare/v1.0.0...v1.0.1) (2024-02-16)


### Bug Fixes

* **deps:** unpin non-dev dependencies ([803b852](https://github.com/scratchfoundation/scratch-render/commit/803b8521a09ca7635716cd58a573521db28e7814))

# 1.0.0 (2024-01-16)


### Bug Fixes

* **deps:** update dependency scratch-render-fonts to v1.0.0-prerelease.20221102164332 ([bfc33aa](https://github.com/scratchfoundation/scratch-render/commit/bfc33aab93ffb4cf702799340da69eee8b932413))
* **deps:** update dependency scratch-render-fonts to v1.0.0-prerelease.20231017225105 ([91b4620](https://github.com/scratchfoundation/scratch-render/commit/91b462079c09b5c736819074c7909f6c65282dda))
* **deps:** update dependency scratch-storage to v1.3.6 ([1ca9b4f](https://github.com/scratchfoundation/scratch-render/commit/1ca9b4f0704a6d106c6655773ab67d3f75de3b0e))
* **deps:** update dependency scratch-svg-renderer to v0.2.0 ([3ac2002](https://github.com/scratchfoundation/scratch-render/commit/3ac200259619c8b232a71182a3c1cdb2c5339b90))
* **deps:** update dependency scratch-svg-renderer to v0.2.0-prerelease.20210727023023 ([6fb49c0](https://github.com/scratchfoundation/scratch-render/commit/6fb49c0f8b6f42922a0d01d963815603e7618d0a))
* **deps:** update dependency scratch-svg-renderer to v0.2.0-prerelease.20221102163324 ([65da944](https://github.com/scratchfoundation/scratch-render/commit/65da94416a76fb0b6af1217ddd8d27f9aed96cd6))
* **deps:** update dependency scratch-svg-renderer to v0.2.0-prerelease.20221120235613 ([d0c9d11](https://github.com/scratchfoundation/scratch-render/commit/d0c9d11bf1b44ddff015fc77a5a8bdbaa4cca4e3))
* **deps:** update dependency scratch-svg-renderer to v0.2.0-prerelease.20230224194137 ([7f7a4d2](https://github.com/scratchfoundation/scratch-render/commit/7f7a4d23d649b59c8000754ec3eccc4e2be0c2b2))
* **deps:** update dependency scratch-svg-renderer to v0.2.0-prerelease.20230710144521 ([9658a42](https://github.com/scratchfoundation/scratch-render/commit/9658a42142bf585ff468240f04253cbdb5387f91))
* **deps:** update dependency scratch-svg-renderer to v0.2.0-prerelease.20231013154115 ([2cf7e8a](https://github.com/scratchfoundation/scratch-render/commit/2cf7e8ad4110fc78a4aa7b6724707b7ff5d165b1))
* **deps:** update dependency scratch-svg-renderer to v0.2.0-prerelease.20231216035817 ([28e3316](https://github.com/scratchfoundation/scratch-render/commit/28e3316b400415a808c1adfb960b06d51585a472))
* **package:** update scratch-storage to version 1.0.0 ([32bd879](https://github.com/scratchfoundation/scratch-render/commit/32bd87933ab275ded80bef6f2ac8e50894bbc9ff)), closes [#298](https://github.com/scratchfoundation/scratch-render/issues/298)
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20180605154326 ([f10f180](https://github.com/scratchfoundation/scratch-render/commit/f10f18042e6a931c21d779a10b90a87d552094f7))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20180613184320 ([bdde016](https://github.com/scratchfoundation/scratch-render/commit/bdde0168ae6d4d92497288cb9c5317dc41d767ef))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20180817005452 ([5c6acbf](https://github.com/scratchfoundation/scratch-render/commit/5c6acbfb73e0e9267251447e0ad9a6aa79ae7add))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20180907141232 ([2eca0a8](https://github.com/scratchfoundation/scratch-render/commit/2eca0a8326a6100125c2767e8f78aa4da6912232))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20181017193458 ([94340de](https://github.com/scratchfoundation/scratch-render/commit/94340deba8263d4e2833ddb905e4644eeb8c8d54))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20181024192149 ([858bff0](https://github.com/scratchfoundation/scratch-render/commit/858bff09e1d07f9fad4510c36e5d3432f09324e6))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20181126212715 ([077d2f1](https://github.com/scratchfoundation/scratch-render/commit/077d2f1d6958d4cad03710989f3702e6a9891232)), closes [#367](https://github.com/scratchfoundation/scratch-render/issues/367)
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20181212190400 ([c6a4aa8](https://github.com/scratchfoundation/scratch-render/commit/c6a4aa870e7a50f3898e27db08eabd0556449b80))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20181213192400 ([2394fb5](https://github.com/scratchfoundation/scratch-render/commit/2394fb5610d5517a1bc3c090c05086b5209f82d2)), closes [#378](https://github.com/scratchfoundation/scratch-render/issues/378)
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20181218153528 ([0de5bef](https://github.com/scratchfoundation/scratch-render/commit/0de5befa4fd8aab8994ebe6f3d85fd0494399ac2))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190109201344 ([cf5aafc](https://github.com/scratchfoundation/scratch-render/commit/cf5aafc12fce7b6ad75b4e085587c098fbb3c45e))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190125192231 ([898d5d7](https://github.com/scratchfoundation/scratch-render/commit/898d5d7885113868b2c339ee44a60164ae4b30bd)), closes [#397](https://github.com/scratchfoundation/scratch-render/issues/397)
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190304180800 ([d59d45b](https://github.com/scratchfoundation/scratch-render/commit/d59d45b6c81ce2a097940f0c61975f12387890c4))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190419183947 ([bffe800](https://github.com/scratchfoundation/scratch-render/commit/bffe80086e3bf13fa1370a997b8d0ac7d25dc077)), closes [#430](https://github.com/scratchfoundation/scratch-render/issues/430)
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190521170426 ([f5ddc24](https://github.com/scratchfoundation/scratch-render/commit/f5ddc24c7ee50682827e1b0e40a4cb04ff8cce3c))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190523193400 ([c3d07db](https://github.com/scratchfoundation/scratch-render/commit/c3d07db39af5422fc170482ad5abfb9746dd9ec5))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190715144718 ([ae507df](https://github.com/scratchfoundation/scratch-render/commit/ae507dfabbc8a31533802527ecb140a6033aa0ca))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20190820171249 ([3bfd4c6](https://github.com/scratchfoundation/scratch-render/commit/3bfd4c65fbec09923ca5f726973884a3e6c96bda))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20191031221353 ([471c88b](https://github.com/scratchfoundation/scratch-render/commit/471c88b8503aeafb6fbaac7de8a9654b044a22b0))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20191104164753 ([c7669b0](https://github.com/scratchfoundation/scratch-render/commit/c7669b00cff1680d267911cefc1c7d55c290a578))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20191217211338 ([db1c7b3](https://github.com/scratchfoundation/scratch-render/commit/db1c7b3b2d43cf08193182abcb12554f9b0dc658))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20200103191258 ([5dc8090](https://github.com/scratchfoundation/scratch-render/commit/5dc809014e220f9a36d8199d3b3f0dce2d6ba536))
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20200205003400 ([c55344d](https://github.com/scratchfoundation/scratch-render/commit/c55344d55baac2e25a4b0b037085a37d36bf3f1f)), closes [#552](https://github.com/scratchfoundation/scratch-render/issues/552)
* **package:** update scratch-svg-renderer to version 0.2.0-prerelease.20200507183648 ([409df2a](https://github.com/scratchfoundation/scratch-render/commit/409df2a74d0a6956448e7fc84e09e5bb2d045763))


### Performance Improvements

* **setRotationCenter:** compare new setRotationCenter values after truncating to 32bit ([0f69cb7](https://github.com/scratchfoundation/scratch-render/commit/0f69cb7f7c117983448ae58fdf7bf040aa16b039))
